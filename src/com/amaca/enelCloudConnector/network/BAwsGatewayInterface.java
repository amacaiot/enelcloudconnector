package com.amaca.enelCloudConnector.network;

import com.amaca.enelCloudConnector.BEnelCloudService;
import com.amaca.enelCloudConnector.network.worker.BAwsWorker;
import com.amazonaws.services.iot.client.*;
import com.amazonaws.services.iot.client.sample.sampleUtil.SampleUtil;
import com.tridium.json.JSONException;
import com.tridium.json.JSONObject;

import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.naming.BOrd;
import javax.baja.nre.annotations.*;
import javax.baja.sys.*;
import javax.baja.units.BUnit;
import javax.baja.util.IFuture;
import javax.baja.util.Invocation;
import java.io.File;
import java.time.Instant;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Logger;


@NiagaraProperty(
        name = "endPoint",
        type = "String",
        defaultValue = "",
        flags = Flags.SUMMARY
)

@NiagaraProperty(
        name = "topic",
        type = "String",
        defaultValue = "\"\"",
        flags = Flags.HIDDEN
)

@NiagaraProperty(
        name = "registrationTopic",
        type = "String",
        defaultValue = "\"\"",
        flags = Flags.SUMMARY
)

@NiagaraProperty(
        name = "notifyTopic",
        type = "String",
        defaultValue = "\"\"",
        flags = Flags.SUMMARY
)

@NiagaraProperty(
        name = "notifyNextTopic",
        type = "String",
        defaultValue = "\"\"",
        flags = Flags.SUMMARY
)

@NiagaraProperty(
        name = "clientId",
        type = "String",
        defaultValue = "\"\"",
        flags = Flags.SUMMARY
)

@NiagaraProperty(
        name = "mqttClientTimeout",
        type = "int",
        defaultValue = "10000",
        flags = Flags.HIDDEN,
        facets =
                {
                        @Facet(name = "BFacets.PRECISION" , value = "0"),
                        @Facet(name ="BFacets.UNITS", value ="BUnit.getUnit(\"millisecond\")"),
                        @Facet(name = "BFacets.MIN", value = "1"),
                        @Facet(name = "BFacets.MAX", value = "10000")
                }
)

@NiagaraProperty(
        name = "mqttPublishTimeout",
        type = "int",
        defaultValue = "9000",
        flags = Flags.HIDDEN,
        facets =
                {
                        @Facet(name = "BFacets.PRECISION" , value = "0"),
                        @Facet(name ="BFacets.UNITS", value ="BUnit.getUnit(\"millisecond\")"),
                        @Facet(name = "BFacets.MIN", value = "1"),
                        @Facet(name = "BFacets.MAX", value = "10000")
                }
)

@NiagaraProperty(
        name = "pauseBetweenConsecutivePublish",
        type = "int",
        defaultValue = "3000",
        flags = Flags.HIDDEN,
        facets =
                {
                        @Facet(name = "BFacets.PRECISION" , value = "0"),
                        @Facet(name = "BFacets.UNITS", value ="BUnit.getUnit(\"millisecond\")"),
                        @Facet(name = "BFacets.MIN", value = "1"),
                        @Facet(name = "BFacets.MAX", value = "3000")
                }
)

@NiagaraProperty(
        name = "certificateFile",
        type = "baja:Ord",
        defaultValue = "BOrd.NULL",
        facets =
                {
                        @Facet(name = "BFacets.TARGET_TYPE", value = "\"baja:IFile\"")
                }
)

@NiagaraProperty(
        name = "privateKeyFile",
        type = "baja:Ord",
        defaultValue = "BOrd.NULL",
        facets =
                {
                        @Facet(name = "BFacets.TARGET_TYPE", value = "\"baja:IFile\"")
                }
)

@NiagaraProperty(
        name = "payload",
        type = "String",
        defaultValue = "\"\"",
        facets =
                {
                        @Facet(name = "BFacets.MULTI_LINE", value = "true")
                }
)

@NiagaraProperty(
        name = "registrationPayload",
        type = "String",
        defaultValue = "\"\"",
        facets =
                {
                        @Facet(name = "BFacets.MULTI_LINE", value = "true")
                }
)

@NiagaraProperty(
        name = "worker",
        type = "BAwsWorker",
        defaultValue= "new BAwsWorker()",
        flags = Flags.HIDDEN
)

@NiagaraAction(
        name = "publish",
        returnType = "baja:Boolean",
        flags = Flags.ASYNC
)

@NiagaraAction(
        name = "registration",
        returnType = "baja:Boolean",
        flags = Flags.ASYNC
)

@NiagaraAction(
        name = "setupMqttClient",
        returnType = "baja:Boolean",
        flags = Flags.ASYNC
)

@NiagaraTopic(
        name = "newMessageOnNotifyNextTopic",
        eventType = "baja:String"
)

@NiagaraType
public class BAwsGatewayInterface extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.network.BAwsGatewayInterface(1909257746)1.0$ @*/
/* Generated Tue Nov 17 10:03:46 CET 2020 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "endPoint"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code endPoint} property.
   * @see #getEndPoint
   * @see #setEndPoint
   */
  public static final Property endPoint = newProperty(Flags.SUMMARY, "", null);
  
  /**
   * Get the {@code endPoint} property.
   * @see #endPoint
   */
  public String getEndPoint() { return getString(endPoint); }
  
  /**
   * Set the {@code endPoint} property.
   * @see #endPoint
   */
  public void setEndPoint(String v) { setString(endPoint, v, null); }

////////////////////////////////////////////////////////////////
// Property "topic"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code topic} property.
   * @see #getTopic
   * @see #setTopic
   */
  public static final Property topic = newProperty(Flags.HIDDEN, "", null);
  
  /**
   * Get the {@code topic} property.
   * @see #topic
   */
  public String getTopic() { return getString(topic); }
  
  /**
   * Set the {@code topic} property.
   * @see #topic
   */
  public void setTopic(String v) { setString(topic, v, null); }

////////////////////////////////////////////////////////////////
// Property "registrationTopic"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code registrationTopic} property.
   * @see #getRegistrationTopic
   * @see #setRegistrationTopic
   */
  public static final Property registrationTopic = newProperty(Flags.SUMMARY, "", null);
  
  /**
   * Get the {@code registrationTopic} property.
   * @see #registrationTopic
   */
  public String getRegistrationTopic() { return getString(registrationTopic); }
  
  /**
   * Set the {@code registrationTopic} property.
   * @see #registrationTopic
   */
  public void setRegistrationTopic(String v) { setString(registrationTopic, v, null); }

////////////////////////////////////////////////////////////////
// Property "notifyTopic"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code notifyTopic} property.
   * @see #getNotifyTopic
   * @see #setNotifyTopic
   */
  public static final Property notifyTopic = newProperty(Flags.SUMMARY, "", null);
  
  /**
   * Get the {@code notifyTopic} property.
   * @see #notifyTopic
   */
  public String getNotifyTopic() { return getString(notifyTopic); }
  
  /**
   * Set the {@code notifyTopic} property.
   * @see #notifyTopic
   */
  public void setNotifyTopic(String v) { setString(notifyTopic, v, null); }

////////////////////////////////////////////////////////////////
// Property "notifyNextTopic"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code notifyNextTopic} property.
   * @see #getNotifyNextTopic
   * @see #setNotifyNextTopic
   */
  public static final Property notifyNextTopic = newProperty(Flags.SUMMARY, "", null);
  
  /**
   * Get the {@code notifyNextTopic} property.
   * @see #notifyNextTopic
   */
  public String getNotifyNextTopic() { return getString(notifyNextTopic); }
  
  /**
   * Set the {@code notifyNextTopic} property.
   * @see #notifyNextTopic
   */
  public void setNotifyNextTopic(String v) { setString(notifyNextTopic, v, null); }

////////////////////////////////////////////////////////////////
// Property "clientId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code clientId} property.
   * @see #getClientId
   * @see #setClientId
   */
  public static final Property clientId = newProperty(Flags.SUMMARY, "", null);
  
  /**
   * Get the {@code clientId} property.
   * @see #clientId
   */
  public String getClientId() { return getString(clientId); }
  
  /**
   * Set the {@code clientId} property.
   * @see #clientId
   */
  public void setClientId(String v) { setString(clientId, v, null); }

////////////////////////////////////////////////////////////////
// Property "mqttClientTimeout"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code mqttClientTimeout} property.
   * @see #getMqttClientTimeout
   * @see #setMqttClientTimeout
   */
  public static final Property mqttClientTimeout = newProperty(Flags.HIDDEN, 10000, BFacets.make(BFacets.make(BFacets.make(BFacets.make(BFacets.PRECISION, 0), BFacets.make(BFacets.UNITS, BUnit.getUnit("millisecond"))), BFacets.make(BFacets.MIN, 1)), BFacets.make(BFacets.MAX, 10000)));
  
  /**
   * Get the {@code mqttClientTimeout} property.
   * @see #mqttClientTimeout
   */
  public int getMqttClientTimeout() { return getInt(mqttClientTimeout); }
  
  /**
   * Set the {@code mqttClientTimeout} property.
   * @see #mqttClientTimeout
   */
  public void setMqttClientTimeout(int v) { setInt(mqttClientTimeout, v, null); }

////////////////////////////////////////////////////////////////
// Property "mqttPublishTimeout"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code mqttPublishTimeout} property.
   * @see #getMqttPublishTimeout
   * @see #setMqttPublishTimeout
   */
  public static final Property mqttPublishTimeout = newProperty(Flags.HIDDEN, 9000, BFacets.make(BFacets.make(BFacets.make(BFacets.make(BFacets.PRECISION, 0), BFacets.make(BFacets.UNITS, BUnit.getUnit("millisecond"))), BFacets.make(BFacets.MIN, 1)), BFacets.make(BFacets.MAX, 10000)));
  
  /**
   * Get the {@code mqttPublishTimeout} property.
   * @see #mqttPublishTimeout
   */
  public int getMqttPublishTimeout() { return getInt(mqttPublishTimeout); }
  
  /**
   * Set the {@code mqttPublishTimeout} property.
   * @see #mqttPublishTimeout
   */
  public void setMqttPublishTimeout(int v) { setInt(mqttPublishTimeout, v, null); }

////////////////////////////////////////////////////////////////
// Property "pauseBetweenConsecutivePublish"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code pauseBetweenConsecutivePublish} property.
   * @see #getPauseBetweenConsecutivePublish
   * @see #setPauseBetweenConsecutivePublish
   */
  public static final Property pauseBetweenConsecutivePublish = newProperty(Flags.HIDDEN, 3000, BFacets.make(BFacets.make(BFacets.make(BFacets.make(BFacets.PRECISION, 0), BFacets.make(BFacets.UNITS, BUnit.getUnit("millisecond"))), BFacets.make(BFacets.MIN, 1)), BFacets.make(BFacets.MAX, 3000)));
  
  /**
   * Get the {@code pauseBetweenConsecutivePublish} property.
   * @see #pauseBetweenConsecutivePublish
   */
  public int getPauseBetweenConsecutivePublish() { return getInt(pauseBetweenConsecutivePublish); }
  
  /**
   * Set the {@code pauseBetweenConsecutivePublish} property.
   * @see #pauseBetweenConsecutivePublish
   */
  public void setPauseBetweenConsecutivePublish(int v) { setInt(pauseBetweenConsecutivePublish, v, null); }

////////////////////////////////////////////////////////////////
// Property "certificateFile"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code certificateFile} property.
   * @see #getCertificateFile
   * @see #setCertificateFile
   */
  public static final Property certificateFile = newProperty(0, BOrd.NULL, BFacets.make(BFacets.TARGET_TYPE, "baja:IFile"));
  
  /**
   * Get the {@code certificateFile} property.
   * @see #certificateFile
   */
  public BOrd getCertificateFile() { return (BOrd)get(certificateFile); }
  
  /**
   * Set the {@code certificateFile} property.
   * @see #certificateFile
   */
  public void setCertificateFile(BOrd v) { set(certificateFile, v, null); }

////////////////////////////////////////////////////////////////
// Property "privateKeyFile"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code privateKeyFile} property.
   * @see #getPrivateKeyFile
   * @see #setPrivateKeyFile
   */
  public static final Property privateKeyFile = newProperty(0, BOrd.NULL, BFacets.make(BFacets.TARGET_TYPE, "baja:IFile"));
  
  /**
   * Get the {@code privateKeyFile} property.
   * @see #privateKeyFile
   */
  public BOrd getPrivateKeyFile() { return (BOrd)get(privateKeyFile); }
  
  /**
   * Set the {@code privateKeyFile} property.
   * @see #privateKeyFile
   */
  public void setPrivateKeyFile(BOrd v) { set(privateKeyFile, v, null); }

////////////////////////////////////////////////////////////////
// Property "payload"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code payload} property.
   * @see #getPayload
   * @see #setPayload
   */
  public static final Property payload = newProperty(0, "", BFacets.make(BFacets.MULTI_LINE, true));
  
  /**
   * Get the {@code payload} property.
   * @see #payload
   */
  public String getPayload() { return getString(payload); }
  
  /**
   * Set the {@code payload} property.
   * @see #payload
   */
  public void setPayload(String v) { setString(payload, v, null); }

////////////////////////////////////////////////////////////////
// Property "registrationPayload"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code registrationPayload} property.
   * @see #getRegistrationPayload
   * @see #setRegistrationPayload
   */
  public static final Property registrationPayload = newProperty(0, "", BFacets.make(BFacets.MULTI_LINE, true));
  
  /**
   * Get the {@code registrationPayload} property.
   * @see #registrationPayload
   */
  public String getRegistrationPayload() { return getString(registrationPayload); }
  
  /**
   * Set the {@code registrationPayload} property.
   * @see #registrationPayload
   */
  public void setRegistrationPayload(String v) { setString(registrationPayload, v, null); }

////////////////////////////////////////////////////////////////
// Property "worker"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code worker} property.
   * @see #getWorker
   * @see #setWorker
   */
  public static final Property worker = newProperty(Flags.HIDDEN, new BAwsWorker(), null);
  
  /**
   * Get the {@code worker} property.
   * @see #worker
   */
  public BAwsWorker getWorker() { return (BAwsWorker)get(worker); }
  
  /**
   * Set the {@code worker} property.
   * @see #worker
   */
  public void setWorker(BAwsWorker v) { set(worker, v, null); }

////////////////////////////////////////////////////////////////
// Action "publish"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code publish} action.
   * @see #publish()
   */
  public static final Action publish = newAction(Flags.ASYNC, null);
  
  /**
   * Invoke the {@code publish} action.
   * @see #publish
   */
  public BBoolean publish() { return (BBoolean)invoke(publish, null, null); }

////////////////////////////////////////////////////////////////
// Action "registration"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code registration} action.
   * @see #registration()
   */
  public static final Action registration = newAction(Flags.ASYNC, null);
  
  /**
   * Invoke the {@code registration} action.
   * @see #registration
   */
  public BBoolean registration() { return (BBoolean)invoke(registration, null, null); }

////////////////////////////////////////////////////////////////
// Action "setupMqttClient"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code setupMqttClient} action.
   * @see #setupMqttClient()
   */
  public static final Action setupMqttClient = newAction(Flags.ASYNC, null);
  
  /**
   * Invoke the {@code setupMqttClient} action.
   * @see #setupMqttClient
   */
  public BBoolean setupMqttClient() { return (BBoolean)invoke(setupMqttClient, null, null); }

////////////////////////////////////////////////////////////////
// Topic "newMessageOnNotifyNextTopic"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code newMessageOnNotifyNextTopic} topic.
   * @see #fireNewMessageOnNotifyNextTopic
   */
  public static final Topic newMessageOnNotifyNextTopic = newTopic(0, null);
  
  /**
   * Fire an event for the {@code newMessageOnNotifyNextTopic} topic.
   * @see #newMessageOnNotifyNextTopic
   */
  public void fireNewMessageOnNotifyNextTopic(BString event) { fire(newMessageOnNotifyNextTopic, event, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAwsGatewayInterface.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  @Override
  public void started() throws Exception
  {
  }

  @Override
  public void descendantsStarted() throws Exception {
    super.descendantsStarted();

    //BAbsTime future =  BAbsTime.now().add(BRelTime.makeSeconds(3)); //3 second delay
    //Clock.schedule(this, future, setupMqttClient , null);
  }

  public void subscribedTopicOnMessage(MyTopic topic, AWSIotMessage message)
  {
    logger.fine("subscribedTopicOnMessage " + topic.getTopic() + ":\n" + message.getStringPayload());
    if (topic.getTopic().endsWith("notify-next"))
    {
      logger.fine("Message received from " + topic.getTopic() + ":\n" + message.getStringPayload());

      try
      {
        JSONObject jsonMessage = new JSONObject(message.getStringPayload());
        logger.fine("timestamp " + jsonMessage.getString("timestamp"));
        JSONObject jsonExecution = new JSONObject( jsonMessage.getString("execution") );
        logger.fine("jobId " + jsonExecution.getString("jobId"));
        logger.fine("status " + jsonExecution.getString("status"));
        JSONObject jsonJobDocument = new JSONObject( jsonExecution.getString("jobDocument") );
        logger.fine("command " + jsonJobDocument.getString("command"));
      }
      catch(JSONException e)
      {
        e.printStackTrace();
      }

    }

    fireNewMessageOnNotifyNextTopic(BString.make(message.getStringPayload()));

  }

  public BBoolean doRegistration()
  {
    return BBoolean.make( registrationProgrammatically( getRegistrationTopic(), getRegistrationPayload(), getMqttPublishTimeout() ) );
  }

  private boolean registrationProgrammatically(String mqttTopic, String mqttPayload, int publishTimeout)
  {
    boolean first = publishProgrammatically(mqttTopic, mqttPayload, publishTimeout);
    try
    {
      Thread.sleep( getPauseBetweenConsecutivePublish());
    } catch (InterruptedException e)
    {
      e.printStackTrace();
    }
    boolean second = publishProgrammatically(mqttTopic, mqttPayload, publishTimeout);
    return first && second;
  }

  public BBoolean doSetupMqttClient()
  {
    return BBoolean.make( setupMqttClientProgrammatically(getCertificateFile().toString(), getPrivateKeyFile().toString(), getEndPoint(), getMqttClientTimeout(), getNotifyTopic(), getNotifyNextTopic()) );
  }

  private boolean setupMqttClientProgrammatically(String certificateNiagaraFileOrd, String privateKeyNiagaraFileOrd, String endPointIot, int clientTimeout, String notifyTopicName, String notifyNextTopicName)
  {
    BOrd myCertificateNiagaraFileOrd = BOrd.make(certificateNiagaraFileOrd);
    BOrd myPrivateKeyNiagaraFileOrd = BOrd.make(privateKeyNiagaraFileOrd);

    //Create key store password pair
    File certificateFile = BFileSystem.INSTANCE.pathToLocalFile(( (BIFile)myCertificateNiagaraFileOrd.get(this) ).getFilePath());
    File privateKeyFile = BFileSystem.INSTANCE.pathToLocalFile(( (BIFile)myPrivateKeyNiagaraFileOrd.get(this) ).getFilePath());
    SampleUtil.KeyStorePasswordPair pair = SampleUtil.getKeyStorePasswordPair(certificateFile.getPath(), privateKeyFile.getPath());

    if (awsMqttClient == null)
    {
      String srtClientID = getClientId();
      if (srtClientID == null || srtClientID == "")
      {
        srtClientID = Long.toString(Instant.now().toEpochMilli());
        logger.warning("Client id null or empty.");
      }
      awsMqttClient = new AWSIotMqttClient(endPointIot, srtClientID, pair.keyStore, pair.keyPassword);
    }
    logger.fine("AWSIotMqtt client created");
    awsMqttClient.setMaxConnectionRetries(3);
    //Connection
    logger.fine("AWS IoT connection status: " + awsMqttClient.getConnection().toString());
    try
    {
      if(awsMqttClient.getConnectionStatus().equals(AWSIotConnectionStatus.DISCONNECTED))
        awsMqttClient.connect(clientTimeout);
      logger.fine("AWSIotMqtt client connected");
    } catch (AWSIotException | AWSIotTimeoutException e)
    {
      e.printStackTrace();
      return false;
    }

    try {
      vNotifyTopic = new MyTopic(notifyTopicName, QOS);
      try {
        awsMqttClient.subscribe(vNotifyTopic);
      } catch (AWSIotException e) {
        e.printStackTrace();
      }

      vNotifyNextTopic = new MyTopic(notifyNextTopicName, QOS);
      try {
        awsMqttClient.subscribe(vNotifyNextTopic);
      } catch (AWSIotException e) {
        e.printStackTrace();
      }
      //TODO
      ConcurrentMap<String, AWSIotTopic> subscriptions = awsMqttClient.getSubscriptions();
      String logStr = "Subscriptions:\n";
      for (ConcurrentMap.Entry<String, AWSIotTopic> entry : subscriptions.entrySet())
        logStr = logStr.concat("Key = " + entry.getKey() + ", Value = " + entry.getValue() + "\n");
      logger.fine(logStr);
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    return true;
  }


  /**
   * Publish action method
   * @return publish successfulor not
   */
  public BBoolean doPublish()
  {
    return BBoolean.make( publishProgrammatically(getTopic(), getPayload(), getMqttPublishTimeout() ) );
  }

  /**
   *
   * @param mqttTopic
   * @param mqttPayload
   * @param publishTimeout
   * @return
   */
  public boolean publishProgrammatically(String mqttTopic, String mqttPayload, int publishTimeout)
  {
    //Crete the message
    MyMessage message = new MyMessage(mqttTopic, QOS, mqttPayload);

    //Publish the message
    try
    {
      awsMqttClient.publish(message, publishTimeout);
      logger.fine("Message published");
    } catch (AWSIotException e)
    {
      e.printStackTrace();
      return false;
    }

    return true;
  }

  /**
   * This is a callback when an async action is invoked. It gives
   * subclasses a chance to manage the invocation using their own
   * queues and threading models. We have overridden the callback
   * here in order to post the execute action onto our own worker
   * for async execution.
   */
  @Override
  public IFuture post(Action action, BValue argument, Context cx)
  {
    getWorker().postAsync(new Invocation(this, action, argument, cx));
    return null;
  }

  /**
   * Inner class to manage topics
   */
  public class MyTopic extends AWSIotTopic
  {
    public MyTopic(String topic, AWSIotQos qos)
    {
      super(topic, qos);
    }

    @Override
    public void onMessage(AWSIotMessage message)
    {
      // called when a message is received
      //logger.fine("MyTopic -  onMessage - Message received from " + this.getTopic() + ":\n" + message.getStringPayload());
      subscribedTopicOnMessage(this, message);
    }
  }
  /**
   * Inner class to manage messages
   */
  private class MyMessage extends AWSIotMessage
  {
    public MyMessage(String topic, AWSIotQos qos, String payload) {
      super(topic, qos, payload);
    }

    @Override
    public void onSuccess() {
      // called when message publishing succeeded
    }

    @Override
    public void onFailure() {
      // called when message publishing failed
    }

    @Override
    public void onTimeout() {
      // called when message publishing timed out
    }

  }

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

  @Override
  public BIcon getIcon() {
    return BIcon.std("device.png");
  }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

  MyTopic vNotifyTopic = null;
  MyTopic vNotifyNextTopic = null;
  AWSIotMqttClient awsMqttClient = null;
  private static final Logger logger = Logger.getLogger("AwsGatewayInterface");
  private static final AWSIotQos QOS = AWSIotQos.QOS0;

}
