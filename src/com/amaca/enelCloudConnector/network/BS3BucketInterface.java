package com.amaca.enelCloudConnector.network;

import com.amaca.enelCloudConnector.network.worker.BAwsWorker;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import javax.baja.file.BFileSystem;
import javax.baja.file.FilePath;
import javax.baja.naming.BOrd;
import javax.baja.nre.annotations.Facet;
import javax.baja.nre.annotations.NiagaraAction;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.security.BPassword;
import javax.baja.sys.*;
import javax.baja.util.IFuture;
import javax.baja.util.Invocation;
import java.io.*;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.logging.Logger;

@NiagaraProperty(
        name = "bucketName",
        type = "String",
        defaultValue = "\"\"",
        flags = Flags.SUMMARY
)

@NiagaraProperty(
        name = "keyName",
        type = "String",
        defaultValue = "\"\"",
        flags = Flags.SUMMARY
)

@NiagaraProperty(
        name = "region",
        type = "String",
        defaultValue = "\"eu-central-1\"",
        flags = Flags.SUMMARY
)

@NiagaraProperty(
        name = "accessKey",
        type = "BPassword",
        defaultValue = "BPassword.DEFAULT"
)

@NiagaraProperty(
        name = "secretKey",
        type = "BPassword",
        defaultValue = "BPassword.DEFAULT"
)

@NiagaraProperty(
        name = "certificatePath",
        type = "baja:Ord",
        defaultValue = "BOrd.make(\"file:^Certificates\")",
        facets =
                {
                        @Facet(name = "BFacets.TARGET_TYPE", value = "\"baja:IFolder\"")
                }
)

@NiagaraProperty(
        name = "downloadCertificatesAutomatically",
        type = "boolean",
        defaultValue = "true"
)

@NiagaraProperty(
        name = "worker",
        type = "BAwsWorker",
        defaultValue= "new BAwsWorker()",
        flags = Flags.HIDDEN
)

@NiagaraProperty(
        name = "collectLogsRelativePath",
        type = "String",
        defaultValue = "\"\""
)

@NiagaraAction(
        name = "downloadCertificates",
        returnType = "baja:Boolean",
        flags = Flags.ASYNC
)

@NiagaraAction(
        name = "uploadFile",
        parameterType="baja:String",
        defaultValue = "BString.make(\"\")",
        returnType = "baja:Boolean",
        flags = Flags.ASYNC
)

@NiagaraType
public class BS3BucketInterface extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.network.BS3BucketInterface(3209053798)1.0$ @*/
/* Generated Thu May 07 14:46:55 CEST 2020 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "bucketName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code bucketName} property.
   * @see #getBucketName
   * @see #setBucketName
   */
  public static final Property bucketName = newProperty(Flags.SUMMARY, "", null);
  
  /**
   * Get the {@code bucketName} property.
   * @see #bucketName
   */
  public String getBucketName() { return getString(bucketName); }
  
  /**
   * Set the {@code bucketName} property.
   * @see #bucketName
   */
  public void setBucketName(String v) { setString(bucketName, v, null); }

////////////////////////////////////////////////////////////////
// Property "keyName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code keyName} property.
   * @see #getKeyName
   * @see #setKeyName
   */
  public static final Property keyName = newProperty(Flags.SUMMARY, "", null);
  
  /**
   * Get the {@code keyName} property.
   * @see #keyName
   */
  public String getKeyName() { return getString(keyName); }
  
  /**
   * Set the {@code keyName} property.
   * @see #keyName
   */
  public void setKeyName(String v) { setString(keyName, v, null); }

////////////////////////////////////////////////////////////////
// Property "region"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code region} property.
   * @see #getRegion
   * @see #setRegion
   */
  public static final Property region = newProperty(Flags.SUMMARY, "eu-central-1", null);
  
  /**
   * Get the {@code region} property.
   * @see #region
   */
  public String getRegion() { return getString(region); }
  
  /**
   * Set the {@code region} property.
   * @see #region
   */
  public void setRegion(String v) { setString(region, v, null); }

////////////////////////////////////////////////////////////////
// Property "accessKey"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code accessKey} property.
   * @see #getAccessKey
   * @see #setAccessKey
   */
  public static final Property accessKey = newProperty(0, BPassword.DEFAULT, null);
  
  /**
   * Get the {@code accessKey} property.
   * @see #accessKey
   */
  public BPassword getAccessKey() { return (BPassword)get(accessKey); }
  
  /**
   * Set the {@code accessKey} property.
   * @see #accessKey
   */
  public void setAccessKey(BPassword v) { set(accessKey, v, null); }

////////////////////////////////////////////////////////////////
// Property "secretKey"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code secretKey} property.
   * @see #getSecretKey
   * @see #setSecretKey
   */
  public static final Property secretKey = newProperty(0, BPassword.DEFAULT, null);
  
  /**
   * Get the {@code secretKey} property.
   * @see #secretKey
   */
  public BPassword getSecretKey() { return (BPassword)get(secretKey); }
  
  /**
   * Set the {@code secretKey} property.
   * @see #secretKey
   */
  public void setSecretKey(BPassword v) { set(secretKey, v, null); }

////////////////////////////////////////////////////////////////
// Property "certificatePath"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code certificatePath} property.
   * @see #getCertificatePath
   * @see #setCertificatePath
   */
  public static final Property certificatePath = newProperty(0, BOrd.make("file:^Certificates"), BFacets.make(BFacets.TARGET_TYPE, "baja:IFolder"));
  
  /**
   * Get the {@code certificatePath} property.
   * @see #certificatePath
   */
  public BOrd getCertificatePath() { return (BOrd)get(certificatePath); }
  
  /**
   * Set the {@code certificatePath} property.
   * @see #certificatePath
   */
  public void setCertificatePath(BOrd v) { set(certificatePath, v, null); }

////////////////////////////////////////////////////////////////
// Property "downloadCertificatesAutomatically"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code downloadCertificatesAutomatically} property.
   * @see #getDownloadCertificatesAutomatically
   * @see #setDownloadCertificatesAutomatically
   */
  public static final Property downloadCertificatesAutomatically = newProperty(0, true, null);
  
  /**
   * Get the {@code downloadCertificatesAutomatically} property.
   * @see #downloadCertificatesAutomatically
   */
  public boolean getDownloadCertificatesAutomatically() { return getBoolean(downloadCertificatesAutomatically); }
  
  /**
   * Set the {@code downloadCertificatesAutomatically} property.
   * @see #downloadCertificatesAutomatically
   */
  public void setDownloadCertificatesAutomatically(boolean v) { setBoolean(downloadCertificatesAutomatically, v, null); }

////////////////////////////////////////////////////////////////
// Property "worker"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code worker} property.
   * @see #getWorker
   * @see #setWorker
   */
  public static final Property worker = newProperty(Flags.HIDDEN, new BAwsWorker(), null);
  
  /**
   * Get the {@code worker} property.
   * @see #worker
   */
  public BAwsWorker getWorker() { return (BAwsWorker)get(worker); }
  
  /**
   * Set the {@code worker} property.
   * @see #worker
   */
  public void setWorker(BAwsWorker v) { set(worker, v, null); }

////////////////////////////////////////////////////////////////
// Property "collectLogsRelativePath"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code collectLogsRelativePath} property.
   * @see #getCollectLogsRelativePath
   * @see #setCollectLogsRelativePath
   */
  public static final Property collectLogsRelativePath = newProperty(0, "", null);
  
  /**
   * Get the {@code collectLogsRelativePath} property.
   * @see #collectLogsRelativePath
   */
  public String getCollectLogsRelativePath() { return getString(collectLogsRelativePath); }
  
  /**
   * Set the {@code collectLogsRelativePath} property.
   * @see #collectLogsRelativePath
   */
  public void setCollectLogsRelativePath(String v) { setString(collectLogsRelativePath, v, null); }

////////////////////////////////////////////////////////////////
// Action "downloadCertificates"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code downloadCertificates} action.
   * @see #downloadCertificates()
   */
  public static final Action downloadCertificates = newAction(Flags.ASYNC, null);
  
  /**
   * Invoke the {@code downloadCertificates} action.
   * @see #downloadCertificates
   */
  public BBoolean downloadCertificates() { return (BBoolean)invoke(downloadCertificates, null, null); }

////////////////////////////////////////////////////////////////
// Action "uploadFile"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code uploadFile} action.
   * @see #uploadFile(BString parameter)
   */
  public static final Action uploadFile = newAction(Flags.ASYNC, BString.make(""), null);
  
  /**
   * Invoke the {@code uploadFile} action.
   * @see #uploadFile
   */
  public BBoolean uploadFile(BString parameter) { return (BBoolean)invoke(uploadFile, parameter, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BS3BucketInterface.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  /**
   *
   * @param awsAccessKey AWS access key
   * @param awsSecretKey AWS secret key
   * @param awsBucketName AS bucket name e.g enel-noprod-esol-ap00000-iotmvp
   * @param awsKeyName AWS key name e.g. dev/private/esol_ap00000_dev_ABC01/ABC01.tar.gz
   * @param awsRegion AWS region
   * @param certificateNiagaraFolderOrd e.g. file:^Folder1/Folder2/../FolderN
   * @return download and extraction of the certificates successful or not
   */
  public boolean downloadCertificatesProgrammatically (String awsAccessKey, String awsSecretKey, String awsBucketName, String awsKeyName, String awsRegion, String certificateNiagaraFolderOrd)
  {
    //Authenticate requests against Amazon S3, programmatically set credentials
    BasicAWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
    //Create S3 client
    AmazonS3 s3Client = createS3Client(awsCredentials, awsRegion);
    //If S3Client is null then exit
    if (s3Client == null)
      return false;
    logger.fine("Amazon S3 client created successfully.");

    //Check if the bucket exists
    if (s3Client.doesBucketExistV2(awsBucketName) == false)
    {
      logger.severe("The bucket " + awsBucketName + " does not exist.");
      return false;
    }
    logger.fine("The bucket " + awsBucketName + " exists.");

    //Download certificate
    String absS3ObjectPath = getS3Object(s3Client, certificateNiagaraFolderOrd, awsBucketName, awsKeyName);
    if (absS3ObjectPath == null)
      return false;
    logger.fine("Certificates downloaded successfully.");

    //Extract the certificates
    if(extractTarGzFile(absS3ObjectPath, getS3ObjectFileName(awsKeyName)) == false)
      return false;
    logger.fine("Certificates unarchived successfully.");

    return true;
  }


  /**
   * Download X.509 certificates from specified S3 bucket
   */
  public BBoolean doDownloadCertificates()
  {
    return BBoolean.make( downloadCertificatesProgrammatically( getAccessKey().getValue(), getSecretKey().getValue(), getBucketName(), getKeyName(), getRegion(), getCertificatePath().toString() ) );
  }

  /**
   *
   * @param awsCredentials
   * @param awsRegion
   * @return
   */
  private AmazonS3 createS3Client(BasicAWSCredentials awsCredentials, String awsRegion)
  {
    final AmazonS3[] s3Client = {null};
    try
    {

      AccessController.doPrivileged(
              new PrivilegedAction()
              {
                public Object run()
                {
                  s3Client[0] = AmazonS3ClientBuilder.standard()

                          .withRegion(awsRegion)

                          .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))

                          .build();
                  return null;
                }
              }
      );
    }catch (Exception e)
    {
      s3Client[0] = null;
      logger.severe("Unable to create the Amazon S3 client");
      e.printStackTrace();
      logger.severe("---");
    }
    return s3Client[0];
  }

  /**
   *
   * @param s3Client
   * @param certificateNiagaraFolderOrd
   * @param awsBucketName
   * @param awsKeyName
   * @return
   */
  private String getS3Object(AmazonS3 s3Client, String certificateNiagaraFolderOrd, String awsBucketName, String awsKeyName)
  {
    //Create folder for the certificates
    String absStationCertificateFolderPath = STATION_HOME_ROOT.concat(getRelativeFilePath(certificateNiagaraFolderOrd));
    try
    {
      BFileSystem.INSTANCE.delete(new FilePath(absStationCertificateFolderPath));
      BFileSystem.INSTANCE.makeDir(new FilePath(absStationCertificateFolderPath));
    } catch(IOException e)
    {
      logger.severe("Unable to delete/create the folder for the certificates.");
      e.printStackTrace();
      logger.severe("---");
      return null;
    }
    String certificateArchiveFileName = getS3ObjectFileName(awsKeyName);
    String certificateArchiveAbsoluteFolderPath = Sys.getStationHome().toString().concat(File.separator).concat((getRelativeFilePath(certificateNiagaraFolderOrd)));
    String certificateArchiveAbsoluteFilePath = certificateArchiveAbsoluteFolderPath.concat(File.separator).concat(certificateArchiveFileName);
    S3Object fullObject;
    try
    {
      fullObject = s3Client.getObject(new GetObjectRequest(awsBucketName, awsKeyName));
      S3ObjectInputStream s3is = fullObject.getObjectContent();
      FileOutputStream fos = new FileOutputStream(new File(certificateArchiveAbsoluteFilePath));
      byte[] read_buf = new byte[BUFFER_SIZE];
      int read_len = 0;
      while ((read_len = s3is.read(read_buf)) > 0)
      {
        fos.write(read_buf, 0, read_len);
      }
      s3is.close();
      fos.close();
    } catch (AmazonServiceException | IOException e)
    {
      logger.severe("Unable to download the certificates.");
      e.printStackTrace();
      logger.severe("---");
      return null;
    }

    //Close the S3 object
    if (fullObject != null)
    {
      try
      {
        fullObject.close();
      } catch (IOException e)
      {
        logger.severe("Unable to close the S3 object.");
        e.printStackTrace();
        logger.severe("---");
        return null;
      }
    }
    return certificateArchiveAbsoluteFolderPath;
  }


  /**
   *
   * @param folderPath
   * @param fileName
   * @return
   */
  private boolean extractTarGzFile(String folderPath, String fileName)
  {
    InputStream compressedStream = null;
    try
    {
      compressedStream = new FileInputStream( new File( folderPath.concat(File.separator).concat(fileName) ) );
    } catch (FileNotFoundException e)
    {
      logger.severe("Certificate archive file not found.");
      e.printStackTrace();
      logger.severe("---");
      return false;
    }
    GzipCompressorInputStream gzipIn = null;
    try
    {
      gzipIn = new GzipCompressorInputStream(compressedStream);
    } catch (IOException e)
    {
      logger.severe("Certificate archive file input stream not created.");
      e.printStackTrace();
      logger.severe("---");
      return false;
    }
    try
    {
      try (TarArchiveInputStream tarIn = new TarArchiveInputStream(gzipIn))
      {
        TarArchiveEntry entry;
        while ((entry = (TarArchiveEntry) tarIn.getNextEntry()) != null)
        {
          /** If the entry is a directory, create the directory. **/
          if (entry.isDirectory()) {
            File f = new File(entry.getName());
            boolean created = f.mkdir();
            if (!created)
            {
              logger.severe("Unable to create directory '%s', during extraction of archive contents.\n" + f.getAbsolutePath());
              return false;
            }
          }
          else
          {
            int count;
            byte data[] = new byte[BUFFER_SIZE];
            FileOutputStream fos = null;
            try
            {
              fos = new FileOutputStream(folderPath.concat(File.separator).concat(entry.getName()), false);
            } catch (FileNotFoundException e)
            {
              logger.severe("Certificate archive file output stream not created.");
              e.printStackTrace();
              logger.severe("---");
              return false;
            }
            try
            {
              try (BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER_SIZE)) {
                while ((count = tarIn.read(data, 0, BUFFER_SIZE)) != -1) {
                  dest.write(data, 0, count);
                }
              }
            } catch (IOException e)
            {
              logger.severe("Certificate archive file buffered output stream not created.");
              e.printStackTrace();
              logger.severe("---");
              return false;
            }
          }
        }
      }
    } catch (IOException e)
    {
      e.printStackTrace();
    }
    return true;
  }

  /**
   * Action handler upload a file into S3 bucket
   * @param textToUplaod text to uploaded
   * @return <code>true</code> if the file has been uploaded successfully; <code>false</code> otherwise.
   */
  public BBoolean doUploadFile(BString textToUplaod)
  {
    return BBoolean.make(uploadFileProgrammatically(getAccessKey().getValue(), getSecretKey().getValue(), getBucketName(), getCollectLogsRelativePath(), getRegion(), textToUplaod.getString() ));
  }

  /**
   * Upload a file into S3 bucket
   * @param textToUplaod text to uploaded
   * @return if the file has been uploaded successfully; <code>false</code> otherwise.
   */
  public boolean uploadFileProgrammatically(String awsAccessKey, String awsSecretKey, String awsBucketName, String collectLogsRelPath, String awsRegion, String textToUplaod)
  {
    //https://docs.aws.amazon.com/AmazonS3/latest/dev/UploadObjSingleOpJava.html

//    //Get the reference to the file to uplaod
//    File fileToUpload = null;
//    try
//    {
//      fileToUpload = new File(absoluteFilePath);
//    }
//    catch (Exception e)
//    {
//      logger.warning("Error during retrieving the file to upload.");
//      e.printStackTrace();
//      return false;
//    }

    //Authenticate requests against Amazon S3, programmatically set credentials
    BasicAWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
    //Create S3 client
    AmazonS3 s3Client = createS3Client(awsCredentials, awsRegion);
    //If S3Client is null then exit
    if (s3Client == null)
      return false;
    logger.fine("Amazon S3 client created successfully.");

    awsBucketName = awsBucketName.concat(collectLogsRelPath);
    //Check if the bucket exists
    if (s3Client.doesBucketExistV2(awsBucketName) == false)
    {
      logger.severe("The bucket " + awsBucketName + " does not exist.");
      return false;
    }
    logger.fine("The bucket " + awsBucketName + " exists.");

    // Upload a file as a new object with ContentType and title specified.
    try
    {

      String stringObjKeyName = "Log_" + BAbsTime.now().getDate() + "_" + BAbsTime.now().getTime();
      // Upload a text string as a new object.
      s3Client.putObject(awsBucketName, stringObjKeyName, textToUplaod);

//      // Upload a text string as a new object.
//      PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName, new File(fileName));
//      ObjectMetadata metadata = new ObjectMetadata();
//      metadata.setContentType("plain/text");
//      metadata.addUserMetadata("title", "someTitle");
//      request.setMetadata(metadata);
//      s3Client.putObject(request);
    }
    catch (AmazonServiceException e) {
    // The call was transmitted successfully, but Amazon S3 couldn't process
    // it, so it returned an error response.
      e.printStackTrace();
      return false;
    }
    catch (SdkClientException e) {
      // Amazon S3 couldn't be contacted for a response, or the client
      // couldn't parse the response from Amazon S3.
      e.printStackTrace();
      return false;
    }

    return true;
  }
  /**
   *
   * @param ord
   * @return
   */
  private String getRelativeFilePath(String ord)
  {
    return ord.replace("file:^","");
  }

  /**
   *
   * @param awsKey
   * @return
   */
  private String getS3ObjectFileName(String awsKey)
  {
    return awsKey.split(AWS_SEPARATOR)[awsKey.split(AWS_SEPARATOR).length-1];
  }

  /**
   * This is a callback when an async action is invoked. It gives
   * subclasses a chance to manage the invocation using their own
   * queues and threading models. We have overridden the callback
   * here in order to post the execute action onto our own worker
   * for async execution.
   */
  @Override
  public IFuture post(Action action, BValue argument, Context cx)
  {
    getWorker().postAsync(new Invocation(this, action, argument, cx));
    return null;
  }

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

  @Override
  public BIcon getIcon() {
    return BIcon.std("license.png");
  }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
  private static final Logger logger = Logger.getLogger("S3BucketInterface");
  private static final int BUFFER_SIZE = 1024;
  private static final String STATION_HOME_ROOT = "^";
  private static final String AWS_SEPARATOR = "/";
}
