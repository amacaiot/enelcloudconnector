package com.amaca.enelCloudConnector.network;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Amaca on 07/10/2018.
 */
@NiagaraProperty(
        name = "requestTimeout",
        type = "baja:RelTime",
        defaultValue = "BRelTime.makeSeconds(10)"
)
@NiagaraType
public class BEnelCloudEnelPostRequestsManager extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.network.BEnelCloudEnelPostRequestsManager(762693910)1.0$ @*/
/* Generated Sun Oct 07 16:09:54 CEST 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "requestTimeout"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code requestTimeout} property.
   * @see #getRequestTimeout
   * @see #setRequestTimeout
   */
  public static final Property requestTimeout = newProperty(0, BRelTime.makeSeconds(10), null);
  
  /**
   * Get the {@code requestTimeout} property.
   * @see #requestTimeout
   */
  public BRelTime getRequestTimeout() { return (BRelTime)get(requestTimeout); }
  
  /**
   * Set the {@code requestTimeout} property.
   * @see #requestTimeout
   */
  public void setRequestTimeout(BRelTime v) { set(requestTimeout, v, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudEnelPostRequestsManager.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

 ////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    // empty constructor
    public BEnelCloudEnelPostRequestsManager()
    {
        super();
    }


////////////////////////////////////////////////////////////////
// POST
////////////////////////////////////////////////////////////////

    public int sendPostRequest(String url, String body) throws Exception
    {
        // define result status code
        int resultCode = 999;

        // create an HTTPClient instance
        CloseableHttpClient httpclient = HttpClients.createDefault();

        //log
        if (logger.isLoggable(Level.ALL))
        {
            logger.fine("Post request URL: " + url);
            logger.fine("Post request body: " + body);
        }

        try
        {
            // make the post request an set the url
            HttpPost httpPost = new HttpPost(url);
            // set header parameters
            httpPost.setHeader("Content-Type", "multipart/form-data; boundary=--------------------------817633883606245657651307");
            //build the body

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try(ZipOutputStream zos = new ZipOutputStream(baos)) {
                ZipEntry entry = new ZipEntry("HistoryData_" + BAbsTime.now().getMillis() +".json");
                zos.putNextEntry(entry);
                zos.write(body.getBytes());
                zos.closeEntry();
            } catch(IOException ioe) {
                ioe.printStackTrace();
            }
  //          MultipartEntity form = new MultipartEntity();
            //ContentBody cd = new InputStreamBody(new ByteArrayInputStream(baos.toByteArray()), "HistoryData.zip");
//            form.addPart("datafile1", new InputStreamBody(new ByteArrayInputStream(baos.toByteArray()), "HistoryData.zip"));

            // set the body
            MultipartEntityBuilder multipartEntityBuilder  = MultipartEntityBuilder.create();
            //System.out.println("FASE 2.1");
            multipartEntityBuilder.addBinaryBody("datafile1", baos.toByteArray(), ContentType.create("application/x-zip-compressed"), "HistoryData.zip");
            //System.out.println("FASE 2.2");
            HttpEntity httpEntity = multipartEntityBuilder.setBoundary("--------------------------817633883606245657651307").build();
            //System.out.println("FASE 2.3");
            httpPost.setEntity(httpEntity);
            //System.out.println("FASE 2.4");

            //System.out.println("BODY RICHIESTA COMPLETA --> " + EntityUtils.toString(httpEntity));
            // set the timeout of the request
            RequestConfig.Builder reqBuilder = RequestConfig.custom();
            reqBuilder.setSocketTimeout((int)getRequestTimeout().getMillis());
            httpPost.setConfig(reqBuilder.build());
            // execute the http post request and get the response
            //System.out.println("LANCIO EXCEUTE");
            CloseableHttpResponse response = httpclient.execute(httpPost);
            try
            {
                // update the result status code
                resultCode = response.getStatusLine().getStatusCode();
                if (logger.isLoggable(Level.FINE))
                    logger.fine("Response status line: " + response.getStatusLine());
                HttpEntity entity = response.getEntity();
                //EntityUtils.consume(entity);
                //System.out.println("RISPOSTA COMPLETA --> " + EntityUtils.toString(entity));
            }
            catch (Exception e)
            {
                if (logger.isLoggable(Level.FINE))
                    logger.fine("Exception: " + e.getMessage());
            }
            finally
            {
                response.close(); // always try to close the connection
            }
        }
        catch (Exception e)
        {
            if (logger.isLoggable(Level.FINE))
                logger.fine("Exception: " + e.getMessage());
            if (e instanceof ConnectTimeoutException)
                resultCode = 504;
            e.printStackTrace();
        }
        finally
        {
            httpclient.close(); // always close the connection.
        }

        return resultCode;
    }

    ////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("gears.png");
    }

    ////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
    public static final Logger logger = Logger.getLogger("CloudLabEnelPostRequestsManager");
}
