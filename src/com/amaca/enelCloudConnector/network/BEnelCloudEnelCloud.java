package com.amaca.enelCloudConnector.network;

import com.tridium.json.JSONObject;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.Header;

import javax.baja.nre.annotations.NiagaraAction;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Amaca on 07/10/2018.
 */
@NiagaraProperty(
        name = "enabled",
        type = "boolean",
        defaultValue = "false"
)
@NiagaraProperty(
        name = "enelCloudUrl",
        type = "baja:String",
        defaultValue = "https://demsitaa.enel.com/secure"
)
@NiagaraProperty(
        name = "accessTokenUrl",
        type = "baja:String",
        defaultValue = "https://demsitaa.enel.com/token"
)
@NiagaraProperty(
        name = "username",
        type = "baja:String",
        defaultValue = "testctrl"
)
@NiagaraProperty(
        name = "password",
        type = "baja:String",
        defaultValue = "testctrl"
)
@NiagaraProperty(
        name = "clientId",
        type = "baja:String",
        defaultValue = "controlroom.testctrl"
)
@NiagaraProperty(
        name = "clientSecret",
        type = "baja:String",
        defaultValue = "n&jasPUC1@h241C5!$Lr"
)
@NiagaraProperty(
        name = "accessToken",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = ""
)
@NiagaraAction(
        name="refreshToken"
)
@NiagaraType
public class BEnelCloudEnelCloud extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.network.BEnelCloudEnelCloud(2896769273)1.0$ @*/
/* Generated Sun Oct 07 16:29:29 CEST 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "enabled"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code enabled} property.
   * @see #getEnabled
   * @see #setEnabled
   */
  public static final Property enabled = newProperty(0, false, null);
  
  /**
   * Get the {@code enabled} property.
   * @see #enabled
   */
  public boolean getEnabled() { return getBoolean(enabled); }
  
  /**
   * Set the {@code enabled} property.
   * @see #enabled
   */
  public void setEnabled(boolean v) { setBoolean(enabled, v, null); }

////////////////////////////////////////////////////////////////
// Property "enelCloudUrl"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code enelCloudUrl} property.
   * @see #getEnelCloudUrl
   * @see #setEnelCloudUrl
   */
  public static final Property enelCloudUrl = newProperty(0, "https://demsitaa.enel.com/secure", null);
  
  /**
   * Get the {@code enelCloudUrl} property.
   * @see #enelCloudUrl
   */
  public String getEnelCloudUrl() { return getString(enelCloudUrl); }
  
  /**
   * Set the {@code enelCloudUrl} property.
   * @see #enelCloudUrl
   */
  public void setEnelCloudUrl(String v) { setString(enelCloudUrl, v, null); }

////////////////////////////////////////////////////////////////
// Property "accessTokenUrl"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code accessTokenUrl} property.
   * @see #getAccessTokenUrl
   * @see #setAccessTokenUrl
   */
  public static final Property accessTokenUrl = newProperty(0, "https://demsitaa.enel.com/token", null);
  
  /**
   * Get the {@code accessTokenUrl} property.
   * @see #accessTokenUrl
   */
  public String getAccessTokenUrl() { return getString(accessTokenUrl); }
  
  /**
   * Set the {@code accessTokenUrl} property.
   * @see #accessTokenUrl
   */
  public void setAccessTokenUrl(String v) { setString(accessTokenUrl, v, null); }

////////////////////////////////////////////////////////////////
// Property "username"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code username} property.
   * @see #getUsername
   * @see #setUsername
   */
  public static final Property username = newProperty(0, "testctrl", null);
  
  /**
   * Get the {@code username} property.
   * @see #username
   */
  public String getUsername() { return getString(username); }
  
  /**
   * Set the {@code username} property.
   * @see #username
   */
  public void setUsername(String v) { setString(username, v, null); }

////////////////////////////////////////////////////////////////
// Property "password"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code password} property.
   * @see #getPassword
   * @see #setPassword
   */
  public static final Property password = newProperty(0, "testctrl", null);
  
  /**
   * Get the {@code password} property.
   * @see #password
   */
  public String getPassword() { return getString(password); }
  
  /**
   * Set the {@code password} property.
   * @see #password
   */
  public void setPassword(String v) { setString(password, v, null); }

////////////////////////////////////////////////////////////////
// Property "clientId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code clientId} property.
   * @see #getClientId
   * @see #setClientId
   */
  public static final Property clientId = newProperty(0, "controlroom.testctrl", null);
  
  /**
   * Get the {@code clientId} property.
   * @see #clientId
   */
  public String getClientId() { return getString(clientId); }
  
  /**
   * Set the {@code clientId} property.
   * @see #clientId
   */
  public void setClientId(String v) { setString(clientId, v, null); }

////////////////////////////////////////////////////////////////
// Property "clientSecret"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code clientSecret} property.
   * @see #getClientSecret
   * @see #setClientSecret
   */
  public static final Property clientSecret = newProperty(0, "n&jasPUC1@h241C5!$Lr", null);
  
  /**
   * Get the {@code clientSecret} property.
   * @see #clientSecret
   */
  public String getClientSecret() { return getString(clientSecret); }
  
  /**
   * Set the {@code clientSecret} property.
   * @see #clientSecret
   */
  public void setClientSecret(String v) { setString(clientSecret, v, null); }

////////////////////////////////////////////////////////////////
// Property "accessToken"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code accessToken} property.
   * @see #getAccessToken
   * @see #setAccessToken
   */
  public static final Property accessToken = newProperty(Flags.READONLY, "", null);
  
  /**
   * Get the {@code accessToken} property.
   * @see #accessToken
   */
  public String getAccessToken() { return getString(accessToken); }
  
  /**
   * Set the {@code accessToken} property.
   * @see #accessToken
   */
  public void setAccessToken(String v) { setString(accessToken, v, null); }

////////////////////////////////////////////////////////////////
// Action "refreshToken"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code refreshToken} action.
   * @see #refreshToken()
   */
  public static final Action refreshToken = newAction(0, null);
  
  /**
   * Invoke the {@code refreshToken} action.
   * @see #refreshToken
   */
  public void refreshToken() { invoke(refreshToken, null, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudEnelCloud.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Refresh TOKEN
////////////////////////////////////////////////////////////////

  public void doRefreshToken()
  {
    // create an HTTPClient instance
    CloseableHttpClient httpclient = HttpClients.createDefault();

    try
    {
      // make the post request an set the url
      HttpPost httpPost = new HttpPost(getAccessTokenUrl());

      // set header parameters
      String encoding = Base64.getEncoder().encodeToString((getClientId()+":"+getClientSecret()).getBytes());
      Header[] headers = new Header[4];
      headers[0] = new BasicHeader("Content-Type", "application/x-www-form-urlencoded");
      headers[1] = new BasicHeader("Authorization", "Basic " + encoding);
      headers[2] = new BasicHeader("accept", "*/*");
      //headers[3] = new BasicHeader("host", "demsitaa.enel.com");
      headers[3] = new BasicHeader("accept-encoding", "gzip, deflate");
      httpPost.setHeaders(headers);

      // set the timeout of the request
      RequestConfig.Builder reqBuilder = RequestConfig.custom();
      httpPost.setConfig(reqBuilder.build());

      // create the body structure
      List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
      nameValuePairs.add(new BasicNameValuePair("client_secret", URLEncoder.encode(getClientSecret(), "UTF-8")));
      nameValuePairs.add(new BasicNameValuePair("client_id", URLEncoder.encode(getClientId(), "UTF-8")));
      nameValuePairs.add(new BasicNameValuePair("grant_type", "password"));
      nameValuePairs.add(new BasicNameValuePair("scope", ""));
      nameValuePairs.add(new BasicNameValuePair("username", getUsername()));
      nameValuePairs.add(new BasicNameValuePair("password", getPassword()));

      // set the body into post
      httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

      // execute the http post request and get the response
      CloseableHttpResponse response = httpclient.execute(httpPost);

      try
      {
        // update the result status code
        if (logger.isLoggable(Level.FINE))
          logger.fine("Response status line: " + response.getStatusLine());

        // get JSON object from response body
        JSONObject json_auth = new JSONObject(EntityUtils.toString(response.getEntity()));
        String token = json_auth.getString("access_token");
        // set slot
        setAccessToken(token);
      }
      catch (Exception e)
      {
        if (logger.isLoggable(Level.FINE))
          logger.fine("Exception: " + e.getMessage());
      }
      finally
      {
        response.close(); // always try to close the connection
      }
    }
    catch (Exception e)
    {
      if (logger.isLoggable(Level.FINE))
        logger.fine("Exception: " + e.getMessage());
    }
  }


////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("connection.png");
    }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

  public static final Logger logger = Logger.getLogger("CloudLabEnelCloud");
}
