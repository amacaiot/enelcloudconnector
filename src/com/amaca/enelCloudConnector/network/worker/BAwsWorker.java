package com.amaca.enelCloudConnector.network.worker;

import javax.baja.nre.annotations.NiagaraType;
import javax.baja.util.BWorker;
import javax.baja.sys.*;
import javax.baja.util.*;

/**
 * Created by Tridium on 07/06/2016.
 */
@NiagaraType
public class BAwsWorker extends BWorker
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelIotPlatformConnector.worker.BAwsWorker(2979906276)1.0$ @*/
/* Generated Tue Mar 10 18:50:25 CET 2020 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAwsWorker.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

    @Override
    public Worker getWorker()
    {
        if (worker == null)
        {
            queue = new CoalesceQueue(1000);
            worker = new Worker(queue);
        }
        return worker;
    }


    /**
     * Post an action to be run asynchronously.
     */
    public void postAsync(Runnable r)
    {
        if (!isRunning() || queue == null)
            throw new NotRunningException();
        queue.enqueue(r);
    }





    //////////////////////////////////////////////////////////
// Attributes
//////////////////////////////////////////////////////////
    private CoalesceQueue queue;
    private Worker worker;

}