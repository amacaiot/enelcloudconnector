package com.amaca.enelCloudConnector.point;

import com.amaca.enelCloudConnector.BEnelCloudService;

import javax.baja.control.*;
import javax.baja.control.util.BBooleanOverride;
import javax.baja.control.util.BEnumOverride;
import javax.baja.control.util.BNumericOverride;
import javax.baja.control.util.BStringOverride;
import javax.baja.nre.annotations.NiagaraAction;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.status.*;
import javax.baja.sys.*;

/**
 * Created by Amaca on 04/08/2018.
 */

@NiagaraProperty(
        name = "inputSchedulingManagerNumeric",
        type = "baja:StatusNumeric",
        defaultValue = "new BStatusNumeric(0, BStatus.nullStatus)"
)
@NiagaraProperty(
        name = "inputPlansDaysManagerNumeric",
        type = "baja:StatusNumeric",
        defaultValue = "new BStatusNumeric(0, BStatus.nullStatus)"
)
@NiagaraProperty(
        name = "inputSchedulingManagerBoolean",
        type = "baja:StatusBoolean",
        defaultValue = "new BStatusBoolean(false, BStatus.nullStatus)"
)
@NiagaraProperty(
        name = "inputPlansDaysManagerBoolean",
        type = "baja:StatusBoolean",
        defaultValue = "new BStatusBoolean(false, BStatus.nullStatus)"
)
@NiagaraProperty(
        name = "inputSchedulingManagerString",
        type = "baja:StatusString",
        defaultValue = "new BStatusString(new String(), BStatus.nullStatus)"
)
@NiagaraProperty(
        name = "inputPlansDaysManagerString",
        type = "baja:StatusString",
        defaultValue = "new BStatusString(new String(), BStatus.nullStatus)"
)
@NiagaraProperty(
        name = "inputSchedulingManagerEnum",
        type = "baja:StatusEnum",
        defaultValue = "new BStatusEnum(BDynamicEnum.DEFAULT, BStatus.nullStatus)"
)
@NiagaraProperty(
        name = "inputPlansDaysManagerEnum",
        type = "baja:StatusEnum",
        defaultValue = "new BStatusEnum(BDynamicEnum.DEFAULT, BStatus.nullStatus)"
)
@NiagaraProperty(
        name = "schedulingManagerInputId",
        type = "baja:String",
        defaultValue = "in8"
)
@NiagaraProperty(
        name = "plansDaysManagerInputId",
        type = "baja:String",
        defaultValue = "in8"
)
@NiagaraAction(
        name = "resetStandardSet",
        parameterType = "baja:String",
        defaultValue = "BString.DEFAULT"
)

@NiagaraType
public class BEnelCloudPointManager extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.point.BEnelCloudPointManager(3481157443)1.0$ @*/
/* Generated Sun Mar 17 12:04:37 CET 2019 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "inputSchedulingManagerNumeric"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code inputSchedulingManagerNumeric} property.
   * @see #getInputSchedulingManagerNumeric
   * @see #setInputSchedulingManagerNumeric
   */
  public static final Property inputSchedulingManagerNumeric = newProperty(0, new BStatusNumeric(0, BStatus.nullStatus), null);
  
  /**
   * Get the {@code inputSchedulingManagerNumeric} property.
   * @see #inputSchedulingManagerNumeric
   */
  public BStatusNumeric getInputSchedulingManagerNumeric() { return (BStatusNumeric)get(inputSchedulingManagerNumeric); }
  
  /**
   * Set the {@code inputSchedulingManagerNumeric} property.
   * @see #inputSchedulingManagerNumeric
   */
  public void setInputSchedulingManagerNumeric(BStatusNumeric v) { set(inputSchedulingManagerNumeric, v, null); }

////////////////////////////////////////////////////////////////
// Property "inputPlansDaysManagerNumeric"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code inputPlansDaysManagerNumeric} property.
   * @see #getInputPlansDaysManagerNumeric
   * @see #setInputPlansDaysManagerNumeric
   */
  public static final Property inputPlansDaysManagerNumeric = newProperty(0, new BStatusNumeric(0, BStatus.nullStatus), null);
  
  /**
   * Get the {@code inputPlansDaysManagerNumeric} property.
   * @see #inputPlansDaysManagerNumeric
   */
  public BStatusNumeric getInputPlansDaysManagerNumeric() { return (BStatusNumeric)get(inputPlansDaysManagerNumeric); }
  
  /**
   * Set the {@code inputPlansDaysManagerNumeric} property.
   * @see #inputPlansDaysManagerNumeric
   */
  public void setInputPlansDaysManagerNumeric(BStatusNumeric v) { set(inputPlansDaysManagerNumeric, v, null); }

////////////////////////////////////////////////////////////////
// Property "inputSchedulingManagerBoolean"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code inputSchedulingManagerBoolean} property.
   * @see #getInputSchedulingManagerBoolean
   * @see #setInputSchedulingManagerBoolean
   */
  public static final Property inputSchedulingManagerBoolean = newProperty(0, new BStatusBoolean(false, BStatus.nullStatus), null);
  
  /**
   * Get the {@code inputSchedulingManagerBoolean} property.
   * @see #inputSchedulingManagerBoolean
   */
  public BStatusBoolean getInputSchedulingManagerBoolean() { return (BStatusBoolean)get(inputSchedulingManagerBoolean); }
  
  /**
   * Set the {@code inputSchedulingManagerBoolean} property.
   * @see #inputSchedulingManagerBoolean
   */
  public void setInputSchedulingManagerBoolean(BStatusBoolean v) { set(inputSchedulingManagerBoolean, v, null); }

////////////////////////////////////////////////////////////////
// Property "inputPlansDaysManagerBoolean"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code inputPlansDaysManagerBoolean} property.
   * @see #getInputPlansDaysManagerBoolean
   * @see #setInputPlansDaysManagerBoolean
   */
  public static final Property inputPlansDaysManagerBoolean = newProperty(0, new BStatusBoolean(false, BStatus.nullStatus), null);
  
  /**
   * Get the {@code inputPlansDaysManagerBoolean} property.
   * @see #inputPlansDaysManagerBoolean
   */
  public BStatusBoolean getInputPlansDaysManagerBoolean() { return (BStatusBoolean)get(inputPlansDaysManagerBoolean); }
  
  /**
   * Set the {@code inputPlansDaysManagerBoolean} property.
   * @see #inputPlansDaysManagerBoolean
   */
  public void setInputPlansDaysManagerBoolean(BStatusBoolean v) { set(inputPlansDaysManagerBoolean, v, null); }

////////////////////////////////////////////////////////////////
// Property "inputSchedulingManagerString"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code inputSchedulingManagerString} property.
   * @see #getInputSchedulingManagerString
   * @see #setInputSchedulingManagerString
   */
  public static final Property inputSchedulingManagerString = newProperty(0, new BStatusString(new String(), BStatus.nullStatus), null);
  
  /**
   * Get the {@code inputSchedulingManagerString} property.
   * @see #inputSchedulingManagerString
   */
  public BStatusString getInputSchedulingManagerString() { return (BStatusString)get(inputSchedulingManagerString); }
  
  /**
   * Set the {@code inputSchedulingManagerString} property.
   * @see #inputSchedulingManagerString
   */
  public void setInputSchedulingManagerString(BStatusString v) { set(inputSchedulingManagerString, v, null); }

////////////////////////////////////////////////////////////////
// Property "inputPlansDaysManagerString"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code inputPlansDaysManagerString} property.
   * @see #getInputPlansDaysManagerString
   * @see #setInputPlansDaysManagerString
   */
  public static final Property inputPlansDaysManagerString = newProperty(0, new BStatusString(new String(), BStatus.nullStatus), null);
  
  /**
   * Get the {@code inputPlansDaysManagerString} property.
   * @see #inputPlansDaysManagerString
   */
  public BStatusString getInputPlansDaysManagerString() { return (BStatusString)get(inputPlansDaysManagerString); }
  
  /**
   * Set the {@code inputPlansDaysManagerString} property.
   * @see #inputPlansDaysManagerString
   */
  public void setInputPlansDaysManagerString(BStatusString v) { set(inputPlansDaysManagerString, v, null); }

////////////////////////////////////////////////////////////////
// Property "inputSchedulingManagerEnum"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code inputSchedulingManagerEnum} property.
   * @see #getInputSchedulingManagerEnum
   * @see #setInputSchedulingManagerEnum
   */
  public static final Property inputSchedulingManagerEnum = newProperty(0, new BStatusEnum(BDynamicEnum.DEFAULT, BStatus.nullStatus), null);
  
  /**
   * Get the {@code inputSchedulingManagerEnum} property.
   * @see #inputSchedulingManagerEnum
   */
  public BStatusEnum getInputSchedulingManagerEnum() { return (BStatusEnum)get(inputSchedulingManagerEnum); }
  
  /**
   * Set the {@code inputSchedulingManagerEnum} property.
   * @see #inputSchedulingManagerEnum
   */
  public void setInputSchedulingManagerEnum(BStatusEnum v) { set(inputSchedulingManagerEnum, v, null); }

////////////////////////////////////////////////////////////////
// Property "inputPlansDaysManagerEnum"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code inputPlansDaysManagerEnum} property.
   * @see #getInputPlansDaysManagerEnum
   * @see #setInputPlansDaysManagerEnum
   */
  public static final Property inputPlansDaysManagerEnum = newProperty(0, new BStatusEnum(BDynamicEnum.DEFAULT, BStatus.nullStatus), null);
  
  /**
   * Get the {@code inputPlansDaysManagerEnum} property.
   * @see #inputPlansDaysManagerEnum
   */
  public BStatusEnum getInputPlansDaysManagerEnum() { return (BStatusEnum)get(inputPlansDaysManagerEnum); }
  
  /**
   * Set the {@code inputPlansDaysManagerEnum} property.
   * @see #inputPlansDaysManagerEnum
   */
  public void setInputPlansDaysManagerEnum(BStatusEnum v) { set(inputPlansDaysManagerEnum, v, null); }

////////////////////////////////////////////////////////////////
// Property "schedulingManagerInputId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code schedulingManagerInputId} property.
   * @see #getSchedulingManagerInputId
   * @see #setSchedulingManagerInputId
   */
  public static final Property schedulingManagerInputId = newProperty(0, "in8", null);
  
  /**
   * Get the {@code schedulingManagerInputId} property.
   * @see #schedulingManagerInputId
   */
  public String getSchedulingManagerInputId() { return getString(schedulingManagerInputId); }
  
  /**
   * Set the {@code schedulingManagerInputId} property.
   * @see #schedulingManagerInputId
   */
  public void setSchedulingManagerInputId(String v) { setString(schedulingManagerInputId, v, null); }

////////////////////////////////////////////////////////////////
// Property "plansDaysManagerInputId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code plansDaysManagerInputId} property.
   * @see #getPlansDaysManagerInputId
   * @see #setPlansDaysManagerInputId
   */
  public static final Property plansDaysManagerInputId = newProperty(0, "in8", null);
  
  /**
   * Get the {@code plansDaysManagerInputId} property.
   * @see #plansDaysManagerInputId
   */
  public String getPlansDaysManagerInputId() { return getString(plansDaysManagerInputId); }
  
  /**
   * Set the {@code plansDaysManagerInputId} property.
   * @see #plansDaysManagerInputId
   */
  public void setPlansDaysManagerInputId(String v) { setString(plansDaysManagerInputId, v, null); }

////////////////////////////////////////////////////////////////
// Action "resetStandardSet"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code resetStandardSet} action.
   * @see #resetStandardSet(BString parameter)
   */
  public static final Action resetStandardSet = newAction(0, BString.DEFAULT, null);
  
  /**
   * Invoke the {@code resetStandardSet} action.
   * @see #resetStandardSet
   */
  public void resetStandardSet(BString parameter) { invoke(resetStandardSet, parameter, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudPointManager.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    public BEnelCloudPointManager(BControlPoint point)
    {
        super();
    }

    public BEnelCloudPointManager()
    {
        super();
    }


  ////////////////////////////////////////////////////////////////
// Changed
////////////////////////////////////////////////////////////////

    @Override
    public void changed(Property property, Context context)
    {
        super.changed(property, context);

        // check if we are in a running state
        if (!isRunning())return;
        if (!Sys.atSteadyState())return;

        try
        {
            //  input from the schedule changed - numeric
            if (property == inputSchedulingManagerNumeric)
            {
                if (getInputSchedulingManagerNumeric().getStatus().equals(BStatus.nullStatus))
                    doResetStandardSet(BString.make(getSchedulingManagerInputId()));
                else
                    setNumericValue(getInputSchedulingManagerNumeric().getValue(), getSchedulingManagerInputId(), BRelTime.DEFAULT, getInputSchedulingManagerNumeric().getStatus());
            } else if (property == inputPlansDaysManagerNumeric) //input from plans day manager changed - numeric
            {
                if (getInputPlansDaysManagerNumeric().getStatus().equals(BStatus.nullStatus))
                    doResetStandardSet(BString.make(getPlansDaysManagerInputId()));
                else
                    setNumericValue(getInputPlansDaysManagerNumeric().getValue(), getPlansDaysManagerInputId(), BRelTime.DEFAULT, getInputPlansDaysManagerNumeric().getStatus());
            } else if (property == inputSchedulingManagerBoolean) // input from the schedule changed - boolean
            {
                if (getInputSchedulingManagerBoolean().getStatus().equals(BStatus.nullStatus))
                    doResetStandardSet(BString.make(getSchedulingManagerInputId()));
                else
                    setBooleanValue(getInputSchedulingManagerBoolean().getValue(), getSchedulingManagerInputId(), BRelTime.DEFAULT, getInputSchedulingManagerBoolean().getStatus());
            } else if (property == inputPlansDaysManagerBoolean) //input from plans day manager changed - boolean
            {
                if (getInputPlansDaysManagerBoolean().getStatus().equals(BStatus.nullStatus))
                    doResetStandardSet(BString.make(getPlansDaysManagerInputId()));
                else
                    setBooleanValue(getInputPlansDaysManagerBoolean().getValue(), getPlansDaysManagerInputId(), BRelTime.DEFAULT, getInputPlansDaysManagerBoolean().getStatus());
            }
            else if (property == inputSchedulingManagerEnum) // input from the schedule changed - enum
            {
                if (getInputSchedulingManagerEnum().getStatus().equals(BStatus.nullStatus))
                    doResetStandardSet(BString.make(getSchedulingManagerInputId()));
                else
                    setEnumValue(getInputSchedulingManagerEnum().getValue().getOrdinal(), getSchedulingManagerInputId(), BRelTime.DEFAULT, getInputSchedulingManagerEnum().getStatus());
            } else if (property == inputPlansDaysManagerEnum) //input from plans day manager changed - enum
            {
                if (getInputPlansDaysManagerEnum().getStatus().equals(BStatus.nullStatus))
                    doResetStandardSet(BString.make(getPlansDaysManagerInputId()));
                else
                    setEnumValue(getInputPlansDaysManagerEnum().getValue().getOrdinal(), getPlansDaysManagerInputId(), BRelTime.DEFAULT, getInputPlansDaysManagerEnum().getStatus());
            }
            else if (property == inputSchedulingManagerString) // input from the schedule changed - sting
            {
                if (getInputSchedulingManagerString().getStatus().equals(BStatus.nullStatus))
                    doResetStandardSet(BString.make(getSchedulingManagerInputId()));
                else
                    setStringValue(getInputSchedulingManagerString().getValue(), getSchedulingManagerInputId(), BRelTime.DEFAULT, getInputSchedulingManagerString().getStatus());
            } else if (property == inputPlansDaysManagerString) //input from plans day manager changed - string
            {
                if (getInputPlansDaysManagerString().getStatus().equals(BStatus.nullStatus))
                    doResetStandardSet(BString.make(getPlansDaysManagerInputId()));
                else
                    setStringValue(getInputPlansDaysManagerString().getValue(), getPlansDaysManagerInputId(), BRelTime.DEFAULT, getInputPlansDaysManagerString().getStatus());
            }
        }catch (Exception e)
        {
            BEnelCloudService service = (BEnelCloudService)Sys.getService(BEnelCloudService.TYPE);
            service.getEnelCloudAlarmsManager().generateEmailAlarm(getParent().getParentComponent(), "ERROR: WRITE FAIL");
        }
    }

    //update values, maybe after a slot change
    public void updateValues()
    {
        if (isNumeric())
        {
            if (getInputSchedulingManagerNumeric().getStatus().equals(BStatus.nullStatus))
                doResetStandardSet(BString.make(getSchedulingManagerInputId()));
            else
                setNumericValue(getInputSchedulingManagerNumeric().getValue(), getSchedulingManagerInputId(), BRelTime.DEFAULT, getInputSchedulingManagerNumeric().getStatus());

            if (getInputPlansDaysManagerNumeric().getStatus().equals(BStatus.nullStatus))
                doResetStandardSet(BString.make(getPlansDaysManagerInputId()));
            else
                setNumericValue(getInputPlansDaysManagerNumeric().getValue(), getPlansDaysManagerInputId(), BRelTime.DEFAULT, getInputPlansDaysManagerNumeric().getStatus());
        }
        else if (isBoolean())
        {
            if (getInputSchedulingManagerBoolean().getStatus().equals(BStatus.nullStatus))
                doResetStandardSet(BString.make(getSchedulingManagerInputId()));
            else
                setBooleanValue(getInputSchedulingManagerBoolean().getValue(), getSchedulingManagerInputId(), BRelTime.DEFAULT, getInputSchedulingManagerBoolean().getStatus());

            if (getInputPlansDaysManagerBoolean().getStatus().equals(BStatus.nullStatus))
                doResetStandardSet(BString.make(getPlansDaysManagerInputId()));
            else
                setBooleanValue(getInputPlansDaysManagerBoolean().getValue(), getPlansDaysManagerInputId(), BRelTime.DEFAULT, getInputPlansDaysManagerBoolean().getStatus());

        }
        else if (isString())
        {
            if (getInputSchedulingManagerString().getStatus().equals(BStatus.nullStatus))
                doResetStandardSet(BString.make(getSchedulingManagerInputId()));
            else
                setStringValue(getInputSchedulingManagerString().getValue(), getSchedulingManagerInputId(), BRelTime.DEFAULT, getInputSchedulingManagerString().getStatus());

            if (getInputPlansDaysManagerString().getStatus().equals(BStatus.nullStatus))
                doResetStandardSet(BString.make(getPlansDaysManagerInputId()));
            else
                setStringValue(getInputPlansDaysManagerString().getValue(), getPlansDaysManagerInputId(), BRelTime.DEFAULT, getInputPlansDaysManagerString().getStatus());

        }
        else if (isEnum())
        {
            if (getInputSchedulingManagerEnum().getStatus().equals(BStatus.nullStatus))
                doResetStandardSet(BString.make(getSchedulingManagerInputId()));
            else
                setEnumValue(getInputSchedulingManagerEnum().getValue().getOrdinal(), getSchedulingManagerInputId(), BRelTime.DEFAULT, getInputSchedulingManagerEnum().getStatus());

            if (getInputPlansDaysManagerBoolean().getStatus().equals(BStatus.nullStatus))
                doResetStandardSet(BString.make(getPlansDaysManagerInputId()));
            else
                setEnumValue(getInputPlansDaysManagerEnum().getValue().getOrdinal(), getPlansDaysManagerInputId(), BRelTime.DEFAULT, getInputPlansDaysManagerEnum().getStatus());

        }
    }



    ////////////////////////////////////////////////////////////////
// SET VALUE
////////////////////////////////////////////////////////////////

    public void setNumericValue(double value, String slotId, BRelTime validityTime, BStatus status)
    {
        // check the status of the point
        if (((BNumericWritable) getParent().getParentComponent()).getStatus().isDown() || ((BNumericWritable) getParent().getParentComponent()).getStatus().isFault() || ((BNumericWritable) getParent().getParentComponent()).getStatus().isDisabled())
        {
            BEnelCloudService service = (BEnelCloudService)Sys.getService(BEnelCloudService.TYPE);
            service.getEnelCloudAlarmsManager().generateEmailAlarm(getParent().getParentComponent(), "ERROR: WRITE FAIL");
        }

        try
        {
            // if the validity time was not set, send the infinite override and value
            if (validityTime.equals(BRelTime.DEFAULT))
            {
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    ((BNumericWritable) getParent().getParentComponent()).override(new BNumericOverride(BDouble.make(value).getDouble()));
                } else if (slotId.equals("in1")) // emergency override function
                {
                    ((BNumericWritable) getParent().getParentComponent()).emergencyOverride(BDouble.make(value));
                } else // standard set function
                {
                    ((BNumericWritable) getParent().getParentComponent()).set(slotId, new BStatusNumeric(value));
                }
            } else // else send the correct validity time and value
            {
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    ((BNumericWritable) getParent().getParentComponent()).override(new BNumericOverride(validityTime, BDouble.make(value).getDouble()));
                } else if (slotId.equals("in1")) // emergency override function
                {
                    ((BNumericWritable) getParent().getParentComponent()).emergencyOverride(BDouble.make(value));
                    // start ticket for reset the emergency override after the validityTime
                    resetEmergencySetTimer = Clock.schedule(getParent().getParentComponent(), validityTime, BNumericWritable.emergencyAuto, null);
                } else // standard set function
                {
                    ((BNumericWritable) getParent().getParentComponent()).set(slotId, new BStatusNumeric(value, status));
                    // start ticket for reset the set after the validityTime
                    resetStandardSetTimer = Clock.schedule(this, validityTime, resetStandardSet, BString.make(slotId));
                }
            }
        }
        catch (Exception e)
        {
            BEnelCloudService service = (BEnelCloudService)Sys.getService(BEnelCloudService.TYPE);
            service.getEnelCloudAlarmsManager().generateEmailAlarm(getParent().getParentComponent(), "ERROR: WRITE FAIL");
        }

    }

    public void setBooleanValue(boolean value, String slotId, BRelTime validityTime, BStatus status)
    {
        // check the status of the point
        if (((BBooleanWritable) getParent().getParentComponent()).getStatus().isDown() || ((BBooleanWritable) getParent().getParentComponent()).getStatus().isFault() || ((BBooleanWritable) getParent().getParentComponent()).getStatus().isDisabled())
        {
            BEnelCloudService service = (BEnelCloudService)Sys.getService(BEnelCloudService.TYPE);
            service.getEnelCloudAlarmsManager().generateEmailAlarm(getParent().getParentComponent(), "ERROR: WRITE FAIL");
        }

        try
        {
            // if the validity time was not set, send the infinite override and value
            if (validityTime.equals(BRelTime.DEFAULT))
            {
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    if (value)
                        ((BBooleanWritable) getParent().getParentComponent()).active(new BBooleanOverride(value));
                    else
                        ((BBooleanWritable) getParent().getParentComponent()).inactive(new BBooleanOverride(value));
                } else if (slotId.equals("in1")) // emergency override function
                {
                    if (value)
                        ((BBooleanWritable) getParent().getParentComponent()).emergencyActive();
                    else
                        ((BBooleanWritable) getParent().getParentComponent()).emergencyInactive();
                } else // standard set function
                {
                    ((BBooleanWritable) getParent().getParentComponent()).set(slotId, new BStatusBoolean(value, status));
                }
            } else // else send the correct validity time and value
            {
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    if (value)
                        ((BBooleanWritable) getParent().getParentComponent()).active(new BBooleanOverride(validityTime, value));
                    else
                        ((BBooleanWritable) getParent().getParentComponent()).inactive(new BBooleanOverride(validityTime, value));
                } else if (slotId.equals("in1")) // emergency override function
                {
                    if (value)
                        ((BBooleanWritable) getParent().getParentComponent()).emergencyActive();
                    else
                        ((BBooleanWritable) getParent().getParentComponent()).emergencyInactive();
                    // start ticket for reset the emergency override after the validityTime
                    resetEmergencySetTimer = Clock.schedule(getParent().getParentComponent(), validityTime, BBooleanWritable.emergencyAuto, null);
                } else // standard set function
                {
                    ((BBooleanWritable) getParent().getParentComponent()).set(slotId, new BStatusBoolean(value, status));
                    // start ticket for reset the set after the validityTime
                    resetStandardSetTimer = Clock.schedule(this, validityTime, resetStandardSet, BString.make(slotId));
                }
            }
        }catch (Exception e)
        {
            BEnelCloudService service = (BEnelCloudService)Sys.getService(BEnelCloudService.TYPE);
            service.getEnelCloudAlarmsManager().generateEmailAlarm(getParent().getParentComponent(), "ERROR: WRITE FAIL");
        }
    }

    public void setStringValue(String value, String slotId, BRelTime validityTime, BStatus status)
    {
        // check the status of the point
        if (((BStringWritable) getParent().getParentComponent()).getStatus().isDown() || ((BStringWritable) getParent().getParentComponent()).getStatus().isFault() || ((BStringWritable) getParent().getParentComponent()).getStatus().isDisabled())
        {
            BEnelCloudService service = (BEnelCloudService)Sys.getService(BEnelCloudService.TYPE);
            service.getEnelCloudAlarmsManager().generateEmailAlarm(getParent().getParentComponent(), "ERROR: WRITE FAIL");
        }

        try
        {
            // if the validity time was not set, send the infinite override and value
            if (validityTime.equals(BRelTime.DEFAULT))
            {
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    ((BStringWritable) getParent().getParentComponent()).override(new BStringOverride(value));
                } else if (slotId.equals("in1")) // emergency override function
                {
                    ((BStringWritable) getParent().getParentComponent()).emergencyOverride(BString.make(value));
                } else // standard set function
                {
                    ((BStringWritable) getParent().getParentComponent()).set(slotId, new BStatusString(value));
                }
            } else // else send the correct validity time and value
            {
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    ((BStringWritable) getParent().getParentComponent()).override(new BStringOverride(validityTime, value));
                } else if (slotId.equals("in1")) // emergency override function
                {
                    ((BStringWritable) getParent().getParentComponent()).emergencyOverride(BString.make(value));
                    // start ticket for reset the emergency override after the validityTime
                    resetEmergencySetTimer = Clock.schedule(getParent().getParentComponent(), validityTime, BStringWritable.emergencyAuto, null);
                } else // standard set function
                {
                    ((BStringWritable) getParent().getParentComponent()).set(slotId, new BStatusString(value, status));
                    // start ticket for reset the set after the validityTime
                    resetStandardSetTimer = Clock.schedule(this, validityTime, resetStandardSet, BString.make(slotId));
                }
            }
        }
        catch (Exception e)
        {
            BEnelCloudService service = (BEnelCloudService)Sys.getService(BEnelCloudService.TYPE);
            service.getEnelCloudAlarmsManager().generateEmailAlarm(getParent().getParentComponent(), "ERROR: WRITE FAIL");
        }

    }

    public void setEnumValue(int value, String slotId, BRelTime validityTime, BStatus status)
    {
// check the status of the point
        if (((BEnumWritable) getParent().getParentComponent()).getStatus().isDown() || ((BEnumWritable) getParent().getParentComponent()).getStatus().isFault() || ((BEnumWritable) getParent().getParentComponent()).getStatus().isDisabled())
        {
            BEnelCloudService service = (BEnelCloudService)Sys.getService(BEnelCloudService.TYPE);
            service.getEnelCloudAlarmsManager().generateEmailAlarm(getParent().getParentComponent(), "ERROR: WRITE FAIL");
        }

        try
        {
            // if the validity time was not set, send the infinite override and value
            if (validityTime.equals(BRelTime.DEFAULT))
            {
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    ((BEnumWritable) getParent().getParentComponent()).override(new BEnumOverride(BDynamicEnum.make(value)));
                } else if (slotId.equals("in1")) // emergency override function
                {
                    ((BEnumWritable) getParent().getParentComponent()).emergencyOverride(BDynamicEnum.make(value));
                } else // standard set function
                {
                    ((BEnumWritable) getParent().getParentComponent()).set(slotId, new BStatusEnum(BDynamicEnum.make(value)));
                }
            } else // else send the correct validity time and value
            {
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    ((BEnumWritable) getParent().getParentComponent()).override(new BEnumOverride(validityTime, BDynamicEnum.make(value)));
                } else if (slotId.equals("in1")) // emergency override function
                {
                    ((BEnumWritable) getParent().getParentComponent()).emergencyOverride(BDynamicEnum.make(value));
                    // start ticket for reset the emergency override after the validityTime
                    resetEmergencySetTimer = Clock.schedule(getParent().getParentComponent(), validityTime, BEnumWritable.emergencyAuto, null);
                } else // standard set function
                {
                    ((BEnumWritable) getParent().getParentComponent()).set(slotId, new BStatusEnum(BDynamicEnum.make(value), status));
                    // start ticket for reset the set after the validityTime
                    resetStandardSetTimer = Clock.schedule(this, validityTime, resetStandardSet, BString.make(slotId));
                }
            }
        }
        catch (Exception e)
        {
            BEnelCloudService service = (BEnelCloudService)Sys.getService(BEnelCloudService.TYPE);
            service.getEnelCloudAlarmsManager().generateEmailAlarm(getParent().getParentComponent(), "ERROR: WRITE FAIL");
        }

    }

 ////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////

    // reset a previously sent reset
    public void doResetStandardSet(BString slotName)
    {
        try
        {

            // create the standard string for the slotID
            String slotId = slotName.getString();

            // checke the type of the parent point
            if (getParent().getParentComponent() instanceof BNumericWritable)
            {
                // reset the value
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    ((BNumericWritable) getParent().getParentComponent()).auto();
                } else if (slotId.equals("in1")) // emergency override function
                {
                    ((BNumericWritable) getParent().getParentComponent()).emergencyAuto();
                } else // standard set function
                {
                    ((BNumericWritable) getParent().getParentComponent()).set(slotId, new BStatusNumeric(0, BStatus.nullStatus));
                }
            } else if (getParent().getParentComponent() instanceof BBooleanWritable)
            {
                // reset the value
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    ((BBooleanWritable) getParent().getParentComponent()).auto();
                } else if (slotId.equals("in1")) // emergency override function
                {
                    ((BBooleanWritable) getParent().getParentComponent()).emergencyAuto();
                } else // standard set function
                {
                    ((BBooleanWritable) getParent().getParentComponent()).set(slotName.getString(), new BStatusBoolean(false, BStatus.nullStatus));
                }
            }
            else if (getParent().getParentComponent() instanceof BStringWritable)
            {
                // reset the value
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    ((BStringWritable) getParent().getParentComponent()).auto();
                } else if (slotId.equals("in1")) // emergency override function
                {
                    ((BStringWritable) getParent().getParentComponent()).emergencyAuto();
                } else // standard set function
                {
                    ((BStringWritable) getParent().getParentComponent()).set(slotName.getString(), new BStatusString("", BStatus.nullStatus));
                }
            }
            else if (getParent().getParentComponent() instanceof BEnumWritable)
            {
                // reset the value
                // check wich type of in we use
                if (slotId.equals("in8")) // override function
                {
                    ((BEnumWritable) getParent().getParentComponent()).auto();
                } else if (slotId.equals("in1")) // emergency override function
                {
                    ((BEnumWritable) getParent().getParentComponent()).emergencyAuto();
                } else // standard set function
                {
                    ((BEnumWritable) getParent().getParentComponent()).set(slotName.getString(), new BStatusEnum(BDynamicEnum.DEFAULT, BStatus.nullStatus));
                }
            }
        }catch (Exception e)
        {
            BEnelCloudService service = (BEnelCloudService)Sys.getService(BEnelCloudService.TYPE);
            service.getEnelCloudAlarmsManager().generateEmailAlarm(getParent().getParentComponent(), "ERROR: WRITE FAIL");
        }
    }

 ////////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

    public boolean isNumeric()
    {
        if (getParent().getParentComponent() instanceof BNumericWritable)
            return true;

        return false;
    }

    public boolean isBoolean()
    {
        if (getParent().getParentComponent() instanceof BBooleanWritable)
            return true;

        return false;
    }

    public boolean isString()
    {
        if (getParent().getParentComponent() instanceof BStringWritable)
            return true;

        return false;
    }

    public boolean isEnum()
    {
        if (getParent().getParentComponent() instanceof BEnumWritable)
            return true;

        return false;
    }

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("action.png");
    }


////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

    private Clock.Ticket resetEmergencySetTimer = null;
    private Clock.Ticket resetStandardSetTimer = null;
}
