package com.amaca.enelCloudConnector.setpoint;

import com.amaca.enelCloudConnector.BEnelCloudService;
import com.amaca.enelCloudConnector.point.BEnelCloudPointManager;
import com.amaca.enelCloudConnector.utils.EnelCloudUtils;
import com.tridium.json.JSONObject;

import javax.baja.collection.BITable;
import javax.baja.collection.Column;
import javax.baja.collection.ColumnList;
import javax.baja.collection.TableCursor;
import javax.baja.control.*;
import javax.baja.driver.BDevice;
import javax.baja.driver.BDeviceExt;
import javax.baja.driver.point.BPointDeviceExt;
import javax.baja.naming.BOrd;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.status.*;
import javax.baja.sys.*;
import javax.baja.web.BWebServlet;
import javax.baja.web.WebOp;
import java.io.BufferedReader;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Amaca on 04/04/2018.
 */

@NiagaraProperty(
        name = "servletName",
        type = "baja:String",
        flags = Flags.READONLY | Flags.HIDDEN,
        defaultValue = "niagara"
)

@NiagaraType
public class BEnelCloudSetpointManager extends BWebServlet
{

/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.setpoint.BEnelCloudSetpointManager(1347797443)1.0$ @*/
/* Generated Sun Apr 19 16:30:21 CEST 2020 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "servletName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code servletName} property.
   * @see #getServletName
   * @see #setServletName
   */
  public static final Property servletName = newProperty(Flags.READONLY | Flags.HIDDEN, "niagara", null);
  
  /**
   * Get the {@code servletName} property.
   * @see #servletName
   */
  public String getServletName() { return getString(servletName); }
  
  /**
   * Set the {@code servletName} property.
   * @see #servletName
   */
  public void setServletName(String v) { setString(servletName, v, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudSetpointManager.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


////////////////////////////////////////////////////////////////
// POST
////////////////////////////////////////////////////////////////

  public boolean setValueToPoint(String deviceCode, String dataPointName, String value, BRelTime validityTime, String slotId)
  {

      // fine log messages
      if (logger.isLoggable(Level.FINE))
      {
        logger.fine("deviceCode: " + deviceCode);
        logger.fine("dataPointName: " + dataPointName);
        logger.fine("value: " + value);
        logger.fine("validityTime: " + validityTime);
        logger.fine("slotId: " + slotId);
      }

      // create the bql query for searching the right device
      BOrd query = BOrd.make("slot:/|bql:select slotPathOrd from driver:Device where name = '" + deviceCode + "'");
      // resolve the bql query
      BITable result = (BITable) query.resolve(this).get();
      // get the columns
      ColumnList columns = result.getColumns();
      // select the frist one with the slotPathOrd
      Column valueColumn = columns.get(0);
      // get the cursor
      TableCursor tableCursor = result.cursor();
      // get the first row
      tableCursor.next();
      // get the device from the slotPathOrd getted
      BDevice device = ((BDevice) tableCursor.get());
      // get all extensions and set to null the points one
      BDeviceExt[] exts = device.getDeviceExts();
      BPointDeviceExt pointDeviceExt = null;

      // search the point extension
      for (int i = 0; i < exts.length; i++)
      {
        if (exts[i] instanceof BPointDeviceExt)
          pointDeviceExt = (BPointDeviceExt) exts[i];
      }

      // check if the extension exist
      if (pointDeviceExt != null)
      {
        // get all points inside the extension
        //BControlPoint[] points = pointDeviceExt.getPoints();
        BControlPoint[] points = pointDeviceExt.getChildren(BControlPoint.class);
        // found flag
        boolean found = false;
        // search for the right one
        for (int i = 0; i < points.length && !found; i++)
        {
          BControlPoint currentPoint = points[i];
          // check the name
          if (currentPoint.getName().equals(dataPointName))
          {
            // change the flag
            found = true;

            //check if there is a manager associated, if not create one.
            BEnelCloudPointManager pointManager = EnelCloudUtils.makePointManager(currentPoint);

            // check if is a numeric or boolean writable point
            if (currentPoint instanceof BNumericWritable)
            {
              // send the set to point manager
              pointManager.setNumericValue(BDouble.make(value).getDouble(), slotId, validityTime, BStatus.ok);

              // send the Ok code
              return true;
            } else if (currentPoint instanceof BBooleanWritable)
            {
              // send the set to point manager
              boolean booleanValue = false;
              if (value.equals("1") || value.equals("1.0"))
                booleanValue = true;

              pointManager.setBooleanValue(BBoolean.make(booleanValue).getBoolean(), slotId, validityTime, BStatus.ok);

              // send the Ok code
              return true;
            } else if (currentPoint instanceof BStringWritable)
            {
              // send the set to point manager
              pointManager.setStringValue(value, slotId, validityTime, BStatus.ok);

              // send the Ok code
              return true;
            }
            else if (currentPoint instanceof BEnumWritable)
            {
              // send the set to point manager
              pointManager.setEnumValue(BInteger.make(value).getInt(), slotId, validityTime, BStatus.ok);

              // send the Ok code
              return true;
            }
            else
            {
              return false;
              //service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
            }
          }
        }
        // if not found in all points, get error
        if (!found)
        {
          return false;
          //service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST");
        }
      } else // if null, error, point do not exist in this device
      {
        return false;
        //service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST");
      }
      return false;
  }

  public boolean resetValueToPoint(String deviceCode, String dataPointName, BRelTime validityTime, String slotId)
  {

    // fine log messages
    if (logger.isLoggable(Level.FINE))
    {
      logger.fine("deviceCode: " + deviceCode);
      logger.fine("dataPointName: " + dataPointName);
      logger.fine("validityTime: " + validityTime);
      logger.fine("slotId: " + slotId);
    }

    // create the bql query for searching the right device
    BOrd query = BOrd.make("slot:/|bql:select slotPathOrd from driver:Device where name = '" + deviceCode + "'");
    // resolve the bql query
    BITable result = (BITable) query.resolve(this).get();
    // get the columns
    ColumnList columns = result.getColumns();
    // select the frist one with the slotPathOrd
    Column valueColumn = columns.get(0);
    // get the cursor
    TableCursor tableCursor = result.cursor();
    // get the first row
    tableCursor.next();
    // get the device from the slotPathOrd getted
    BDevice device = ((BDevice) tableCursor.get());
    // get all extensions and set to null the points one
    BDeviceExt[] exts = device.getDeviceExts();
    BPointDeviceExt pointDeviceExt = null;

    // search the point extension
    for (int i = 0; i < exts.length; i++)
    {
      if (exts[i] instanceof BPointDeviceExt)
        pointDeviceExt = (BPointDeviceExt) exts[i];
    }

    // check if the extension exist
    if (pointDeviceExt != null)
    {
      // get all points inside the extension
      //BControlPoint[] points = pointDeviceExt.getPoints();
      BControlPoint[] points = pointDeviceExt.getChildren(BControlPoint.class);
      // found flag
      boolean found = false;
      // search for the right one
      for (int i = 0; i < points.length && !found; i++)
      {
        BControlPoint currentPoint = points[i];
        // check the name
        if (currentPoint.getName().equals(dataPointName))
        {
          // change the flag
          found = true;

          //check if there is a manager associated, if not create one.
          BEnelCloudPointManager pointManager = EnelCloudUtils.makePointManager(currentPoint);

          // check if is a numeric or boolean writable point
          if (currentPoint instanceof BNumericWritable)
          {
             // reset the set
            pointManager.resetStandardSet(BString.make(slotId));
            // send the Ok code
            return true;
          } else if (currentPoint instanceof BBooleanWritable)
          {
            // send the set to point manager
            boolean booleanValue = false;

            // reset the set
            pointManager.resetStandardSet(BString.make(slotId));
                       // send the Ok code
            return true;
          } else if (currentPoint instanceof BStringWritable)
          {
            // send the set to point manager
            // reset the set
            pointManager.resetStandardSet(BString.make(slotId));

            // send the Ok code
            return true;
          }
          else if (currentPoint instanceof BEnumWritable)
          {
            // send the set to point manager
            // reset the set
            pointManager.resetStandardSet(BString.make(slotId));

            // send the Ok code
            return true;
          }
          else
          {
            return false;
            //service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
          }
        }
      }
      // if not found in all points, get error
      if (!found)
      {
        return false;
        //service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST");
      }
    } else // if null, error, point do not exist in this device
    {
      return false;
      //service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST");
    }
    return false;
  }

  ////////////////////////////////////////////////////////////////
// POST
////////////////////////////////////////////////////////////////

  @Override
  public void doPost(WebOp op) throws Exception
  {
    try
    {
      // load the main service
      loadService();

      // security check
      if (isRequestWithValidToken(op))
      {
        // get the data from url
        // es. installations/:installationId/devices/code/:deviceCode/datapoint/:dataPointName/commands/setPoint
        StringTokenizer tokenizer = new StringTokenizer(op.getPathInfo(), "/");
        tokenizer.nextToken();                        // installations
        String installatonId = tokenizer.nextToken(); //installationId
        tokenizer.nextToken();                        // devices
        tokenizer.nextToken();                        // code
        String deviceCode = tokenizer.nextToken();    //deviceCode
        tokenizer.nextToken();                        // datapoint
        String dataPointName = tokenizer.nextToken(); //dataPointName
        tokenizer.nextToken();                        // commands
        String commands = tokenizer.nextToken();      //setPoint

        // select the right function
        if (commands.equals("setPoint")) // token function
        {
          // Read data from request
          StringBuilder buffer = new StringBuilder();
          BufferedReader reader = op.getRequest().getReader();
          String line;
          // cycle all lines
          while ((line = reader.readLine()) != null)
          {
            buffer.append(line);
          }
          // all line in one string.
          String data = buffer.toString();

          // read the JSON object and get the value
          JSONObject dataObj = new JSONObject(data);
          String value = dataObj.getString("value");

          // check if we have also the validity time for this set
          BRelTime validityTime = BRelTime.DEFAULT;
          try
          {
            int minutes = Integer.parseInt(dataObj.getString("validityTime"));
            validityTime = BRelTime.makeMinutes(minutes);
          } catch (Exception e) {};

          // check if we have also the slot for this set
          String slotId = "in8";
          try
          {
            slotId = dataObj.getString("slotId");
          } catch (Exception e) {};

          // fine log messages
          if (logger.isLoggable(Level.FINE))
          {
            logger.fine("installatonId: " + installatonId);
            logger.fine("deviceCode: " + deviceCode);
            logger.fine("dataPointName: " + dataPointName);
            logger.fine("value: " + value);
            logger.fine("validityTime: " + validityTime);
            logger.fine("slotId: " + slotId);
          }

          // create the bql query for searching the right device
          BOrd query = BOrd.make("slot:/|bql:select slotPathOrd from driver:Device where name = '" + deviceCode + "'");
          // resolve the bql query
          BITable result = (BITable) query.resolve(this).get();
          // get the columns
          ColumnList columns = result.getColumns();
          // select the frist one with the slotPathOrd
          Column valueColumn = columns.get(0);
          // get the cursor
          TableCursor tableCursor = result.cursor();
          // get the first row
          tableCursor.next();
          // get the device from the slotPathOrd getted
          BDevice device = ((BDevice) tableCursor.get());
          // get all extensions and set to null the points one
          BDeviceExt[] exts = device.getDeviceExts();
          BPointDeviceExt pointDeviceExt = null;

          // search the point extension
          for (int i = 0; i < exts.length; i++)
          {
            if (exts[i] instanceof BPointDeviceExt)
              pointDeviceExt = (BPointDeviceExt) exts[i];
          }

          // check if the extension exist
          if (pointDeviceExt != null)
          {
            // get all points inside the extension
            //BControlPoint[] points = pointDeviceExt.getPoints();
            BControlPoint[] points = pointDeviceExt.getChildren(BControlPoint.class);
            // found flag
            boolean found = false;
            // search for the right one
            for (int i = 0; i < points.length && !found; i++)
            {
              BControlPoint currentPoint = points[i];
              // check the name
              if (currentPoint.getName().equals(dataPointName))
              {
                // change the flag
                found = true;

                //check if there is a manager associated, if not create one.
                BEnelCloudPointManager pointManager = EnelCloudUtils.makePointManager(currentPoint);

                // check if is a numeric or boolean writable point
                if (currentPoint instanceof BNumericWritable)
                {
                  // send the set to point manager
                  pointManager.setNumericValue(BDouble.make(value).getDouble(), slotId, validityTime, BStatus.ok);

                  // send the Ok code
                  op.getWriter().write(buildResponse("true", "" + ((BNumericWritable) currentPoint).getOut().getValue(), "", "" + BAbsTime.now(), ""));
                } else if (currentPoint instanceof BBooleanWritable)
                {
                  // send the set to point manager
                  boolean booleanValue = false;
                  if (value.equals("1") || value.equals("1.0"))
                    booleanValue = true;

                  pointManager.setBooleanValue(BBoolean.make(booleanValue).getBoolean(), slotId, validityTime, BStatus.ok);

                  // send the Ok code
                  op.getWriter().write(buildResponse("true", "" + ((BBooleanWritable) currentPoint).getOut().getValue(), "", "" + BAbsTime.now(), ""));
                } else if (currentPoint instanceof BStringWritable)
                {
                  // send the set to point manager
                  pointManager.setStringValue(value, slotId, validityTime, BStatus.ok);

                  // send the Ok code
                  op.getWriter().write(buildResponse("true", "" + ((BStringWritable) currentPoint).getOut().getValue(), "", "" + BAbsTime.now(), ""));
                }
                else if (currentPoint instanceof BEnumWritable)
                {
                  // send the set to point manager
                  pointManager.setEnumValue(BInteger.make(value).getInt(), slotId, validityTime, BStatus.ok);

                  // send the Ok code
                  op.getWriter().write(buildResponse("true", "" + ((BEnumWritable) currentPoint).getOut().getValue(), "", "" + BAbsTime.now(), ""));
                }
                else
                {
                  op.getResponse().sendError(400, "BAD REQUEST");
                  service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                }
              }
            }
            // if not found in all points, get error
            if (!found)
            {
              op.getResponse().sendError(400, "BAD REQUEST");
              service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST");
            }
          } else // if null, error, point do not exist in this device
          {
            op.getResponse().sendError(400, "BAD REQUEST");
            service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST");
          }
        } else
        {
          op.getResponse().sendError(400, "BAD REQUEST");
          service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST");
        }
      } else
      {
        op.getResponse().sendError(403, "FORBIDDEN");
        service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: FORBIDDEN");
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      op.getResponse().sendError(400, "BAD REQUEST");
      service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST");
    }
  }


////////////////////////////////////////////////////////////////
// GET
////////////////////////////////////////////////////////////////

  @Override
  public void doGet(WebOp op) throws Exception
  {
    try
    {
      // load the main service
      loadService();

      // security check
      if (isRequestWithValidToken(op))
      {
        // get the data from url
        // es. installations/:installationId/devices/code/:deviceCode/datapoint/:dataPointName/commands/setPoint
        StringTokenizer tokenizer = new StringTokenizer(op.getPathInfo(), "/");
        tokenizer.nextToken();                        // installations
        String installatonId = tokenizer.nextToken(); //installationId
        tokenizer.nextToken();                        // devices
        tokenizer.nextToken();                        // code
        String deviceCode = tokenizer.nextToken();    //deviceCode
        tokenizer.nextToken();                        // datapoint
        String dataPointName = tokenizer.nextToken(); //dataPointName
        tokenizer.nextToken();                        // commands
        String commands = tokenizer.nextToken();      //setPoint

        // select the right function
        if (commands.equals("setPoint")) // token function
        {
          // fine log messages
          if (logger.isLoggable(Level.FINE))
          {
            logger.fine("installatonId: " + installatonId);
            logger.fine("deviceCode: " + deviceCode);
            logger.fine("dataPointName: " + dataPointName);
          }

          // create the bql query for searching the right device
          BOrd query = BOrd.make("slot:/|bql:select slotPathOrd from driver:Device where name = '" + deviceCode + "'");
          // resolve the bql query
          BITable result = (BITable) query.resolve(this).get();
          // get the columns
          ColumnList columns = result.getColumns();
          // select the frist one with the slotPathOrd
          Column valueColumn = columns.get(0);
          // get the cursor
          TableCursor tableCursor = result.cursor();
          // get the first row
          tableCursor.next();
          // get the device from the slotPathOrd getted
          BDevice device = ((BDevice) tableCursor.get());
          // get all extensions and set to null the points one
          BDeviceExt[] exts = device.getDeviceExts();
          BPointDeviceExt pointDeviceExt = null;

          // search the point extension
          for (int i = 0; i < exts.length; i++)
          {
            if (exts[i] instanceof BPointDeviceExt)
              pointDeviceExt = (BPointDeviceExt) exts[i];
          }

          // check if the extension exist
          if (pointDeviceExt != null)
          {
            // get all points inside the extension
            //BControlPoint[] points = pointDeviceExt.getPoints();
            BControlPoint[] points = pointDeviceExt.getChildren(BControlPoint.class);
            // found flag
            boolean found = false;
            // search for the right one
            for (int i = 0; i < points.length && !found; i++)
            {
              BControlPoint currentPoint = points[i];
              // check the name
              if (currentPoint.getName().equals(dataPointName))
              {
                // change the flag
                found = true;

                // lease point value
                currentPoint.lease();
                // wait point update
                Thread.sleep(300);

                // check if is a numeric or boolean writable point
                if (currentPoint instanceof BNumericWritable)
                {
                  // send the Ok code with out value
                  op.getWriter().write(buildResponse("true", "" + ((BNumericWritable) currentPoint).getOut().getValue(), "", "" + BAbsTime.now(), "" + ((BNumericWritable) currentPoint).getActiveLevel()));
                } else if (currentPoint instanceof BBooleanWritable)
                {
                  // send the Ok code with out value
                  op.getWriter().write(buildResponse("true", "" + ((BBooleanWritable) currentPoint).getOut().getValue(), "", "" + BAbsTime.now(), ""+((BNumericWritable) currentPoint).getActiveLevel()));
                }
                else if (currentPoint instanceof BStringWritable)
                {
                  // send the Ok code with out value
                  op.getWriter().write(buildResponse("true", "" + ((BStringWritable) currentPoint).getOut().getValue(), "", "" + BAbsTime.now(), ""+((BStringWritable) currentPoint).getActiveLevel()));
                }
                else if (currentPoint instanceof BEnumWritable)
                {
                  // send the Ok code with out value
                  op.getWriter().write(buildResponse("true", "" + ((BEnumWritable) currentPoint).getOut().getValue(), "", "" + BAbsTime.now(), ""+((BEnumWritable) currentPoint).getActiveLevel()));
                }
                else
                {
                  op.getResponse().sendError(400, "BAD REQUEST");
                  service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: BAD REQUEST " + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                }
              }
            }
            // if not found in all points, get error
            if (!found)
            {
              op.getResponse().sendError(400, "BAD REQUEST");
              service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: BAD REQUEST");
            }
          } else // if null, error, point do not exist in this device
          {
            op.getResponse().sendError(400, "BAD REQUEST");
            service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: BAD REQUEST");
          }
        } else
        {
          op.getResponse().sendError(400, "BAD REQUEST");
          service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: BAD REQUEST");
        }
      } else
      {
        op.getResponse().sendError(403, "FORBIDDEN");
        service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: FORBIDDEN");
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      op.getResponse().sendError(400, "BAD REQUEST");
      service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: BAD REQUEST");
    }

  }

  ////////////////////////////////////////////////////////////////
// DELETE
////////////////////////////////////////////////////////////////

  @Override
  public void doDelete(WebOp op) throws Exception
  {
    try
    {
      // load the main service
      loadService();

      // security check
      if (isRequestWithValidToken(op))
      {
        // get the data from url
        // es. installations/:installationId/devices/code/:deviceCode/datapoint/:dataPointName
        StringTokenizer tokenizer = new StringTokenizer(op.getPathInfo(), "/");
        tokenizer.nextToken();                        // installations
        String installatonId = tokenizer.nextToken(); //installationId
        tokenizer.nextToken();                        // devices
        tokenizer.nextToken();                        // code
        String deviceCode = tokenizer.nextToken();    //deviceCode
        tokenizer.nextToken();                        // datapoint
        String dataPointName = tokenizer.nextToken(); //dataPointName

        // Read data from request
        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = op.getRequest().getReader();
        String line;
        // cycle all lines
        while ((line = reader.readLine()) != null)
        {
          buffer.append(line);
        }
        // all line in one string.
        String data = buffer.toString();

        // read the JSON object and get the value, if present, aslo all default
        JSONObject dataObj = null;

        try
        {
          dataObj = new JSONObject(data);
        }catch (Exception e){};

        // check if we have also the slot for this set
        String slotId = "in8";
        try
        {
          slotId = dataObj.getString("slotId");
        } catch (Exception e) {};

        // check if we have the slot of the set, in case, send error
        boolean wrongData = false;
        try
        {
          dataObj.getString("value");
          wrongData = true;
        } catch (Exception e) {};

        // error, exit
        if (wrongData)
        {
          op.getResponse().sendError(400, "BAD REQUEST");
          service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: BAD REQUEST " + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
          return;
        }

          // fine log messages
          if (logger.isLoggable(Level.FINE))
          {
            logger.fine("installatonId: " + installatonId);
            logger.fine("deviceCode: " + deviceCode);
            logger.fine("dataPointName: " + dataPointName);
          }

          // create the bql query for searching the right device
          BOrd query = BOrd.make("slot:/|bql:select slotPathOrd from driver:Device where name = '" + deviceCode + "'");
          // resolve the bql query
          BITable result = (BITable) query.resolve(this).get();
          // get the columns
          ColumnList columns = result.getColumns();
          // select the frist one with the slotPathOrd
          Column valueColumn = columns.get(0);
          // get the cursor
          TableCursor tableCursor = result.cursor();
          // get the first row
          tableCursor.next();
          // get the device from the slotPathOrd getted
          BDevice device = ((BDevice) tableCursor.get());
          // get all extensions and set to null the points one
          BDeviceExt[] exts = device.getDeviceExts();
          BPointDeviceExt pointDeviceExt = null;

          // search the point extension
          for (int i = 0; i < exts.length; i++)
          {
            if (exts[i] instanceof BPointDeviceExt)
              pointDeviceExt = (BPointDeviceExt) exts[i];
          }

          // check if the extension exist
          if (pointDeviceExt != null)
          {
            // get all points inside the extension
            //BControlPoint[] points = pointDeviceExt.getPoints();
            BControlPoint[] points = pointDeviceExt.getChildren(BControlPoint.class);
            // found flag
            boolean found = false;
            // search for the right one
            for (int i = 0; i < points.length && !found; i++)
            {
              BControlPoint currentPoint = points[i];
              // check the name
              if (currentPoint.getName().equals(dataPointName))
              {
                // change the flag
                found = true;

                //check if there is a manager associated, if not create one.
                BEnelCloudPointManager pointManager = EnelCloudUtils.makePointManager(currentPoint);

                // check if is a numeric or boolean writable point
                if (currentPoint instanceof BNumericWritable)
                {
                  //check if the slot is already in null
                  String failOver = "";
                  BStatusNumeric currentSlot = (BStatusNumeric)((BNumericWritable) currentPoint).get(slotId);
                  if (currentSlot.getStatus().isNull())
                    failOver = "Deleted slot already in null";

                  //reset the set
                  pointManager.resetStandardSet(BString.make(slotId));

                  // send the Ok code
                  op.getWriter().write(buildResponse("true", "" + ((BNumericWritable) currentPoint).getOut().getValue(), failOver, "" + BAbsTime.now(), ""));
                } else if (currentPoint instanceof BBooleanWritable)
                {
                  //check if the slot is already in null
                  String failOver = "";
                  BStatusBoolean currentSlot = (BStatusBoolean)((BBooleanWritable) currentPoint).get(slotId);
                  if (currentSlot.getStatus().isNull())
                    failOver = "Deleted slot already in null";

                  // reset the set
                  pointManager.resetStandardSet(BString.make(slotId));

                  // send the Ok code
                  op.getWriter().write(buildResponse("true", "" + ((BBooleanWritable) currentPoint).getOut().getValue(), failOver, "" + BAbsTime.now(), ""));
                }
                else if (currentPoint instanceof BStringWritable)
                {
                  //check if the slot is already in null
                  String failOver = "";
                  BStatusString currentSlot = (BStatusString)((BStringWritable) currentPoint).get(slotId);
                  if (currentSlot.getStatus().isNull())
                    failOver = "Deleted slot already in null";

                  // reset the set
                  pointManager.resetStandardSet(BString.make(slotId));

                  // send the Ok code
                  op.getWriter().write(buildResponse("true", "" + ((BStringWritable) currentPoint).getOut().getValue(), failOver, "" + BAbsTime.now(), ""));
                }
                else if (currentPoint instanceof BEnumWritable)
                {
                  //check if the slot is already in null
                  String failOver = "";
                  BStatusEnum currentSlot = (BStatusEnum)((BEnumWritable) currentPoint).get(slotId);
                  if (currentSlot.getStatus().isNull())
                    failOver = "Deleted slot already in null";

                  // reset the set
                  pointManager.resetStandardSet(BString.make(slotId));

                  // send the Ok code
                  op.getWriter().write(buildResponse("true", "" + ((BEnumWritable) currentPoint).getOut().getValue(), failOver, "" + BAbsTime.now(), ""));
                }
                else
                {
                  op.getResponse().sendError(400, "BAD REQUEST");
                  service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                }
              }
            }
            // if not found in all points, get error
            if (!found)
            {
              op.getResponse().sendError(400, "BAD REQUEST");
              service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
            }
          } else // if null, error, point do not exist in this device
          {
            op.getResponse().sendError(400, "BAD REQUEST");
            service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
          }
      } else
      {
        op.getResponse().sendError(403, "FORBIDDEN");
        service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: BAD REQUEST");
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      op.getResponse().sendError(400, "BAD REQUEST");
      service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: BAD REQUEST");
    }
  }


  ////////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

  private void loadService()
  {
    // check if the service is loaded, if not, load it.
    try
    {
      if (service == null)
        service = (BEnelCloudService)getParent();
    }
    catch (Exception e)
    {
      //error - servlet in wrong location.
      e.printStackTrace();
    }
  }

  // utility fo building the string response
  private String buildResponse(String retVal, String output, String failOver, String timestamp, String slotId )
  {

    // add slotId info, if set.
    if (slotId != "")
      slotId =  "\"slotId\":\"" + slotId + "\",";

    String result = "{"+
            "\"retval\":\"" + retVal+ "\"," +
            "\"dataObject\":{" +
            "\"output\":\"" + output +"\"," + slotId +
            "\"failOver\":\"" + failOver + "\"," +
            "\"timestamp\":\"" + timestamp + "\"" +
            "}" +
            "}";

    return result;
  }


  // check if the token on the request is valid
  private boolean isRequestWithValidToken(WebOp op)
  {

    try
    {
      String token = op.getRequest().getHeader("x-access-token");
      // ask to the service for the validity
      if (service.getAccessToken().equals(token))
        return true;
      else
        return false;
    }
    catch (Exception e)
    {
      return false; // something wrong, security check failed.
    }
  }

  ////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

  @Override
  public BIcon getIcon() {
    return BIcon.std("selectAll.png");
  }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
  private BEnelCloudService service = null;
  public static final Logger logger = Logger.getLogger("EnelCloudSetpointManager");

}
