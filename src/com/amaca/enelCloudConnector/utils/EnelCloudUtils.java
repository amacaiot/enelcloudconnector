package com.amaca.enelCloudConnector.utils;

import com.amaca.enelCloudConnector.point.BEnelCloudPointManager;

import javax.baja.control.BControlPoint;
import javax.baja.sys.BComponent;
import javax.baja.sys.BLink;

/**
 * Created by Amaca on 29/07/2018.
 */
public final class EnelCloudUtils
{
    // make or return the point manager associated to a specific control point
    public static final BEnelCloudPointManager makePointManager(BControlPoint point)
    {
        // create the currentPointManager attribute
        BEnelCloudPointManager currentPointManager = null;

        // check if already exist one component
        try
        {
            currentPointManager = point.getChildren(BEnelCloudPointManager.class)[0];
        }
        catch (Exception e){System.out.println("NO ELEMENT");};

        // if not, create a new one and add inside the source point
        if (currentPointManager == null)
        {
            currentPointManager = new BEnelCloudPointManager(point);
            point.add("CloudLabPointManager_" + point.getName(), currentPointManager);
        }

        //return the point manager
        return currentPointManager;
    }

    // make link between two components
    public static final void makeLink(String linkName, BComponent sourceComponent, BComponent targetComponent, String sourceSlotName, String targetSlotName)
    {
        try
        {
            if(targetComponent == null || sourceComponent == null)
            {
                String str = "Either source or target is null for linking! --> ";

                // check name
                if (linkName != null)
                    str += " name = " + linkName +", ";
                else
                    str += " name = NULL, ";

                // check source component
                if (sourceComponent != null)
                    str += " source = " + sourceComponent.getName() +", ";
                else
                    str += " source = NULL, ";

                // check target component
                if (targetComponent != null)
                    str += " target = " + targetComponent.getName() +", ";
                else
                    str += " target = NULL, ";

                //check source slot name
                if (sourceSlotName != null)
                    str += " srcSlot = " + sourceSlotName +", ";
                else
                    str += " srcSlot = NULL, ";

                // check target slot name
                if (targetSlotName != null)
                    str += " targetSlot = " + targetSlotName +"";
                else
                    str += " targetSlot = NULL";

                // log and exit
                //logTrace(str);
                System.out.println("--> " + str );
                return;
            }
            if(sourceComponent.get(sourceSlotName) == null || targetComponent.get(targetSlotName) == null)
            {
                if(sourceComponent.get(sourceSlotName) == null)
                    //logTrace("Link Source " + sourceComponent.getName() + "." + sourceSlotName + " is null.");

                if(targetComponent.get(targetSlotName) == null)
                    //logTrace("Link Target " + targetComponent.getName() + "." + targetSlotName + " is null.");

                if(targetComponent.get(linkName) != null)
                    targetComponent.remove(linkName);
                return;
            }

            BLink link = new BLink(sourceComponent.getHandleOrd(), sourceSlotName, targetSlotName, true);
            if(linkName != null)
            {
                // have name to check against, therefore check if it already exists or not
                if(targetComponent.get(linkName) == null)
                    targetComponent.add(linkName, link);
                else
                {
                    // cannot use SET to add a link. Instead, inform user that they cant use the name they specified.
                    //logTrace("Name " + linkName + " already exists. Unable to create link");
                    return;
                }
            }
            else
                targetComponent.add(linkName, link);

            // activate link
            link.activate();
        }
        catch(IllegalStateException ie)
        {
            System.out.println("IllegalStateException on link to " + targetComponent.getName());
        }
        catch(Exception ie)
        {
            System.out.println("General Exception on link to " + targetComponent.getName());
            ie.printStackTrace();
        }
    }


    // make link between two components but with source "topic "
    public static final void makeLinkTopic(String linkName, BComponent sourceComponent, BComponent targetComponent, String sourceSlotName, String targetSlotName)
    {
        try
        {
            if(targetComponent == null || sourceComponent == null)
            {
                String str = "Either source or target is null for linking! --> ";

                // check name
                if (linkName != null)
                    str += " name = " + linkName +", ";
                else
                    str += " name = NULL, ";

                // check source component
                if (sourceComponent != null)
                    str += " source = " + sourceComponent.getName() +", ";
                else
                    str += " source = NULL, ";

                // check target component
                if (targetComponent != null)
                    str += " target = " + targetComponent.getName() +", ";
                else
                    str += " target = NULL, ";

                //check source slot name
                if (sourceSlotName != null)
                    str += " srcSlot = " + sourceSlotName +", ";
                else
                    str += " srcSlot = NULL, ";

                // check target slot name
                if (targetSlotName != null)
                    str += " targetSlot = " + targetSlotName +"";
                else
                    str += " targetSlot = NULL";

                // log and exit
                //logTrace(str);
                System.out.println("--> " + str );
                return;
            }

            BLink link = new BLink(sourceComponent.getHandleOrd(), sourceSlotName, targetSlotName, true);
            if(linkName != null)
            {
                // have name to check against, therefore check if it already exists or not
                if(targetComponent.get(linkName) == null)
                    targetComponent.add(linkName, link);
                else
                {
                    // cannot use SET to add a link. Instead, inform user that they cant use the name they specified.
                    //logTrace("Name " + linkName + " already exists. Unable to create link");
                    return;
                }
            }
            else
                targetComponent.add(linkName, link);

            // activate link
            link.activate();
        }
        catch(IllegalStateException ie)
        {
            System.out.println("IllegalStateException on link to " + targetComponent.getName());
        }
        catch(Exception ie)
        {
            System.out.println("General Exception on link to " + targetComponent.getName());
            ie.printStackTrace();
        }
    }
}

