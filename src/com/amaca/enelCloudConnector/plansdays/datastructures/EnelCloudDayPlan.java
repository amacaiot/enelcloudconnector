package com.amaca.enelCloudConnector.plansdays.datastructures;

import javax.baja.schedule.BDaySchedule;
import javax.baja.status.BStatusBoolean;
import javax.baja.status.BStatusEnum;
import javax.baja.status.BStatusNumeric;
import javax.baja.status.BStatusString;
import javax.baja.sys.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by Amaca on 05/08/2018.
 */
public class EnelCloudDayPlan
{

////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    public EnelCloudDayPlan(String object, String slotId) throws Exception
    {
        StringTokenizer tokenizer = new StringTokenizer(object, ";");
        // read the main parameters for the id
        this.timestamp = decodeTimestampString(tokenizer.nextToken());
        String deviceName = tokenizer.nextToken();
        String pointName = tokenizer.nextToken();

        // get the slotid
        this.slotId = slotId;

        // save attributes and calculate the uniqueID
        this.deviceName = deviceName;
        this.pointName = pointName;
        this.uniqueId = deviceName + "_" + pointName;
        // create the objects array
        objects = new ArrayList<String>();
        objects.add(object);
    }

 ////////////////////////////////////////////////////////////////
// Main
////////////////////////////////////////////////////////////////

    public void addValue(String newObject)
    {
        objects.add(newObject);
    }

    // get the day schedule
    public BDaySchedule getDayScheduleObject() throws Exception
    {
        // create a new dayschedule
        BDaySchedule daySchedule = new BDaySchedule();

        //set the start and end time
        BTime startTime = BTime.MIDNIGHT;
        BTime endTime = BTime.MIDNIGHT;

        // cycle for every 96 values (24*4) data every 15mins
        for (int i = 0; i < objects.size() ; i++)
        {
            startTime = getTime(objects.get(i));

            try
            {
                endTime = getTime(objects.get(i + 1));
            }
            catch (Exception e)
            {
                endTime = BTime.MIDNIGHT;
            }
            //add the  value
            daySchedule.add(startTime,endTime, new BStatusNumeric(getNumericValue(objects.get(i))));
        }

        // return the complete daySchedule
        return daySchedule;
    }

    // get the day schedule enum
    public BDaySchedule getDayScheduleObjectEnum() throws Exception
    {
        // create a new dayschedule
        BDaySchedule daySchedule = new BDaySchedule();

        //set the start and end time
        BTime startTime = BTime.MIDNIGHT;
        BTime endTime = BTime.MIDNIGHT;

        // cycle for every 96 values (24*4) data every 15mins
        for (int i = 0; i < objects.size() ; i++)
        {
            startTime = getTime(objects.get(i));

            try
            {
                endTime = getTime(objects.get(i + 1));
            }
            catch (Exception e)
            {
                endTime = BTime.MIDNIGHT;
            }
            //add the  value
            daySchedule.add(startTime,endTime, new BStatusEnum(BDynamicEnum.make((int)getNumericValue(objects.get(i)))));
        }

        // return the complete daySchedule
        return daySchedule;
    }

    // get the day schedule boolean
    public BDaySchedule getDayScheduleObjectBoolean() throws Exception
    {
        // create a new dayschedule
        BDaySchedule daySchedule = new BDaySchedule();

        //set the start and end time
        BTime startTime = BTime.MIDNIGHT;
        BTime endTime = BTime.MIDNIGHT;

        // cycle for every 96 values (24*4) data every 15mins
        for (int i = 0; i < objects.size() ; i++)
        {
            startTime = getTime(objects.get(i));

            try
            {
                endTime = getTime(objects.get(i + 1));
            }
            catch (Exception e)
            {
                endTime = BTime.MIDNIGHT;
            }
            //add the  value
            boolean valueToSet = false;
            if (getNumericValue(objects.get(i)) == 0)
                valueToSet = false;
            else
                valueToSet = true;

            daySchedule.add(startTime,endTime, new BStatusBoolean(valueToSet));
        }

        // return the complete daySchedule
        return daySchedule;
    }

    // get the day schedule string
    public BDaySchedule getDayScheduleObjectString() throws Exception
    {
        // create a new dayschedule
        BDaySchedule daySchedule = new BDaySchedule();

        //set the start and end time
        BTime startTime = BTime.MIDNIGHT;
        BTime endTime = BTime.MIDNIGHT;

        // cycle for every 96 values (24*4) data every 15mins
        for (int i = 0; i < objects.size() ; i++)
        {
            startTime = getTime(objects.get(i));

            try
            {
                endTime = getTime(objects.get(i + 1));
            }
            catch (Exception e)
            {
                endTime = BTime.MIDNIGHT;
            }
            // add the value
            daySchedule.add(startTime,endTime, new BStatusString(getStringValue(objects.get(i))));
        }

        // return the complete daySchedule
        return daySchedule;
    }


    public BTime getTime(String object) throws Exception
    {
        StringTokenizer tokenizer = new StringTokenizer(object, ";");
        // read the main parameters for the id
        return decodeTimestampString(tokenizer.nextToken()).getTime();
    }

    public double getNumericValue(String object)throws Exception
    {
        StringTokenizer tokenizer = new StringTokenizer(object, ";");
        tokenizer.nextToken();// timestamp
        tokenizer.nextToken(); // device
        tokenizer.nextToken(); // point
        String value = tokenizer.nextToken(); //value

        return BDouble.make(value).getDouble();
    }

    public String getStringValue(String object)throws Exception
    {
        StringTokenizer tokenizer = new StringTokenizer(object, ";");
        tokenizer.nextToken();// timestamp
        tokenizer.nextToken(); // device
        tokenizer.nextToken(); // point
        String value = tokenizer.nextToken(); //value

        return value;
    }

    // decode the timestamp format into stnd one
    public BAbsTime decodeTimestampString(String timestamp)
    {

        StringTokenizer tokenizerDate = new StringTokenizer(timestamp.substring(0, timestamp.lastIndexOf(" ")), "-");
        int year = Integer.parseInt(tokenizerDate.nextToken());
        int month = Integer.parseInt(tokenizerDate.nextToken()) - 1;
        int day = Integer.parseInt(tokenizerDate.nextToken());

        StringTokenizer tokenizerTime = new StringTokenizer(timestamp.substring(timestamp.lastIndexOf(" ")+1),":");
        int hours = Integer.parseInt(tokenizerTime.nextToken());;
        int minutes = Integer.parseInt(tokenizerTime.nextToken());

        return BAbsTime.make(year, BMonth.make(month), day, hours, minutes);
    }

    public String getDeviceName()
    {
        return deviceName;
    }

    public String getPointName()
    {
        return pointName;
    }

    public String getSlotId()
    {
        return slotId;
    }
    ////////////////////////////////////////////////////////////////
// Equals
////////////////////////////////////////////////////////////////

    @Override
    public boolean equals(Object obj)
    {
        EnelCloudDayPlan argument = (EnelCloudDayPlan)obj;

        if (argument.uniqueId.equals(uniqueId))
            return true;
        else
            return false;
    }

 ////////////////////////////////////////////////////////////////
// Access
////////////////////////////////////////////////////////////////

    public int getDay()
    {
        return timestamp.getDay();
    }

    public int getYear()
    {
        return timestamp.getYear();
    }

    public BMonth getMonth()
    {
        return timestamp.getMonth();
    }

    public String getUniqueId()
    {
        return uniqueId;
    }

    ////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
    String deviceName = null;
    String pointName = null;
    String uniqueId = null;
    String slotId = "in8";
    BAbsTime timestamp = null;
    ArrayList<String> objects;

}
