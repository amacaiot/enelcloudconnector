package com.amaca.enelCloudConnector.plansdays;

import com.amaca.enelCloudConnector.BEnelCloudService;
import com.amaca.enelCloudConnector.plansdays.datastructures.EnelCloudDayPlan;
import com.amaca.enelCloudConnector.point.BEnelCloudPointManager;
import com.amaca.enelCloudConnector.utils.EnelCloudUtils;
import com.tridium.json.JSONArray;
import com.tridium.json.JSONObject;

import javax.baja.collection.BITable;
import javax.baja.collection.Column;
import javax.baja.collection.ColumnList;
import javax.baja.collection.TableCursor;
import javax.baja.control.BControlPoint;
import javax.baja.driver.BDevice;
import javax.baja.driver.BDeviceExt;
import javax.baja.driver.point.BPointDeviceExt;
import javax.baja.naming.BOrd;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.schedule.*;
import javax.baja.status.BStatus;
import javax.baja.sys.*;
import javax.baja.util.BFolder;
import javax.baja.web.BWebServlet;
import javax.baja.web.WebOp;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 * Created by Amaca on 01/08/2018.
 */
@NiagaraProperty(
        name = "servletName",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "niagaraplanday"
)
@NiagaraProperty(
        name = "GeneratedSchedulersContainer",
        type = "BFolder",
        defaultValue = "new BFolder()"
)

@NiagaraType
public class BEnelCloudPlansDaysManager extends BWebServlet
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.plansdays.BEnelCloudPlansDaysManager(3438357777)1.0$ @*/
/* Generated Sun Aug 05 10:54:24 CEST 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "servletName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code servletName} property.
   * @see #getServletName
   * @see #setServletName
   */
  public static final Property servletName = newProperty(Flags.READONLY, "niagaraplanday", null);
  
  /**
   * Get the {@code servletName} property.
   * @see #servletName
   */
  public String getServletName() { return getString(servletName); }
  
  /**
   * Set the {@code servletName} property.
   * @see #servletName
   */
  public void setServletName(String v) { setString(servletName, v, null); }

////////////////////////////////////////////////////////////////
// Property "GeneratedSchedulersContainer"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code GeneratedSchedulersContainer} property.
   * @see #getGeneratedSchedulersContainer
   * @see #setGeneratedSchedulersContainer
   */
  public static final Property GeneratedSchedulersContainer = newProperty(0, new BFolder(), null);
  
  /**
   * Get the {@code GeneratedSchedulersContainer} property.
   * @see #GeneratedSchedulersContainer
   */
  public BFolder getGeneratedSchedulersContainer() { return (BFolder)get(GeneratedSchedulersContainer); }
  
  /**
   * Set the {@code GeneratedSchedulersContainer} property.
   * @see #GeneratedSchedulersContainer
   */
  public void setGeneratedSchedulersContainer(BFolder v) { set(GeneratedSchedulersContainer, v, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudPlansDaysManager.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/




////////////////////////////////////////////////////////////////
// POST
////////////////////////////////////////////////////////////////

    public boolean setPlanDay(JSONObject data) //TODO: No commands looks like this on the list.
    {
        try
        {
            // load the main service
            loadService();

                // get the data from url
                // /installations/sendPlan/ ID_IMPIANTO HTTP/1.1
                //StringTokenizer tokenizer = new StringTokenizer("id", "/");
                //tokenizer.nextToken();                        // installations
                //String commands = tokenizer.nextToken();      // sendPlan
                //String installatonId = tokenizer.nextToken(); //installationId

                // select the right function

                    /*DATA STRUCTURE
                        "planDay": {string},    // Giorno di applicazione dello schedule NOTA: verificare che la variabile sia una stringa
                        "slotId": {string}
                        "content": {tuple}      // Tuple contenenti il piano diviso in "Timestamp;Sistema;Variabile;Valore"
                     */



                    // read the JSON object and get the value
                    JSONObject dataObj = new JSONObject(data);

                    // get the plan day
                    String planDay = dataObj.getString("planDay"); //YYYY-MM-DD

                    // parse the date format
                    StringTokenizer tokenizerDate = new StringTokenizer(planDay, "-");
                    int year = Integer.parseInt(tokenizerDate.nextToken());
                    int month = Integer.parseInt(tokenizerDate.nextToken()) - 1;
                    int day = Integer.parseInt(tokenizerDate.nextToken());

                    BAbsTime planDayReceived = BAbsTime.make(year, BMonth.make(month), day);

                    // check if we have also the slot for this set
                    String slotId = "in8";
                    try
                    {
                        slotId = dataObj.getString("slotId");
                    } catch (Exception e) {};

                    // get the content
                    JSONArray content = dataObj.getJSONArray("content");

                    // create an array of planDay
                    ArrayList<EnelCloudDayPlan> plans = new ArrayList<EnelCloudDayPlan>();

                    // cycle all data inside the array
                    for (int i = 0; i < content.length(); i++)
                    {
                        String currentObject = content.getString(i);
                        // check if already exist for this point the structure
                        EnelCloudDayPlan currentDayPlan = null;

                        try
                        {
                            currentDayPlan = plans.get(plans.indexOf(new EnelCloudDayPlan(currentObject, slotId)));
                        }catch (Exception e){};

                        if (currentDayPlan == null) //create a new dayPlan
                            plans.add(new EnelCloudDayPlan(currentObject, slotId));
                        else // add the new object to the existing one dayplan
                            currentDayPlan.addValue(currentObject);
                    }

                    //cycle for check if all points exists, if not, BAD request
                    for (int i = 0; i < plans.size(); i++)
                    {
                        EnelCloudDayPlan currentPlan = plans.get(i);
                        if (findRealPoint(currentPlan.getDeviceName(), currentPlan.getPointName()) == null)
                            throw new Exception();
                    }

                    //cycle all plans/device based
                    for (int i = 0; i < plans.size(); i++)
                    {
                        EnelCloudDayPlan currentPlan = plans.get(i);
                        BWeeklySchedule weeklySchedule = searchExistingSchedule("Schedule_" + currentPlan.getUniqueId());

                        if (weeklySchedule == null) // create a new schedule
                        {
                            //check if there is a manager associated, if not create one.
                            BEnelCloudPointManager pointManager = EnelCloudUtils.makePointManager(findRealPoint(currentPlan.getDeviceName(), currentPlan.getPointName()));

                            // new instance
                            BWeeklySchedule newSchedule = null;
                            // create a new schedule
                            if (pointManager.isNumeric())
                                newSchedule = new BNumericSchedule();
                            else if (pointManager.isBoolean())
                            {
                                newSchedule = new BBooleanSchedule();
                                newSchedule.getDefaultOutput().setStatus(BStatus.nullStatus);
                            }
                            else if (pointManager.isString())
                                newSchedule = new BStringSchedule();
                            else if (pointManager.isEnum())
                                newSchedule = new BEnumSchedule();

                            //add the special event with the day
                            // create a new dailySchedule
                            BDailySchedule newDailyCustomEventSchedule = new BDailySchedule();
                            // set the day value coming from profile associated to this task
                            BDaySchedule currentDailySchedule = null;
                            if (pointManager.isNumeric())
                                currentDailySchedule = currentPlan.getDayScheduleObject();
                            else if (pointManager.isBoolean())
                                currentDailySchedule = currentPlan.getDayScheduleObjectBoolean();
                            else if (pointManager.isString())
                                currentDailySchedule = currentPlan.getDayScheduleObjectString();
                            else if (pointManager.isEnum())
                                currentDailySchedule = currentPlan.getDayScheduleObjectEnum();

                            newDailyCustomEventSchedule.setDay((BDaySchedule) currentDailySchedule.newCopy());
                            // set the current day
                            BDateSchedule dateSchedule = new BDateSchedule(currentPlan.getDay(), currentPlan.getMonth(), currentPlan.getYear());
                            // add to special event the dateschedule
                            newDailyCustomEventSchedule.setDays(dateSchedule);
                            // add this special event
                            newSchedule.addSpecialEvent("DayPlan_" + currentPlan.getUniqueId() + "?", newDailyCustomEventSchedule);
                            //add schedule into conatiner with the right id
                            getGeneratedSchedulersContainer().add("Schedule_" + currentPlan.getUniqueId(),newSchedule);

                            //set the right slotID for scheduling manager, based on high priorirty task.
                            pointManager.doResetStandardSet(BString.make(pointManager.getPlansDaysManagerInputId())); // first reset the previous one
                            pointManager.setPlansDaysManagerInputId(currentPlan.getSlotId());

                            // make link with scheduler to the real point manager
                            if (pointManager.isNumeric())
                                EnelCloudUtils.makeLink("Link_PD_" + currentPlan.getDeviceName() + "_" + currentPlan.getPointName(), newSchedule, pointManager, "out", "inputPlansDaysManagerNumeric");
                            else if (pointManager.isBoolean())
                                EnelCloudUtils.makeLink("Link_PD_" + currentPlan.getDeviceName() + "_" + currentPlan.getPointName(), newSchedule, pointManager, "out", "inputPlansDaysManagerBoolean");
                            else if (pointManager.isString())
                                EnelCloudUtils.makeLink("Link_PD_" + currentPlan.getDeviceName() + "_" + currentPlan.getPointName(), newSchedule, pointManager, "out", "inputPlansDaysManagerString");
                            else if (pointManager.isEnum())
                                EnelCloudUtils.makeLink("Link_PD_" + currentPlan.getDeviceName() + "_" + currentPlan.getPointName(), newSchedule, pointManager, "out", "inputPlansDaysManagerEnum");
                        }
                        else//update the existing one
                        {
                            // delete previous special event in the same day of the newone
                            BDailySchedule[] allSpecialEvents = weeklySchedule.getSpecialEventsChildren();
                            for (int j = 0; j < allSpecialEvents.length; j++)
                            {
                                BDateSchedule dateSch = (BDateSchedule)allSpecialEvents[j].getDays();
                                if (dateSch.getDay() == currentPlan.getDay() && dateSch.getMonth() == currentPlan.getMonth().getOrdinal() && dateSch.getYear() == currentPlan.getYear())
                                    weeklySchedule.getSpecialEvents().remove(allSpecialEvents[j].getName());
                            }

                            //check if there is a manager associated, if not create one.
                            BEnelCloudPointManager pointManager = EnelCloudUtils.makePointManager(findRealPoint(currentPlan.getDeviceName(), currentPlan.getPointName()));


                            BDailySchedule newDailyCustomEventSchedule = new BDailySchedule();
                            // set the day value coming from profile associated to this task
                            BDaySchedule currentDailySchedule = null;
                            if (pointManager.isNumeric())
                                currentDailySchedule = currentPlan.getDayScheduleObject();
                            else if (pointManager.isBoolean())
                                currentDailySchedule = currentPlan.getDayScheduleObjectBoolean();
                            else if (pointManager.isString())
                                currentDailySchedule = currentPlan.getDayScheduleObjectString();
                            else if (pointManager.isEnum())
                                currentDailySchedule = currentPlan.getDayScheduleObjectEnum();
                            newDailyCustomEventSchedule.setDay((BDaySchedule) currentDailySchedule.newCopy());

                            // set the current day
                            BDateSchedule dateSchedule = new BDateSchedule(currentPlan.getDay(), currentPlan.getMonth(), currentPlan.getYear());
                            // add to special event the dateschedule
                            newDailyCustomEventSchedule.setDays(dateSchedule);
                            // add this special event
                            weeklySchedule.addSpecialEvent("DayPlan_" + currentPlan.getUniqueId() + "?", newDailyCustomEventSchedule);
                            //set the right slotID for scheduling manager, based on high priorirty task.
                            //check if there is a manager associated, if not create one.
                            pointManager.doResetStandardSet(BString.make(pointManager.getPlansDaysManagerInputId())); // first reset the previous one
                            pointManager.setPlansDaysManagerInputId(currentPlan.getSlotId());
                            pointManager.updateValues();
                        }

                    }
                    System.out.println("New Day Plan created");
                    return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Bad Request");
            return false;
        }
    }

    @Override
    public void doPost(WebOp op) throws Exception
    {
        try
        {
            // load the main service
            loadService();

            // security check
            if (isRequestWithValidToken(op))
            {
                // get the data from url
                // /installations/sendPlan/ ID_IMPIANTO HTTP/1.1
                StringTokenizer tokenizer = new StringTokenizer(op.getPathInfo(), "/");
                tokenizer.nextToken();                        // installations
                String commands = tokenizer.nextToken();      // sendPlan
                String installatonId = tokenizer.nextToken(); //installationId

                // select the right function
                if (commands.equals("sendPlan")) // token function
                {
                    /*DATA STRUCTURE
                        "planDay": {string},    // Giorno di applicazione dello schedule NOTA: verificare che la variabile sia una stringa
                        "slotId": {string}
                        "content": {tuple}      // Tuple contenenti il piano diviso in "Timestamp;Sistema;Variabile;Valore"
                     */

                    // Read data from request
                    StringBuilder buffer = new StringBuilder();
                    BufferedReader reader = op.getRequest().getReader();
                    String line;
                    // cycle all lines
                    while ((line = reader.readLine()) != null)
                    {
                        buffer.append(line);
                    }
                    // all line in one string.
                    String data = buffer.toString();

                    // read the JSON object and get the value
                    JSONObject dataObj = new JSONObject(data);

                    // get the plan day
                    String planDay = dataObj.getString("planDay"); //YYYY-MM-DD

                    // parse the date format
                    StringTokenizer tokenizerDate = new StringTokenizer(planDay, "-");
                    int year = Integer.parseInt(tokenizerDate.nextToken());
                    int month = Integer.parseInt(tokenizerDate.nextToken()) - 1;
                    int day = Integer.parseInt(tokenizerDate.nextToken());

                    BAbsTime planDayReceived = BAbsTime.make(year, BMonth.make(month), day);

                    // check if we have also the slot for this set
                    String slotId = "in8";
                    try
                    {
                        slotId = dataObj.getString("slotId");
                    } catch (Exception e) {};

                    // get the content
                    JSONArray content = dataObj.getJSONArray("content");

                    // create an array of planDay
                    ArrayList<EnelCloudDayPlan> plans = new ArrayList<EnelCloudDayPlan>();

                    // cycle all data inside the array
                    for (int i = 0; i < content.length(); i++)
                    {
                        String currentObject = content.getString(i);
                        // check if already exist for this point the structure
                        EnelCloudDayPlan currentDayPlan = null;

                        try
                        {
                            currentDayPlan = plans.get(plans.indexOf(new EnelCloudDayPlan(currentObject, slotId)));
                        }catch (Exception e){};

                        if (currentDayPlan == null) //create a new dayPlan
                            plans.add(new EnelCloudDayPlan(currentObject, slotId));
                        else // add the new object to the existing one dayplan
                            currentDayPlan.addValue(currentObject);
                    }

                    //cycle for check if all points exists, if not, BAD request
                    for (int i = 0; i < plans.size(); i++)
                    {
                        EnelCloudDayPlan currentPlan = plans.get(i);
                        if (findRealPoint(currentPlan.getDeviceName(), currentPlan.getPointName()) == null)
                            throw new Exception();
                    }

                    //cycle all plans/device based
                    for (int i = 0; i < plans.size(); i++)
                    {
                        EnelCloudDayPlan currentPlan = plans.get(i);
                        BWeeklySchedule weeklySchedule = searchExistingSchedule("Schedule_" + currentPlan.getUniqueId());

                        if (weeklySchedule == null) // create a new schedule
                        {
                            //check if there is a manager associated, if not create one.
                            BEnelCloudPointManager pointManager = EnelCloudUtils.makePointManager(findRealPoint(currentPlan.getDeviceName(), currentPlan.getPointName()));

                            // new instance
                            BWeeklySchedule newSchedule = null;
                            // create a new schedule
                            if (pointManager.isNumeric())
                                newSchedule = new BNumericSchedule();
                            else if (pointManager.isBoolean())
                            {
                                newSchedule = new BBooleanSchedule();
                                newSchedule.getDefaultOutput().setStatus(BStatus.nullStatus);
                            }
                            else if (pointManager.isString())
                                newSchedule = new BStringSchedule();
                            else if (pointManager.isEnum())
                                newSchedule = new BEnumSchedule();

                            //add the special event with the day
                            // create a new dailySchedule
                            BDailySchedule newDailyCustomEventSchedule = new BDailySchedule();
                            // set the day value coming from profile associated to this task
                            BDaySchedule currentDailySchedule = null;
                            if (pointManager.isNumeric())
                                currentDailySchedule = currentPlan.getDayScheduleObject();
                            else if (pointManager.isBoolean())
                                currentDailySchedule = currentPlan.getDayScheduleObjectBoolean();
                            else if (pointManager.isString())
                                currentDailySchedule = currentPlan.getDayScheduleObjectString();
                            else if (pointManager.isEnum())
                                currentDailySchedule = currentPlan.getDayScheduleObjectEnum();

                            newDailyCustomEventSchedule.setDay((BDaySchedule) currentDailySchedule.newCopy());
                            // set the current day
                            BDateSchedule dateSchedule = new BDateSchedule(currentPlan.getDay(), currentPlan.getMonth(), currentPlan.getYear());
                            // add to special event the dateschedule
                            newDailyCustomEventSchedule.setDays(dateSchedule);
                            // add this special event
                            newSchedule.addSpecialEvent("DayPlan_" + currentPlan.getUniqueId() + "?", newDailyCustomEventSchedule);
                            //add schedule into conatiner with the right id
                            getGeneratedSchedulersContainer().add("Schedule_" + currentPlan.getUniqueId(),newSchedule);

                            //set the right slotID for scheduling manager, based on high priorirty task.
                            pointManager.doResetStandardSet(BString.make(pointManager.getPlansDaysManagerInputId())); // first reset the previous one
                            pointManager.setPlansDaysManagerInputId(currentPlan.getSlotId());

                            // make link with scheduler to the real point manager
                            if (pointManager.isNumeric())
                                EnelCloudUtils.makeLink("Link_PD_" + currentPlan.getDeviceName() + "_" + currentPlan.getPointName(), newSchedule, pointManager, "out", "inputPlansDaysManagerNumeric");
                            else if (pointManager.isBoolean())
                                EnelCloudUtils.makeLink("Link_PD_" + currentPlan.getDeviceName() + "_" + currentPlan.getPointName(), newSchedule, pointManager, "out", "inputPlansDaysManagerBoolean");
                            else if (pointManager.isString())
                                EnelCloudUtils.makeLink("Link_PD_" + currentPlan.getDeviceName() + "_" + currentPlan.getPointName(), newSchedule, pointManager, "out", "inputPlansDaysManagerString");
                            else if (pointManager.isEnum())
                                EnelCloudUtils.makeLink("Link_PD_" + currentPlan.getDeviceName() + "_" + currentPlan.getPointName(), newSchedule, pointManager, "out", "inputPlansDaysManagerEnum");
                        }
                        else//update the existing one
                        {
                            // delete previous special event in the same day of the newone
                            BDailySchedule[] allSpecialEvents = weeklySchedule.getSpecialEventsChildren();
                            for (int j = 0; j < allSpecialEvents.length; j++)
                            {
                                BDateSchedule dateSch = (BDateSchedule)allSpecialEvents[j].getDays();
                                if (dateSch.getDay() == currentPlan.getDay() && dateSch.getMonth() == currentPlan.getMonth().getOrdinal() && dateSch.getYear() == currentPlan.getYear())
                                    weeklySchedule.getSpecialEvents().remove(allSpecialEvents[j].getName());
                            }

                            //check if there is a manager associated, if not create one.
                            BEnelCloudPointManager pointManager = EnelCloudUtils.makePointManager(findRealPoint(currentPlan.getDeviceName(), currentPlan.getPointName()));


                            BDailySchedule newDailyCustomEventSchedule = new BDailySchedule();
                            // set the day value coming from profile associated to this task
                            BDaySchedule currentDailySchedule = null;
                            if (pointManager.isNumeric())
                                currentDailySchedule = currentPlan.getDayScheduleObject();
                            else if (pointManager.isBoolean())
                                currentDailySchedule = currentPlan.getDayScheduleObjectBoolean();
                            else if (pointManager.isString())
                                currentDailySchedule = currentPlan.getDayScheduleObjectString();
                            else if (pointManager.isEnum())
                                currentDailySchedule = currentPlan.getDayScheduleObjectEnum();
                            newDailyCustomEventSchedule.setDay((BDaySchedule) currentDailySchedule.newCopy());

                            // set the current day
                            BDateSchedule dateSchedule = new BDateSchedule(currentPlan.getDay(), currentPlan.getMonth(), currentPlan.getYear());
                            // add to special event the dateschedule
                            newDailyCustomEventSchedule.setDays(dateSchedule);
                            // add this special event
                            weeklySchedule.addSpecialEvent("DayPlan_" + currentPlan.getUniqueId() + "?", newDailyCustomEventSchedule);
                            //set the right slotID for scheduling manager, based on high priorirty task.
                            //check if there is a manager associated, if not create one.
                            pointManager.doResetStandardSet(BString.make(pointManager.getPlansDaysManagerInputId())); // first reset the previous one
                            pointManager.setPlansDaysManagerInputId(currentPlan.getSlotId());
                            pointManager.updateValues();
                        }

                    }
                    op.getWriter().write("New Day Plan created");
                }
                else
                {
                    op.getResponse().sendError(400, "BAD REQUEST");
                    service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST" );
                }
            } else
            {
                op.getResponse().sendError(403, "FORBIDDEN");
                service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: FORBIDDEN");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            op.getResponse().sendError(400, "BAD REQUEST");
            service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST");
        }
    }

    ////////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

    private void loadService()
    {
        // check if the service is loaded, if not, load it.
        try
        {
            if (service == null)
                service = (BEnelCloudService)getParent();
        }
        catch (Exception e)
        {
            //error - servlet in wrong location.
            e.printStackTrace();
        }
    }

    // check if the token on the request is valid
    private boolean isRequestWithValidToken(WebOp op)
    {
        try
        {
            String token = op.getRequest().getHeader("x-access-token");
            // ask to the service for the validity
            if (service.getAccessToken().equals(token))
                return true;
            else
                return false;
        }
        catch (Exception e)
        {
            return false; // something wrong, security check failed.
        }
    }


    // check if already exist one schedule with this id
    private BWeeklySchedule searchExistingSchedule(String uniqueScheduleId)
    {
        // get all schedules objects
        BWeeklySchedule[] schedules = getGeneratedSchedulersContainer().getChildren(BWeeklySchedule.class);
        // set the found flag to false
        boolean found = false;
        // cycle every schedules instance
        for (int i = 0; i < schedules.length && found == false; i++)
        {
            BWeeklySchedule currentSchedule = schedules[i];

            // if found get it.
            if (currentSchedule.getName().equals(uniqueScheduleId))
            {
                return currentSchedule;
            }
        }

        // if not found return null
        return null;
    }

    ////////////////////////////////////////////////////////////////
// Find the real point inside drivers
////////////////////////////////////////////////////////////////

    private BControlPoint findRealPoint(String deviceCode, String dataPointName)
    {
        // create the bql query for searching the right device
        BOrd query = BOrd.make("slot:/|bql:select slotPathOrd from driver:Device where name = '" + deviceCode + "'");
        // resolve the bql query
        BITable result = (BITable) query.resolve(this).get();
        // get the columns
        ColumnList columns = result.getColumns();
        // select the frist one with the slotPathOrd
        Column valueColumn = columns.get(0);
        // get the cursor
        TableCursor tableCursor = result.cursor();
        // get the first row
        tableCursor.next();
        // get the device from the slotPathOrd getted
        BDevice device = ((BDevice) tableCursor.get());
        // get all extensions and set to null the points one
        BDeviceExt[] exts = device.getDeviceExts();
        BPointDeviceExt pointDeviceExt = null;

        // search the point extension
        for (int i = 0; i < exts.length; i++)
        {
            if (exts[i] instanceof BPointDeviceExt)
                pointDeviceExt = (BPointDeviceExt) exts[i];
        }

        // check if the extension exist
        if (pointDeviceExt != null)
        {
            // get all points inside the extension
            //BControlPoint[] points = pointDeviceExt.getPoints();
            BControlPoint[] points = pointDeviceExt.getChildren(BControlPoint.class);
            // found flag
            boolean found = false;
            // search for the right one
            for (int i = 0; i < points.length && !found; i++)
            {
                BControlPoint currentPoint = points[i];
                // check the name
                if (currentPoint.getName().equals(dataPointName))
                {
                    // change the flag
                    return currentPoint;
                }
            }
            // if not found in all points, get error
            if (!found)
            {
                return null;
            }
        }
        return null;
    }

    ////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("databaseDisconnected.png");
    }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
    private BEnelCloudService service = null;
    public static final Logger logger = Logger.getLogger("EnelCloudPlansDaysManager");
}
