package com.amaca.enelCloudConnector.alarming;

import com.amaca.enelCloudConnector.utils.EnelCloudUtils;
import com.tridium.alarm.BConsoleRecipient;
import com.tridium.email.alarm.BEmailRecipient;

import javax.baja.alarm.BAlarmClass;
import javax.baja.alarm.BAlarmRecord;
import javax.baja.alarm.BAlarmService;
import javax.baja.email.BEmailService;
import javax.baja.email.BOutgoingAccount;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import javax.baja.util.BUuid;

/**
 * Created by Amaca on 16/08/2018.
 */

@NiagaraProperty(
        name = "enabled",
        type = "boolean",
        defaultValue = "false"
)

@NiagaraType
public class BEnelCloudAlarmsManager extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.alarming.BEnelCloudAlarmsManager(921182301)1.0$ @*/
/* Generated Thu Aug 16 14:12:47 CEST 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "enabled"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code enabled} property.
   * @see #getEnabled
   * @see #setEnabled
   */
  public static final Property enabled = newProperty(0, false, null);
  
  /**
   * Get the {@code enabled} property.
   * @see #enabled
   */
  public boolean getEnabled() { return getBoolean(enabled); }
  
  /**
   * Set the {@code enabled} property.
   * @see #enabled
   */
  public void setEnabled(boolean v) { setBoolean(enabled, v, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudAlarmsManager.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Constructor
////////////////////////////////////////////////////////////////

    public BEnelCloudAlarmsManager()
    {
    }

 ////////////////////////////////////////////////////////////////
// Changed
////////////////////////////////////////////////////////////////

    @Override
    public void changed(Property property, Context context)
    {
        super.changed(property, context);

        // check if we are in a running state
        if (!isRunning())return;
        if (!Sys.atSteadyState())return;

        if (property == enabled)
            if (getEnabled())
                setupEnv();
    }

    ////////////////////////////////////////////////////////////////
// Alarm Generators
////////////////////////////////////////////////////////////////

    public void generateAlarm(BComponent source, String message)
    {
        // get the alarm class
        BAlarmService alarmService = (BAlarmService) Sys.getService(BAlarmService.TYPE);
        BAlarmClass cloudLabAlarmClass = (BAlarmClass)alarmService.get("cloudLab");
        //generate alarm data into BFACETS
        BFacets alarmData = BFacets.make("msgText", BString.make(message) ,"sourceName", BString.make(source.getNavName()));
        //generate alarm record
        BAlarmRecord alarmRecord = new BAlarmRecord(source, "cloudLab", alarmData, BUuid.make());
        try
        {
            alarmService.doRouteAlarm(alarmRecord);
        }
        catch (Exception e) {e.printStackTrace();}
    }

    public void generateEmailAlarm(BComponent source, String message)
    {
        // get the alarm class
        BAlarmService alarmService = (BAlarmService) Sys.getService(BAlarmService.TYPE);
        BAlarmClass cloudLabAlarmClass = (BAlarmClass)alarmService.get("cloudLabEmail");
        //generate alarm data into BFACETS
        BFacets alarmData = BFacets.make("msgText", BString.make(message) ,"sourceName", BString.make(source.getNavName()));
        //generate alarm record
        BAlarmRecord alarmRecord = new BAlarmRecord(source, "cloudLabEmail", alarmData, BUuid.make());

        try
        {
            alarmService.doRouteAlarm(alarmRecord);
        }
        catch (Exception e) {e.printStackTrace();}

    }


////////////////////////////////////////////////////////////////
// Environment Setup
////////////////////////////////////////////////////////////////

    private void setupEnv()
    {
        try
        {
            // get the alarm service
            BAlarmService alarmService = (BAlarmService) Sys.getService(BAlarmService.TYPE);
            // create the standard alarm class
            BAlarmClass cloudLabAlarmClass = new BAlarmClass();
            // create the email connected alarm class
            BAlarmClass cloudLabEmailAlarmClass = new BAlarmClass();
            try
            {
                //add these alarm classes into alarm service
                alarmService.add("cloudLab", cloudLabAlarmClass);
                alarmService.add("cloudLabEmail", cloudLabEmailAlarmClass);
            }catch(Exception e){}

            // create a new console recipient and add into alarm service
            BConsoleRecipient consoleRecipient = new BConsoleRecipient();
            try
            {
                alarmService.add("CloudLabConsole", consoleRecipient);
                EnelCloudUtils.makeLinkTopic("Link_CloudLab", cloudLabAlarmClass, consoleRecipient, "alarm", "routeAlarm");
                EnelCloudUtils.makeLinkTopic("Link_CloudLab_2", cloudLabEmailAlarmClass, consoleRecipient, "alarm", "routeAlarm");
            }
            catch (Exception e){e.printStackTrace();};

            // create the e-mail service and outgoing account
            BEmailService emailService = new BEmailService();
            BOutgoingAccount outgoingAccount = new BOutgoingAccount();
            try
            {
                emailService.add("cloudLabAccount", outgoingAccount);
                // add the email service, into services folder
                alarmService.getParent().getParentComponent().add("EmailService", emailService);
            }catch (Exception e){}

            // create a new email recipient and add into alarm service
            BEmailRecipient emailRecipient = new BEmailRecipient();
            emailRecipient.setEmailAccount("cloudLabAccount");
            try
            {
                alarmService.add("CloudLabEmailRecipient", emailRecipient);
                EnelCloudUtils.makeLinkTopic("Link_CloudLabEmail", cloudLabEmailAlarmClass, emailRecipient, "alarm", "routeAlarm");
            }catch (Exception e){}

        }
        catch(Exception e){};
    }

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("alarm.png");
    }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

}
