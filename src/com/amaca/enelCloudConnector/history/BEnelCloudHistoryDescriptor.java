package com.amaca.enelCloudConnector.history;

import com.amaca.enelCloudConnector.BEnelCloudService;
import com.tridium.history.db.BLocalDbHistory;
import com.tridium.json.JSONArray;
import com.tridium.json.JSONObject;

import javax.baja.history.BBooleanTrendRecord;
import javax.baja.history.BHistoryId;
import javax.baja.history.BNumericTrendRecord;
import javax.baja.naming.BOrd;
import javax.baja.naming.SlotPath;
import javax.baja.nre.annotations.NiagaraAction;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import javax.baja.sys.Action;
import javax.baja.units.BUnit;
import javax.baja.util.BTypeSpec;
import java.util.StringTokenizer;

/**
 * Created by Amaca on 11/03/2018.
 */
@NiagaraProperty(
        name = "thingActive",
        type = "boolean",
        defaultValue = "false"
)
@NiagaraProperty(
        name = "fastQueue",
        type = "boolean",
        defaultValue = "false"
)
@NiagaraProperty(
        name = "historyId",
        type = "history:HistoryId",
        flags = Flags.READONLY,
        defaultValue = "BHistoryId.NULL"
)
@NiagaraProperty(
        name = "historyType",
        type = "baja:TypeSpec",
        flags = Flags.READONLY,
        defaultValue = "BTypeSpec.NULL"
)
@NiagaraProperty(
        name = "deviceName",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "pointName",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "deviceType",
        type = "baja:String",
        defaultValue = "fhttps"
)
@NiagaraProperty(
        name = "niagaraId",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "thingId",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "installationID",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "model",
        type = "baja:String",
        defaultValue = "null"
)
@NiagaraProperty(
        name = "maker",
        type = "baja:String",
        defaultValue = "null"
)
@NiagaraProperty(
        name = "alias",
        type = "baja:String",
        defaultValue = "null"
)
@NiagaraProperty(
        name = "geolocalization",
        type = "baja:String",
        defaultValue = "null"
)
@NiagaraProperty(
        name = "trendID",
        type = "baja:String",
        defaultValue = "null"
)
@NiagaraProperty(
        name = "uniqueId",
        type = "baja:String",
        defaultValue = "null"
)
@NiagaraProperty(
        name = "lastSuccessfulSync",
        type = "baja:AbsTime",
        flags = Flags.READONLY,
        defaultValue = "BAbsTime.NULL"
)
@NiagaraProperty(
        name = "lastRecordSync",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "lastRecordSyncTimestamp",
        type = "baja:AbsTime",
        flags = Flags.READONLY,
        defaultValue = "BAbsTime.NULL"
)

@NiagaraAction(
        name="printNewDataForCloud",
        flags = Flags.HIDDEN
)


@NiagaraType
public class BEnelCloudHistoryDescriptor extends BComponent
{





/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.history.BEnelCloudHistoryDescriptor(2373399679)1.0$ @*/
/* Generated Mon Nov 30 21:14:27 CET 2020 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "thingActive"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code thingActive} property.
   * @see #getThingActive
   * @see #setThingActive
   */
  public static final Property thingActive = newProperty(0, false, null);
  
  /**
   * Get the {@code thingActive} property.
   * @see #thingActive
   */
  public boolean getThingActive() { return getBoolean(thingActive); }
  
  /**
   * Set the {@code thingActive} property.
   * @see #thingActive
   */
  public void setThingActive(boolean v) { setBoolean(thingActive, v, null); }

////////////////////////////////////////////////////////////////
// Property "fastQueue"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code fastQueue} property.
   * @see #getFastQueue
   * @see #setFastQueue
   */
  public static final Property fastQueue = newProperty(0, false, null);
  
  /**
   * Get the {@code fastQueue} property.
   * @see #fastQueue
   */
  public boolean getFastQueue() { return getBoolean(fastQueue); }
  
  /**
   * Set the {@code fastQueue} property.
   * @see #fastQueue
   */
  public void setFastQueue(boolean v) { setBoolean(fastQueue, v, null); }

////////////////////////////////////////////////////////////////
// Property "historyId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code historyId} property.
   * @see #getHistoryId
   * @see #setHistoryId
   */
  public static final Property historyId = newProperty(Flags.READONLY, BHistoryId.NULL, null);
  
  /**
   * Get the {@code historyId} property.
   * @see #historyId
   */
  public BHistoryId getHistoryId() { return (BHistoryId)get(historyId); }
  
  /**
   * Set the {@code historyId} property.
   * @see #historyId
   */
  public void setHistoryId(BHistoryId v) { set(historyId, v, null); }

////////////////////////////////////////////////////////////////
// Property "historyType"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code historyType} property.
   * @see #getHistoryType
   * @see #setHistoryType
   */
  public static final Property historyType = newProperty(Flags.READONLY, BTypeSpec.NULL, null);
  
  /**
   * Get the {@code historyType} property.
   * @see #historyType
   */
  public BTypeSpec getHistoryType() { return (BTypeSpec)get(historyType); }
  
  /**
   * Set the {@code historyType} property.
   * @see #historyType
   */
  public void setHistoryType(BTypeSpec v) { set(historyType, v, null); }

////////////////////////////////////////////////////////////////
// Property "deviceName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code deviceName} property.
   * @see #getDeviceName
   * @see #setDeviceName
   */
  public static final Property deviceName = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code deviceName} property.
   * @see #deviceName
   */
  public String getDeviceName() { return getString(deviceName); }
  
  /**
   * Set the {@code deviceName} property.
   * @see #deviceName
   */
  public void setDeviceName(String v) { setString(deviceName, v, null); }

////////////////////////////////////////////////////////////////
// Property "pointName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code pointName} property.
   * @see #getPointName
   * @see #setPointName
   */
  public static final Property pointName = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code pointName} property.
   * @see #pointName
   */
  public String getPointName() { return getString(pointName); }
  
  /**
   * Set the {@code pointName} property.
   * @see #pointName
   */
  public void setPointName(String v) { setString(pointName, v, null); }

////////////////////////////////////////////////////////////////
// Property "deviceType"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code deviceType} property.
   * @see #getDeviceType
   * @see #setDeviceType
   */
  public static final Property deviceType = newProperty(0, "fhttps", null);
  
  /**
   * Get the {@code deviceType} property.
   * @see #deviceType
   */
  public String getDeviceType() { return getString(deviceType); }
  
  /**
   * Set the {@code deviceType} property.
   * @see #deviceType
   */
  public void setDeviceType(String v) { setString(deviceType, v, null); }

////////////////////////////////////////////////////////////////
// Property "niagaraId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code niagaraId} property.
   * @see #getNiagaraId
   * @see #setNiagaraId
   */
  public static final Property niagaraId = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code niagaraId} property.
   * @see #niagaraId
   */
  public String getNiagaraId() { return getString(niagaraId); }
  
  /**
   * Set the {@code niagaraId} property.
   * @see #niagaraId
   */
  public void setNiagaraId(String v) { setString(niagaraId, v, null); }

////////////////////////////////////////////////////////////////
// Property "thingId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code thingId} property.
   * @see #getThingId
   * @see #setThingId
   */
  public static final Property thingId = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code thingId} property.
   * @see #thingId
   */
  public String getThingId() { return getString(thingId); }
  
  /**
   * Set the {@code thingId} property.
   * @see #thingId
   */
  public void setThingId(String v) { setString(thingId, v, null); }

////////////////////////////////////////////////////////////////
// Property "installationID"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code installationID} property.
   * @see #getInstallationID
   * @see #setInstallationID
   */
  public static final Property installationID = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code installationID} property.
   * @see #installationID
   */
  public String getInstallationID() { return getString(installationID); }
  
  /**
   * Set the {@code installationID} property.
   * @see #installationID
   */
  public void setInstallationID(String v) { setString(installationID, v, null); }

////////////////////////////////////////////////////////////////
// Property "model"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code model} property.
   * @see #getModel
   * @see #setModel
   */
  public static final Property model = newProperty(0, "null", null);
  
  /**
   * Get the {@code model} property.
   * @see #model
   */
  public String getModel() { return getString(model); }
  
  /**
   * Set the {@code model} property.
   * @see #model
   */
  public void setModel(String v) { setString(model, v, null); }

////////////////////////////////////////////////////////////////
// Property "maker"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code maker} property.
   * @see #getMaker
   * @see #setMaker
   */
  public static final Property maker = newProperty(0, "null", null);
  
  /**
   * Get the {@code maker} property.
   * @see #maker
   */
  public String getMaker() { return getString(maker); }
  
  /**
   * Set the {@code maker} property.
   * @see #maker
   */
  public void setMaker(String v) { setString(maker, v, null); }

////////////////////////////////////////////////////////////////
// Property "alias"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code alias} property.
   * @see #getAlias
   * @see #setAlias
   */
  public static final Property alias = newProperty(0, "null", null);
  
  /**
   * Get the {@code alias} property.
   * @see #alias
   */
  public String getAlias() { return getString(alias); }
  
  /**
   * Set the {@code alias} property.
   * @see #alias
   */
  public void setAlias(String v) { setString(alias, v, null); }

////////////////////////////////////////////////////////////////
// Property "geolocalization"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code geolocalization} property.
   * @see #getGeolocalization
   * @see #setGeolocalization
   */
  public static final Property geolocalization = newProperty(0, "null", null);
  
  /**
   * Get the {@code geolocalization} property.
   * @see #geolocalization
   */
  public String getGeolocalization() { return getString(geolocalization); }
  
  /**
   * Set the {@code geolocalization} property.
   * @see #geolocalization
   */
  public void setGeolocalization(String v) { setString(geolocalization, v, null); }

////////////////////////////////////////////////////////////////
// Property "trendID"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code trendID} property.
   * @see #getTrendID
   * @see #setTrendID
   */
  public static final Property trendID = newProperty(0, "null", null);
  
  /**
   * Get the {@code trendID} property.
   * @see #trendID
   */
  public String getTrendID() { return getString(trendID); }
  
  /**
   * Set the {@code trendID} property.
   * @see #trendID
   */
  public void setTrendID(String v) { setString(trendID, v, null); }

////////////////////////////////////////////////////////////////
// Property "uniqueId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code uniqueId} property.
   * @see #getUniqueId
   * @see #setUniqueId
   */
  public static final Property uniqueId = newProperty(0, "null", null);
  
  /**
   * Get the {@code uniqueId} property.
   * @see #uniqueId
   */
  public String getUniqueId() { return getString(uniqueId); }
  
  /**
   * Set the {@code uniqueId} property.
   * @see #uniqueId
   */
  public void setUniqueId(String v) { setString(uniqueId, v, null); }

////////////////////////////////////////////////////////////////
// Property "lastSuccessfulSync"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code lastSuccessfulSync} property.
   * @see #getLastSuccessfulSync
   * @see #setLastSuccessfulSync
   */
  public static final Property lastSuccessfulSync = newProperty(Flags.READONLY, BAbsTime.NULL, null);
  
  /**
   * Get the {@code lastSuccessfulSync} property.
   * @see #lastSuccessfulSync
   */
  public BAbsTime getLastSuccessfulSync() { return (BAbsTime)get(lastSuccessfulSync); }
  
  /**
   * Set the {@code lastSuccessfulSync} property.
   * @see #lastSuccessfulSync
   */
  public void setLastSuccessfulSync(BAbsTime v) { set(lastSuccessfulSync, v, null); }

////////////////////////////////////////////////////////////////
// Property "lastRecordSync"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code lastRecordSync} property.
   * @see #getLastRecordSync
   * @see #setLastRecordSync
   */
  public static final Property lastRecordSync = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code lastRecordSync} property.
   * @see #lastRecordSync
   */
  public String getLastRecordSync() { return getString(lastRecordSync); }
  
  /**
   * Set the {@code lastRecordSync} property.
   * @see #lastRecordSync
   */
  public void setLastRecordSync(String v) { setString(lastRecordSync, v, null); }

////////////////////////////////////////////////////////////////
// Property "lastRecordSyncTimestamp"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code lastRecordSyncTimestamp} property.
   * @see #getLastRecordSyncTimestamp
   * @see #setLastRecordSyncTimestamp
   */
  public static final Property lastRecordSyncTimestamp = newProperty(Flags.READONLY, BAbsTime.NULL, null);
  
  /**
   * Get the {@code lastRecordSyncTimestamp} property.
   * @see #lastRecordSyncTimestamp
   */
  public BAbsTime getLastRecordSyncTimestamp() { return (BAbsTime)get(lastRecordSyncTimestamp); }
  
  /**
   * Set the {@code lastRecordSyncTimestamp} property.
   * @see #lastRecordSyncTimestamp
   */
  public void setLastRecordSyncTimestamp(BAbsTime v) { set(lastRecordSyncTimestamp, v, null); }

////////////////////////////////////////////////////////////////
// Action "printNewDataForCloud"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code printNewDataForCloud} action.
   * @see #printNewDataForCloud()
   */
  public static final Action printNewDataForCloud = newAction(Flags.HIDDEN, null);
  
  /**
   * Invoke the {@code printNewDataForCloud} action.
   * @see #printNewDataForCloud
   */
  public void printNewDataForCloud() { invoke(printNewDataForCloud, null, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudHistoryDescriptor.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

  // empty constructor
  public BEnelCloudHistoryDescriptor()
  {
    super();
  }

////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////

  public void doPrintNewDataForCloud()
  {
    System.out.println("DEBUG: " + getNewDataForCloud());
  }

////////////////////////////////////////////////////////////////
// Data
////////////////////////////////////////////////////////////////

  public void generateNiagaraId()
  {
    setNiagaraId(getDeviceName() + "_" + getPointName());
  }

  public JSONObject getNewDataForCloudInJSON()
  {
    BEnelCloudService srv = (BEnelCloudService)getParent().getParent().getParent().getParentComponent();
    setNiagaraId(getDeviceName() + "_" + getPointName());

    // create a result array container
    JSONArray resultArray = new JSONArray();

    // resolve history
    BLocalDbHistory history = (BLocalDbHistory) BOrd.make("history:" + getHistoryId().toString()).get();
    // get a cursor
    Cursor cursor = history.cursor();
    // record limit (max 100 entity per request)
    int limit = 99;
    // get history facets
    BFacets facets = (BFacets) history.getConfig().get("valueFacets").newCopy();
    // get Measurement Unit
    String unitSymbol = ""; // as default empty
    try
    {
      BUnit unit = (BUnit) facets.get("units");
      unitSymbol = unit.getSymbol();
    }
    catch (Exception e){};

    JSONObject mainObj = new JSONObject();
    mainObj.put("radio_type", "Eth");
    mainObj.put("is_online", "true");
    mainObj.put("timestamp", getCurrentTimeStampFormatted(BAbsTime.now()));
    mainObj.put("device_type", getDeviceType()); //parametro?
    mainObj.put("thing_id", getThingId());
    mainObj.put("environment_prefix", srv.getEnvPrefix()); // da cambiare

    // read all record from start date
    while(cursor.next() && limit > 0)
    {
      // check the type of record: numeric or boolean
      if (getHistoryType().equals(BTypeSpec.make(BNumericTrendRecord.TYPE)))
      {
        // get the record
        BNumericTrendRecord record = (BNumericTrendRecord) cursor.get();
        // check if the record is new (not already sent)
        if (isNew(record.getTimestamp()))
        {
          // create a jsonobject with the right structure with data of the current record
          JSONObject jsonRecord = new JSONObject();
          jsonRecord.put("timestamp", getTimeStampFormatted(record.getTimestamp()));
          jsonRecord.put("thingId", getThingId());
          jsonRecord.put("name", getPointName());
          jsonRecord.put("nativeTimestamp", getNativeTimeStampFormatted(record.getTimestamp()));
          jsonRecord.put("trendId", getTrendID());
          jsonRecord.put("value", "" + record.getNumeric());
          jsonRecord.put("quality", "" + "true");
          // put the record inside the result array
          resultArray.put(jsonRecord);
          // update the last records attributes
          lastRecord = jsonRecord.toString();
          lastRecordTimestamp = record.getTimestamp();
          // decrease the limit
          limit--;
        }
      }
      else if (getHistoryType().equals(BTypeSpec.make(BBooleanTrendRecord.TYPE)))
      {
        // get the record
        BBooleanTrendRecord record = (BBooleanTrendRecord) cursor.get();
        // check if the record is new (not already sent)
        if (isNew(record.getTimestamp()))
        {
          // create a jsonobject with the right structure with data of the current record
          JSONObject jsonRecord = new JSONObject();
          jsonRecord.put("timestamp", getTimeStampFormatted(record.getTimestamp()));
          jsonRecord.put("thingId", getThingId());
          jsonRecord.put("name", getPointName());
          jsonRecord.put("nativeTimestamp", getNativeTimeStampFormatted(record.getTimestamp()));
          jsonRecord.put("trendId", getTrendID());
          if (record.getBoolean())
            jsonRecord.put("value", "1");
          else
            jsonRecord.put("value", "0");
          jsonRecord.put("quality", "" + "true");
          // put the record inside the result array
          resultArray.put(jsonRecord);
          // update the last records attributes
          lastRecord = jsonRecord.toString();
          lastRecordTimestamp = record.getTimestamp();
          // decrease the limit
          limit--;
        }
      }
    }

    JSONObject data = new JSONObject();
    data.put("tags", resultArray);
    mainObj.put("data", data);

    return mainObj;
  }

  // return the string of the JSON array created (used in standard send mode)
  public String getNewDataForCloud()
  {
    if (getThingActive())
      return getNewDataForCloudInJSON().toString();
    else
      return "";
  }

  // update the "last data" slots
  public void confirmLastRecordAndLastSync()
  {
    setLastSuccessfulSync(BAbsTime.now());
    setLastRecordSync(lastRecord);
    setLastRecordSyncTimestamp(lastRecordTimestamp);
  }

  public String getUnitSymbol()
  {
    BEnelCloudService srv = (BEnelCloudService)getParent().getParent().getParent().getParentComponent();
    setNiagaraId(getDeviceName() + "_" + getPointName());

    // create a result array container
    JSONArray resultArray = new JSONArray();

    // resolve history
    BLocalDbHistory history = (BLocalDbHistory) BOrd.make("history:" + getHistoryId().toString()).get();
    // get a cursor
    Cursor cursor = history.cursor();
    // record limit (max 100 entity per request)
    int limit = 99;
    // get history facets
    BFacets facets = (BFacets) history.getConfig().get("valueFacets").newCopy();
    // get Measurement Unit
    try
    {
      BUnit unit = (BUnit) facets.get("units");
      unitSymbol = unit.getSymbol();
    }
    catch (Exception e){};

    return unitSymbol;
  }

  public boolean isWritable()
  {
    return true;
  }

  public boolean isReadable()
  {
    return true;
  }

////////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

  // get the device name based on ID structure: <deviceName>/<pointName>
  public void calculateDeviceAndPointName()
  {
    // get the historyId and create two token
    StringTokenizer stringTokenizer = new StringTokenizer(SlotPath.unescape(getHistoryId().encodeToString()), "/");
    // first part is the station name, ignore it.
    stringTokenizer.nextToken();
    // second token is the device name.
    setDeviceName(stringTokenizer.nextToken());
    // last token is the point name
    setPointName(stringTokenizer.nextToken());
  }

  // check if the parameter timestamp is after the last successful send timestamp.
  private boolean isNew(BAbsTime time)
  {
    // if null the last success, all is new
    if (getLastRecordSyncTimestamp().isNull())
      return true;

    // check
    if( time.isAfter(getLastRecordSyncTimestamp()))
      return true;
    else
      return false;
  }

  // convert the niagara timestamp to requested format: "2018-03-14T11:46:07.011Z"
  private String getTimeStampFormatted(BAbsTime timestamp)
  {
    //use utc
    timestamp = timestamp.toUtcTime();
    String result = "";

    result += timestamp.getMillis();

    return result;
  }
  private String getNativeTimeStampFormatted(BAbsTime timestamp)
  {
    //use utc
    timestamp = timestamp.toUtcTime();

    String result = "";
    result += timestamp.getYear() + "-" + mustBeDoubleDigit("" + (timestamp.getMonth().getOrdinal()+1)) + "-" + mustBeDoubleDigit("" + timestamp.getDay());
    result +=  "T" + mustBeDoubleDigit("" + timestamp.getHour()) + ":" + mustBeDoubleDigit("" + timestamp.getMinute()) + ":" + mustBeDoubleDigit("" + timestamp.getSecond()) + ".011Z";

    return result;
  }

  private String getCurrentTimeStampFormatted(BAbsTime timestamp)
  {
    //use utc
    timestamp = timestamp.toUtcTime();
    String result = "";

    result += timestamp.getYear() + "" + mustBeDoubleDigit("" + (timestamp.getMonth().getOrdinal()+1)) + "" + mustBeDoubleDigit("" + timestamp.getDay());
    result +=  "" + mustBeDoubleDigit("" + timestamp.getHour()) + "" + mustBeDoubleDigit("" + timestamp.getMinute()) + "" + mustBeDoubleDigit("" + timestamp.getSecond()) + "" + timestamp.getMillisecond();

    return result;
  }

  // all numbers must have two digits at minimum
  private String mustBeDoubleDigit(String in)
  {
    if (in.length() == 1)
      return "0" + in;
    return in;
  }

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

  @Override
  public BIcon getIcon()
  {
    return BIcon.std("history.png");
  }
  private String lastRecord = null;
  private String unitSymbol = "";
  private BAbsTime lastRecordTimestamp = null;
}
