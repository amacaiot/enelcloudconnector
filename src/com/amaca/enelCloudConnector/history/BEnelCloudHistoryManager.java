package com.amaca.enelCloudConnector.history;

import com.amaca.enelCloudConnector.BEnelCloudService;
import com.amaca.enelCloudConnector.history.job.BEnelCloudHistoriesSyncJob;
import com.amaca.enelCloudConnector.history.job.BEnelCloudHistoriesSyncJobForRTM;
import com.amaca.enelCloudConnector.history.job.BEnelCloudReducedTrafficModeHistoriesSyncJob;
import com.amaca.enelCloudConnector.utils.EnelCloudUtils;
import com.amaca.enelCloudConnector.network.BAwsGatewayInterface;
import com.tridium.history.db.BLocalDbHistory;
import com.tridium.history.log.BLogRecord;
import com.tridium.json.JSONArray;
import com.tridium.json.JSONObject;
import com.tridium.json.JSONUtil;
import com.tridium.platform.BFilesystemAttributes;
import com.tridium.platform.BPlatformServiceContainer;
import com.tridium.platform.BSystemPlatformService;

import javax.baja.collection.TableCursor;
import javax.baja.control.BNumericWritable;
import javax.baja.history.*;
import javax.baja.history.db.BHistoryDatabase;
import javax.baja.naming.BOrd;
import javax.baja.naming.SlotPath;
import javax.baja.nre.annotations.NiagaraAction;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.schedule.BAbstractSchedule;
import javax.baja.schedule.BNumericSchedule;
import javax.baja.schedule.BWeeklySchedule;
import javax.baja.sys.*;
import javax.baja.util.BTypeSpec;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 * Created by Amaca on 25/02/2018.
 */

@NiagaraProperty(
        name = "syncTimeout",
        type = "baja:RelTime",
        defaultValue = "BRelTime.makeMinutes(5)"
)
@NiagaraProperty(
        name = "syncTimeoutFastQueue",
        type = "baja:RelTime",
        defaultValue = "BRelTime.makeSeconds(40)"
)
@NiagaraProperty(
        name = "reducedTrafficMode",
        type = "boolean",
        flags = Flags.HIDDEN,
        defaultValue = "true"
)
@NiagaraProperty(
        name = "numberOfRecordsPerRequest",
        type = "int",
        flags = Flags.HIDDEN,
        defaultValue = "100"
)
@NiagaraProperty(
        name = "awsInterface",
        type = "enelCloudConnector:AwsGatewayInterface",
        flags = Flags.HIDDEN,
        defaultValue = "new BAwsGatewayInterface()"
)
@NiagaraProperty(
        name = "awsThingInterface",
        type = "enelCloudConnector:AwsGatewayInterface",
        flags = Flags.HIDDEN,
        defaultValue = "new BAwsGatewayInterface()"
)
@NiagaraProperty(
        name = "awsThingCommandsInterface",
        type = "enelCloudConnector:AwsGatewayInterface",
        flags = Flags.HIDDEN,
        defaultValue = "new BAwsGatewayInterface()"
)
@NiagaraProperty(
        name = "historyList",
        type = "enelCloudConnector:EnelCloudHistoryList",
        defaultValue = "new BEnelCloudHistoryList()"
)
@NiagaraAction(
        name = "refreshHistoriesList"
)
@NiagaraAction(
        name = "removeAllHistoriesDescriptors"
)
@NiagaraAction(
        name="startHistoriesSyncJob",
        flags = Flags.HIDDEN
)
@NiagaraAction(
        name="startHistoriesSyncJobFastQueue",
        flags = Flags.HIDDEN
)
@NiagaraAction(
        name = "newMessageFromCloud",
        parameterType = "baja:String",
        defaultValue = "BString.DEFAULT",
        flags = Flags.HIDDEN
)
@NiagaraAction(
        name = "newMessageFromThingCloud",
        parameterType = "baja:String",
        defaultValue = "BString.DEFAULT",
        flags = Flags.HIDDEN
)
@NiagaraType
public class BEnelCloudHistoryManager extends BAbstractService
{











/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.history.BEnelCloudHistoryManager(1713851846)1.0$ @*/
/* Generated Mon Nov 30 21:20:59 CET 2020 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "syncTimeout"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code syncTimeout} property.
   * @see #getSyncTimeout
   * @see #setSyncTimeout
   */
  public static final Property syncTimeout = newProperty(0, BRelTime.makeMinutes(5), null);
  
  /**
   * Get the {@code syncTimeout} property.
   * @see #syncTimeout
   */
  public BRelTime getSyncTimeout() { return (BRelTime)get(syncTimeout); }
  
  /**
   * Set the {@code syncTimeout} property.
   * @see #syncTimeout
   */
  public void setSyncTimeout(BRelTime v) { set(syncTimeout, v, null); }

////////////////////////////////////////////////////////////////
// Property "syncTimeoutFastQueue"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code syncTimeoutFastQueue} property.
   * @see #getSyncTimeoutFastQueue
   * @see #setSyncTimeoutFastQueue
   */
  public static final Property syncTimeoutFastQueue = newProperty(0, BRelTime.makeSeconds(40), null);
  
  /**
   * Get the {@code syncTimeoutFastQueue} property.
   * @see #syncTimeoutFastQueue
   */
  public BRelTime getSyncTimeoutFastQueue() { return (BRelTime)get(syncTimeoutFastQueue); }
  
  /**
   * Set the {@code syncTimeoutFastQueue} property.
   * @see #syncTimeoutFastQueue
   */
  public void setSyncTimeoutFastQueue(BRelTime v) { set(syncTimeoutFastQueue, v, null); }

////////////////////////////////////////////////////////////////
// Property "reducedTrafficMode"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code reducedTrafficMode} property.
   * @see #getReducedTrafficMode
   * @see #setReducedTrafficMode
   */
  public static final Property reducedTrafficMode = newProperty(Flags.HIDDEN, true, null);
  
  /**
   * Get the {@code reducedTrafficMode} property.
   * @see #reducedTrafficMode
   */
  public boolean getReducedTrafficMode() { return getBoolean(reducedTrafficMode); }
  
  /**
   * Set the {@code reducedTrafficMode} property.
   * @see #reducedTrafficMode
   */
  public void setReducedTrafficMode(boolean v) { setBoolean(reducedTrafficMode, v, null); }

////////////////////////////////////////////////////////////////
// Property "numberOfRecordsPerRequest"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code numberOfRecordsPerRequest} property.
   * @see #getNumberOfRecordsPerRequest
   * @see #setNumberOfRecordsPerRequest
   */
  public static final Property numberOfRecordsPerRequest = newProperty(Flags.HIDDEN, 100, null);
  
  /**
   * Get the {@code numberOfRecordsPerRequest} property.
   * @see #numberOfRecordsPerRequest
   */
  public int getNumberOfRecordsPerRequest() { return getInt(numberOfRecordsPerRequest); }
  
  /**
   * Set the {@code numberOfRecordsPerRequest} property.
   * @see #numberOfRecordsPerRequest
   */
  public void setNumberOfRecordsPerRequest(int v) { setInt(numberOfRecordsPerRequest, v, null); }

////////////////////////////////////////////////////////////////
// Property "awsInterface"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code awsInterface} property.
   * @see #getAwsInterface
   * @see #setAwsInterface
   */
  public static final Property awsInterface = newProperty(Flags.HIDDEN, new BAwsGatewayInterface(), null);
  
  /**
   * Get the {@code awsInterface} property.
   * @see #awsInterface
   */
  public BAwsGatewayInterface getAwsInterface() { return (BAwsGatewayInterface)get(awsInterface); }
  
  /**
   * Set the {@code awsInterface} property.
   * @see #awsInterface
   */
  public void setAwsInterface(BAwsGatewayInterface v) { set(awsInterface, v, null); }

////////////////////////////////////////////////////////////////
// Property "awsThingInterface"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code awsThingInterface} property.
   * @see #getAwsThingInterface
   * @see #setAwsThingInterface
   */
  public static final Property awsThingInterface = newProperty(Flags.HIDDEN, new BAwsGatewayInterface(), null);
  
  /**
   * Get the {@code awsThingInterface} property.
   * @see #awsThingInterface
   */
  public BAwsGatewayInterface getAwsThingInterface() { return (BAwsGatewayInterface)get(awsThingInterface); }
  
  /**
   * Set the {@code awsThingInterface} property.
   * @see #awsThingInterface
   */
  public void setAwsThingInterface(BAwsGatewayInterface v) { set(awsThingInterface, v, null); }

////////////////////////////////////////////////////////////////
// Property "awsThingCommandsInterface"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code awsThingCommandsInterface} property.
   * @see #getAwsThingCommandsInterface
   * @see #setAwsThingCommandsInterface
   */
  public static final Property awsThingCommandsInterface = newProperty(Flags.HIDDEN, new BAwsGatewayInterface(), null);
  
  /**
   * Get the {@code awsThingCommandsInterface} property.
   * @see #awsThingCommandsInterface
   */
  public BAwsGatewayInterface getAwsThingCommandsInterface() { return (BAwsGatewayInterface)get(awsThingCommandsInterface); }
  
  /**
   * Set the {@code awsThingCommandsInterface} property.
   * @see #awsThingCommandsInterface
   */
  public void setAwsThingCommandsInterface(BAwsGatewayInterface v) { set(awsThingCommandsInterface, v, null); }

////////////////////////////////////////////////////////////////
// Property "historyList"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code historyList} property.
   * @see #getHistoryList
   * @see #setHistoryList
   */
  public static final Property historyList = newProperty(0, new BEnelCloudHistoryList(), null);
  
  /**
   * Get the {@code historyList} property.
   * @see #historyList
   */
  public BEnelCloudHistoryList getHistoryList() { return (BEnelCloudHistoryList)get(historyList); }
  
  /**
   * Set the {@code historyList} property.
   * @see #historyList
   */
  public void setHistoryList(BEnelCloudHistoryList v) { set(historyList, v, null); }

////////////////////////////////////////////////////////////////
// Action "refreshHistoriesList"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code refreshHistoriesList} action.
   * @see #refreshHistoriesList()
   */
  public static final Action refreshHistoriesList = newAction(0, null);
  
  /**
   * Invoke the {@code refreshHistoriesList} action.
   * @see #refreshHistoriesList
   */
  public void refreshHistoriesList() { invoke(refreshHistoriesList, null, null); }

////////////////////////////////////////////////////////////////
// Action "removeAllHistoriesDescriptors"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code removeAllHistoriesDescriptors} action.
   * @see #removeAllHistoriesDescriptors()
   */
  public static final Action removeAllHistoriesDescriptors = newAction(0, null);
  
  /**
   * Invoke the {@code removeAllHistoriesDescriptors} action.
   * @see #removeAllHistoriesDescriptors
   */
  public void removeAllHistoriesDescriptors() { invoke(removeAllHistoriesDescriptors, null, null); }

////////////////////////////////////////////////////////////////
// Action "startHistoriesSyncJob"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code startHistoriesSyncJob} action.
   * @see #startHistoriesSyncJob()
   */
  public static final Action startHistoriesSyncJob = newAction(Flags.HIDDEN, null);
  
  /**
   * Invoke the {@code startHistoriesSyncJob} action.
   * @see #startHistoriesSyncJob
   */
  public void startHistoriesSyncJob() { invoke(startHistoriesSyncJob, null, null); }

////////////////////////////////////////////////////////////////
// Action "startHistoriesSyncJobFastQueue"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code startHistoriesSyncJobFastQueue} action.
   * @see #startHistoriesSyncJobFastQueue()
   */
  public static final Action startHistoriesSyncJobFastQueue = newAction(Flags.HIDDEN, null);
  
  /**
   * Invoke the {@code startHistoriesSyncJobFastQueue} action.
   * @see #startHistoriesSyncJobFastQueue
   */
  public void startHistoriesSyncJobFastQueue() { invoke(startHistoriesSyncJobFastQueue, null, null); }

////////////////////////////////////////////////////////////////
// Action "newMessageFromCloud"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code newMessageFromCloud} action.
   * @see #newMessageFromCloud(BString parameter)
   */
  public static final Action newMessageFromCloud = newAction(Flags.HIDDEN, BString.DEFAULT, null);
  
  /**
   * Invoke the {@code newMessageFromCloud} action.
   * @see #newMessageFromCloud
   */
  public void newMessageFromCloud(BString parameter) { invoke(newMessageFromCloud, parameter, null); }

////////////////////////////////////////////////////////////////
// Action "newMessageFromThingCloud"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code newMessageFromThingCloud} action.
   * @see #newMessageFromThingCloud(BString parameter)
   */
  public static final Action newMessageFromThingCloud = newAction(Flags.HIDDEN, BString.DEFAULT, null);
  
  /**
   * Invoke the {@code newMessageFromThingCloud} action.
   * @see #newMessageFromThingCloud
   */
  public void newMessageFromThingCloud(BString parameter) { invoke(newMessageFromThingCloud, parameter, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudHistoryManager.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

  // empty constructor
  public BEnelCloudHistoryManager()
  {
    super();
  }

////////////////////////////////////////////////////////////////
// Access
////////////////////////////////////////////////////////////////

  public BEnelCloudService getEnelCloudService()
  {
    return enelCloudService;
  }

  public void setEnelCloudService(BEnelCloudService service)
  {
    enelCloudService = service;
  }

////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////

  // elaborate messages coming from the cloud for thing registration/activation
  public void doNewMessageFromCloud(BString payload)
  {
    try {
      System.out.println("PAYLOAD RECEIVED: " + payload.getString());

      //JSONObject payloadRec = new JSONObject(SlotPath.escape(payload.getString()));
      JSONObject payloadRec = new JSONObject(payload.getString());
      //JSONObject payloadRec = new JSONObject("{\"timestamp\":1586358603,\"execution\":{\"jobId\":\"esol_ap07615_dev-2004081510-16686d1f-15ce-4693-9ea7-0f9d2b6fc67a\",\"status\":\"QUEUED\",\"queuedAt\":1586358603,\"lastUpdatedAt\":1586358603,\"versionNumber\":1,\"executionNumber\":1,\"jobDocument\":{\"version\":\"1.0\",\"command\":\"activate-thing\",\"parameters\":{\"thing_id\":\"HUMPIOX02_thing1\",\"interaction_mode\":\"gateway\",\"radio_type\":\"Serial\",\"device_type\":\"modbus_rtu\",\"serial_number\":\"1\",\"model\":\"MX9\",\"portName\":\"/dev/ttyXRUSB0\",\"baudRate\":9600,\"dataBits\":8,\"parity\":0,\"stopBits\":0,\"maker\":\"EnergyTeam\",\"filter_tag\":[],\"environment_prefix\":\"esol_ap07615_dev\"}}}}");
      //JSONObject jobs = payloadRec.getJSONObject("jobs");
      //JSONArray quequed = jobs.getJSONArray("QUEUED");
      JSONObject execution = payloadRec.getJSONObject("execution");
      JSONObject jobDocument = execution.getJSONObject("jobDocument");
      JSONObject parametersInput = jobDocument.getJSONObject("parameters");

      String command = jobDocument.getString("command"); // "activate-thing"
      System.out.println("COMMAND RECEIVED: " + command);
      String jobId = execution.getString("jobId");

      if (command.equals("activate-thing"))
      {

        //cycle of jobs
        String thingId = parametersInput.getString("thing_id");
        String deviceType = parametersInput.getString("device_type");
        String model = parametersInput.getString("model");
        String maker = parametersInput.getString("maker");
        String serialNumber = parametersInput.getString("serial_number");
        JSONObject mapping = parametersInput.getJSONObject("mapping");

        Iterator keys = mapping.sortedKeys();

        String niagaraId = keys.next().toString();
        System.out.println("##NIAGARA_ID: " + niagaraId);
        String uniqueId = mapping.getString(niagaraId);

        StringTokenizer tokenizer = new StringTokenizer(uniqueId, "_");
        String currentTrendId = "";
         while (tokenizer.hasMoreTokens())
         {
             currentTrendId = tokenizer.nextToken();
         }


        //find the real thing and activate it
        BEnelCloudHistoryDescriptor realDescriptor = getHistoryList().getDescriptorByNiagaraId(niagaraId);
        if (realDescriptor == null)
        {
          System.out.println("NIAGARA ID NOT FOUND: " + niagaraId + " THING ID: " + thingId);

          //build activation payload
          JSONObject activationPayload = new JSONObject();
          activationPayload.put("status", "FAILED");
          JSONObject statusDetails = new JSONObject();
          statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
          statusDetails.put("step", "thing not found");
          statusDetails.put("thing_id", enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId());
          statusDetails.put("command_target", "gateway");
          activationPayload.put("statusDetails", statusDetails);

          //build the topic with the jobid and send the payload
          getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
          getAwsThingInterface().setPayload(activationPayload.toString());
          BBoolean activationResult = getAwsThingInterface().publish();
          System.out.println("ACTIVATION FAILED SENT, PAYLOAD: " + activationPayload.toString());

        } else {
          System.out.println("NIAGARA ID FOUND: " + niagaraId + " THING ID: " + thingId);
          // register the thing
          JSONObject registrationPayload = new JSONObject();
          registrationPayload.put("version", "1.0");
          registrationPayload.put("command", "register-thing");
          registrationPayload.put("gateway_id", enelCloudService.getGatewayId());

          JSONObject parameters = new JSONObject();
          parameters.put("thing_id", thingId);
          parameters.put("device_type", deviceType);
          parameters.put("radio_type", "eth");
          parameters.put("authentication_mode", "gateway");
          parameters.put("model", model);
          parameters.put("maker", maker);
          parameters.put("serial_number", serialNumber);
          parameters.put("environment_prefix", enelCloudService.getEnvPrefix());
          parameters.put("interaction_mode", "gateway");
          JSONArray comm = new JSONArray();
          comm.put("topic");
          parameters.put("outbound_communication_modes", comm);
          parameters.put("inbound_communication_mode", "topic");

          registrationPayload.put("parameters", parameters);
          //send the registration
          getAwsThingInterface().setRegistrationPayload(registrationPayload.toString());
          BBoolean registrationResult = BBoolean.DEFAULT;
          registrationResult = getAwsThingInterface().registration();
          Thread.sleep(500);
          System.out.println("REGISTRATION SENT, PAYLOAD: " + registrationPayload.toString());
          Thread.sleep(500);
          //System.out.println("REGISTRATION SENT: " + registrationPayload.toString() + " - " + registrationResult.toString());
          // send the activation
          //build activation payload
          JSONObject activationPayload = new JSONObject();
          activationPayload.put("status", "SUCCEEDED");
          JSONObject statusDetails = new JSONObject();
          statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
          statusDetails.put("step", "thing activated successfully");
          statusDetails.put("thing_id", thingId);
          statusDetails.put("command_target", "gateway");
          activationPayload.put("statusDetails", statusDetails);

          //build the topic with the jobid and send the payload
          getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
          getAwsThingInterface().setPayload(activationPayload.toString());
          BBoolean activationResult = getAwsThingInterface().publish();
          System.out.println("ACTIVATION SENT, PAYLOAD: " + activationPayload.toString());
          //System.out.println("ACTIVATION SENT: " + activationPayload.toString() + " - " + activationResult.toString());

          //set uniqueID
          realDescriptor.setUniqueId(uniqueId);
          realDescriptor.setTrendID(currentTrendId);
          realDescriptor.setThingId(thingId);
          //activate the real thing
          realDescriptor.setThingActive(true);
        }
      }
      else if (command.equals("reboot"))
      {
        //build reply payload
        JSONObject replyPayload = new JSONObject();
        replyPayload.put("status", "SUCCEEDED");
        JSONObject statusDetails = new JSONObject();
        statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
        statusDetails.put("step", "Station rebooting in progress");
        statusDetails.put("command_target", "gateway");
        replyPayload.put("statusDetails", statusDetails);

        //build the topic with the jobid and send the payload
        getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
        System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
        //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
        getAwsThingInterface().setPayload(replyPayload.toString());
        BBoolean activationResult = getAwsThingInterface().publish();
        System.out.println("JOB OK SENT: " + replyPayload.toString());
        Thread.sleep(500);
        //reboot jace
        BPlatformServiceContainer plat = (BPlatformServiceContainer)Sys.getService(BPlatformServiceContainer.TYPE);
        plat.doRestartStation();
      }
      else if (command.equals("deactivate-gateway"))
      {
        //build reply payload
        JSONObject replyPayload = new JSONObject();
        replyPayload.put("status", "SUCCEEDED");
        JSONObject statusDetails = new JSONObject();
        statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
        statusDetails.put("step", "Gateway deactivated");
        statusDetails.put("command_target", "gateway");
        replyPayload.put("statusDetails", statusDetails);

        //build the topic with the jobid and send the payload
        getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
        System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
        //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
        getAwsThingInterface().setPayload(replyPayload.toString());
        BBoolean activationResult = getAwsThingInterface().publish();
        System.out.println("JOB OK SENT: " + replyPayload.toString());

        //deactivate/unregister gateway
        setEnabled(false);
        getEnelCloudService().setGatewayRegistrerd(false);
      }
      else if (command.equals("monitor-gw"))
      {

        int newFrequency = parametersInput.getInt("frequency");

        //build the payload with the diagnostic data
        JSONObject monitorPayload = new JSONObject();
        monitorPayload.put("version", "1.0");
        monitorPayload.put("gateway_id", enelCloudService.getGatewayId());
        monitorPayload.put("environmentPrefix", enelCloudService.getEnvPrefix());

        JSONObject paramPayload = new JSONObject();
        paramPayload.put("primary_iface","ETH");
        paramPayload.put("connected_thing","" + getHistoryList().getAllDescriptors().length);

        JSONObject sysInfoPayload = new JSONObject();

        BPlatformServiceContainer plat = (BPlatformServiceContainer)Sys.getService(BPlatformServiceContainer.TYPE);
        BSystemPlatformService platSrv = (BSystemPlatformService)plat.get("SystemService");

        JSONObject cpuInfo = new JSONObject();
        cpuInfo.put("user", "0");
        cpuInfo.put("system", "0");
        cpuInfo.put("idle", "0");
        cpuInfo.put("total", platSrv.getCurrentCpuUsage());

        JSONObject memoryInfo = new JSONObject();
        memoryInfo.put("total", platSrv.getTotalPhysicalMemory());
        memoryInfo.put("used", "0");
        memoryInfo.put("free", platSrv.getFreePhysicalMemory());
        memoryInfo.put("shared", "0");
        memoryInfo.put("cache", "0");
        memoryInfo.put("available", "0");


        BFilesystemAttributes diskatt = (BFilesystemAttributes)platSrv.getFilesystemAttributes().getChildComponents()[0];

        JSONObject diskInfo = new JSONObject();
        diskInfo.put("total", diskatt.getTotalSpace());
        diskInfo.put("used", "");
        diskInfo.put("free", diskatt.getFreeSpace());

        sysInfoPayload.put("cpuInfo", cpuInfo);
        sysInfoPayload.put("memoryInfo", memoryInfo);
        sysInfoPayload.put("diskInfo", diskInfo);

        monitorPayload.put("parameters", paramPayload);
        monitorPayload.put("systemInfo", sysInfoPayload);

        //build the topic with the jobid and send the payload
        getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/monitoring");
        System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/monitoring");
        //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
        getAwsThingInterface().setPayload(monitorPayload.toString());
        BBoolean activationResult = getAwsThingInterface().publish();
        System.out.println("MONITORING DATA SENT: " + monitorPayload.toString());

        Thread.sleep(500);

        //build reply payload
        JSONObject replyPayload = new JSONObject();
        replyPayload.put("status", "SUCCEEDED");
        JSONObject statusDetails = new JSONObject();
        statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
        statusDetails.put("step", "Data frequency updated");
        statusDetails.put("command_target", "gateway");
        replyPayload.put("statusDetails", statusDetails);

        //build the topic with the jobid and send the payload
        getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
        System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
        //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
        getAwsThingInterface().setPayload(replyPayload.toString());
        activationResult = getAwsThingInterface().publish();
        System.out.println("JOB OK SENT: " + replyPayload.toString());

        //update sync timeout
        setSyncTimeout(BRelTime.makeMinutes(newFrequency));
      }
      else if (command.equals("collect-logs"))
      {
        //Qui inserisci il tuo codice, una volta fatto comunque devi chiudere il job, il codice te lo
        // metto gia io qui dopo queste righe
        logger.fine("collect-logs command received");
        String log = "";
        BAbsTime now = BAbsTime.now();
        try
        {
          //LogHistory HistoryID is fixed
          final String LOG_HISTORY_ID = "/" + Sys.getStation().getStationName() + "/LogHistory";
          //Resolve the history
          BLocalDbHistory history = (BLocalDbHistory) BOrd.make("history:" + LOG_HISTORY_ID).get();
          //Get a cursor for the history
          TableCursor<BHistoryRecord> tableCursor = history.cursor();
          log = log.concat(LOG_HISTORY_ID + " @ " + now + "\n");
          //Get data from each record and add it into the stringRecords List
          for (BHistoryRecord recordDb : tableCursor)
            log = log.concat( recordDb.getTimestamp() + " " + ((BLogRecord) recordDb).getLogName() + " " + ((BLogRecord) recordDb).getSeverity() + " " + ((BLogRecord) recordDb).getMessage());
        }
        catch (Exception e)
        {
          log = "Error during reading 'LogHistory' from Niagara DB";
          logger.warning(log);
          e.printStackTrace();
        }
        logger.fine("collect-logs LogHistory reading completed.");

        enelCloudService.getEnelS3BucketInterface().doUploadFile(BString.make(log));
        logger.fine("collect-logs LogHistory uploaded.");

        //build reply payload
        JSONObject replyPayload = new JSONObject();
        replyPayload.put("status", "SUCCEEDED");
        JSONObject statusDetails = new JSONObject();
        statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
        statusDetails.put("step", "Station rebooting in progress");
        statusDetails.put("command_target", "gateway");
        replyPayload.put("statusDetails", statusDetails);

        //build the topic with the jobid and send the payload
        getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
        System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
        //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
        getAwsThingInterface().setPayload(replyPayload.toString());
        BBoolean activationResult = getAwsThingInterface().publish();
        System.out.println("JOB OK SENT: " + replyPayload.toString());

      }
      else if (command.equals("update-thing"))
      {
        //cycle of jobs
        String thingId = parametersInput.getString("thing_id");
        //String deviceType = parametersInput.getString("device_type");
        String model = parametersInput.getString("model");
        String maker = parametersInput.getString("maker");
        String alias = parametersInput.getString("alias");
        String geolocalization = parametersInput.getString("geolocalization");
        //String serialNumber = parametersInput.getString("serial_number");

        //find the real thing and activate it
        BEnelCloudHistoryDescriptor realDescriptor = getHistoryList().getDescriptorByUniqueId(thingId);
        if (realDescriptor == null)
        {
          System.out.println("THING ID NOT FOUND: " + thingId);

          //build activation payload
          JSONObject activationPayload = new JSONObject();
          activationPayload.put("status", "FAILED");
          JSONObject statusDetails = new JSONObject();
          statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
          statusDetails.put("step", "thing not found");
          statusDetails.put("thing_id", enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId());
          statusDetails.put("command_target", "gateway");
          activationPayload.put("statusDetails", statusDetails);

          //build the topic with the jobid and send the payload
          getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
          getAwsThingInterface().setPayload(activationPayload.toString());
          BBoolean activationResult = getAwsThingInterface().publish();
          System.out.println("FAILED UPDATE SENT, PAYLOAD: " + activationPayload.toString());

        }
        else {
          System.out.println("THING ID FOUND: " + thingId);

          // send Ok
          //build reply payload
          JSONObject replyPayload = new JSONObject();
          replyPayload.put("status", "SUCCEEDED");
          JSONObject statusDetails = new JSONObject();
          statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
          statusDetails.put("step", "Station rebooting in progress");
          statusDetails.put("command_target", "gateway");
          replyPayload.put("statusDetails", statusDetails);

          //build the topic with the jobid and send the payload
          getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
          getAwsThingInterface().setPayload(replyPayload.toString());
          BBoolean activationResult = getAwsThingInterface().publish();
          System.out.println("JOB OK SENT: " + replyPayload.toString());

          //change parameters on the real thing
          realDescriptor.setModel(model);
          realDescriptor.setMaker(maker);
          realDescriptor.setAlias(alias);
          realDescriptor.setGeolocalization(geolocalization);
        }

      }
      else if (command.equals("update-state")) //TODO: working here
      {
        //disable
        JSONObject dataobj = parametersInput.getJSONObject("data");
        String disableStatus = dataobj.getString("status");
        String secondaryCommand = dataobj.getString("command"); // disable-gateway
        String correlationId = dataobj.getString("correlation-id");

        String textForStep = "command received";

        if (secondaryCommand.equals("topology"))
        {
          System.out.println("TOPOLOGY COMMAND RECEIVED");
          textForStep = "Topology command received";
          //TODO: here the topology code
          // get the list of the unique thing id mapped into the station
          BEnelCloudHistoryDescriptor[] hisDescriptors = getHistoryList().getAllDescriptors();
          ArrayList<String> uniqueThingsIds = new ArrayList<>();
          for (int i =0; i < hisDescriptors.length; i++)
          {
            if(!uniqueThingsIds.contains(hisDescriptors[i].getThingId()))
              uniqueThingsIds.add(hisDescriptors[i].getThingId());
          }

          JSONArray tags = new JSONArray();
          JSONObject tag = new JSONObject();
          tag.put("trendId",58200000);
          tag.put("name","TOPOLOGY");
          tag.put("timestamp", getCurrentTimeStampFormatted(BAbsTime.now()));
          JSONObject value = new JSONObject();
          value.put("gateway_id",getEnelCloudService().getGatewayId());
          JSONArray things = new JSONArray();

          //cycle for creating this object
          for (int j=0; j< uniqueThingsIds.size(); j++)
          {
            String currentThingId = uniqueThingsIds.get(j);
            JSONObject thing = new JSONObject();
            thing.put("thing_id", currentThingId);
            thing.put("thing_label", currentThingId);
            JSONArray variables = new JSONArray();

            for (int i =0; i < hisDescriptors.length; i++)
            {
              if(currentThingId.equals(hisDescriptors[i].getThingId()))
              {
                JSONObject variable = new JSONObject();
                variable.put("trend_id", hisDescriptors[i].getTrendID());
                variable.put("label", hisDescriptors[i].getNiagaraId());
                variable.put("Unit", hisDescriptors[i].getUnitSymbol());
                variable.put("readable", hisDescriptors[i].isReadable());
                variable.put("writable", hisDescriptors[i].isWritable());
                variables.put(variable);
              }
            }
            thing.put("variables", variables);
            things.put(thing);
          }

          value.put("things", things);
          tag.put("value", value);
          tags.put(tag);


          //build reply payload
          JSONObject replyPayload = new JSONObject();
          replyPayload.put("status", "SUCCEEDED");
          JSONObject statusDetails = new JSONObject();
          statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
          statusDetails.put("step", textForStep);
          statusDetails.put("command_target", "gateway");
          statusDetails.put("correlation-id", correlationId);

          statusDetails.put("timestamp", "");
          statusDetails.put("gateway_id", enelCloudService.getGatewayId());
          statusDetails.put("radio_type", "Eth");
          statusDetails.put("is_online", "true");
          statusDetails.put("eventType", "TOPOLOGY");

          JSONObject data = new JSONObject();
          data.put("tags", tags);
          statusDetails.put("data", data);
          replyPayload.put("statusDetails", statusDetails);


          //

          //send the registration
          //2 invii, uno per chiudere il job e uno che manda la topologia.

          // env_prefix”/”gw_id”/”device_type”/”status_thing_id”/data
          getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/data");
          System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/data");
          //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
          getAwsThingInterface().setPayload(replyPayload.toString());
          BBoolean activationResult = getAwsThingInterface().publish();
          Thread.sleep(500);
          System.out.println("DATA SENT: " + replyPayload.toString());
          Thread.sleep(500);

          //build reply payload
          JSONObject replyPayloadJob = new JSONObject();
          replyPayloadJob.put("status", "SUCCEEDED");
          JSONObject statusDetailsJob = new JSONObject();
          statusDetailsJob.put("environment_prefix", enelCloudService.getEnvPrefix());
          statusDetailsJob.put("step", "Station rebooting in progress");
          statusDetailsJob.put("command_target", "gateway");
          replyPayloadJob.put("statusDetails", statusDetailsJob);

          //STANDARD
          //build the topic with the jobid and send the payload
          getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
          getAwsThingInterface().setPayload(replyPayloadJob.toString());
          activationResult = getAwsThingInterface().publish();
          System.out.println("JOB OK SENT: " + replyPayloadJob.toString());

        }
        else if (secondaryCommand.equals("disable-gateway"))
        {
          textForStep = "Disable command received";
          if (disableStatus.equals("ON"))
          {
            getEnelCloudService().setGatewayStatus("ON");
            BComponent[] allSchedulers = getEnelCloudService().getEnelCloudSchedulingManager().getGeneratedSchedulersContainer().getChildComponents();
            for (int k = 0; k < allSchedulers.length; k++)
            {
              BComponent currentSchedule = (BComponent)allSchedulers[k];
              //ENABLE SCHEDULERS
              BLink[] allLinks = currentSchedule.getLinks();
              for (int z = 0; z < allLinks.length; z++)
                allLinks[z].activate();

            }

            BNumericWritable statusPoint = (BNumericWritable)getEnelCloudService().getGatewayStatusPoint().get();
            statusPoint.doSet(BDouble.make(1));
          }
          else if (disableStatus.equals("OFF"))
          {
            getEnelCloudService().setGatewayStatus("OFF");
            BComponent[] allSchedulers = getEnelCloudService().getEnelCloudSchedulingManager().getGeneratedSchedulersContainer().getChildComponents();
            for (int k = 0; k < allSchedulers.length; k++)
            {
              BComponent currentSchedule = (BComponent)allSchedulers[k];
              //DISABLE SCHEDULERS
              BLink[] allLinks = currentSchedule.getLinks();
              for (int z = 0; z < allLinks.length; z++)
                allLinks[z].deactivate();
            }

            BNumericWritable statusPoint = (BNumericWritable)getEnelCloudService().getGatewayStatusPoint().get();
            statusPoint.doSet(BDouble.make(0));
          }
          else if (disableStatus.equals("MANUAL"))
          {
            getEnelCloudService().setGatewayStatus("MANUAL");
            BComponent[] allSchedulers = getEnelCloudService().getEnelCloudSchedulingManager().getGeneratedSchedulersContainer().getChildComponents();
            for (int k = 0; k < allSchedulers.length; k++)
            {
              BComponent currentSchedule = (BComponent)allSchedulers[k];
              //DISABLE SCHEDULERS
              BLink[] allLinks = currentSchedule.getLinks();
              for (int z = 0; z < allLinks.length; z++)
                allLinks[z].deactivate();
            }

            BNumericWritable statusPoint = (BNumericWritable)getEnelCloudService().getGatewayStatusPoint().get();
            statusPoint.doSet(BDouble.make(1));
          }

          //build reply payload
          JSONObject replyPayload = new JSONObject();
          replyPayload.put("status", "SUCCEEDED");
          JSONObject statusDetails = new JSONObject();
          statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
          statusDetails.put("step", textForStep);
          statusDetails.put("command_target", "gateway");
          statusDetails.put("correlation-id", correlationId);
          replyPayload.put("statusDetails", statusDetails);

          //build the topic with the jobid and send the payload
          getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
          //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
          getAwsThingInterface().setPayload(replyPayload.toString());
          BBoolean activationResult = getAwsThingInterface().publish();
          System.out.println("JOB OK SENT: " + replyPayload.toString());

        }


      }
      else
      {
        System.out.println("COMMAND NOT VALID");

        //build activation payload
        JSONObject activationPayload = new JSONObject();
        activationPayload.put("status", "FAILED");
        JSONObject statusDetails = new JSONObject();
        statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
        statusDetails.put("step", "command not valid");
        statusDetails.put("command_target", "gateway");
        activationPayload.put("statusDetails", statusDetails);

        //build the topic with the jobid and send the payload
        getAwsThingInterface().setTopic("$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
        System.out.println("TOPIC GENERATED: " + "$aws/things/" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() + "/jobs/" + jobId + "/update");
        //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
        getAwsThingInterface().setPayload(activationPayload.toString());
        BBoolean activationResult = getAwsThingInterface().publish();
        System.out.println("JOB FAILED SENT: " + activationPayload.toString());
      }
    }catch(Exception e)
    {
      e.printStackTrace();
    }
  }

    // elaborate messages coming from the cloud for thing registration/activation
    public void doNewMessageFromThingCloud(BString payload)
    {
        try {
            System.out.println("PAYLOAD FOR THING RECEIVED: " + payload.getString());

            //JSONObject payloadRec = new JSONObject(SlotPath.escape(payload.getString()));
            JSONObject payloadRec = new JSONObject(payload.getString());
            //JSONObject payloadRec = new JSONObject("{\"timestamp\":1586358603,\"execution\":{\"jobId\":\"esol_ap07615_dev-2004081510-16686d1f-15ce-4693-9ea7-0f9d2b6fc67a\",\"status\":\"QUEUED\",\"queuedAt\":1586358603,\"lastUpdatedAt\":1586358603,\"versionNumber\":1,\"executionNumber\":1,\"jobDocument\":{\"version\":\"1.0\",\"command\":\"activate-thing\",\"parameters\":{\"thing_id\":\"HUMPIOX02_thing1\",\"interaction_mode\":\"gateway\",\"radio_type\":\"Serial\",\"device_type\":\"modbus_rtu\",\"serial_number\":\"1\",\"model\":\"MX9\",\"portName\":\"/dev/ttyXRUSB0\",\"baudRate\":9600,\"dataBits\":8,\"parity\":0,\"stopBits\":0,\"maker\":\"EnergyTeam\",\"filter_tag\":[],\"environment_prefix\":\"esol_ap07615_dev\"}}}}");
            //JSONObject jobs = payloadRec.getJSONObject("jobs");
            //JSONArray quequed = jobs.getJSONArray("QUEUED");
            //JSONObject execution = payloadRec.getJSONObject("execution");
            //JSONObject jobDocument = execution.getJSONObject("jobDocument");
            JSONObject parametersInput = payloadRec.getJSONObject("parameters");
            JSONObject data = parametersInput.getJSONObject("data");

            String command = data.getString("command"); // "activate-thing"
            System.out.println("COMMAND RECEIVED: " + command);
            String commandId = payloadRec.getString("command_id");
            String thingId = "";
            String correlationId = data.getString("correlation-id");

            if (command.equals("set-value") && !getEnelCloudService().getGatewayStatus().equals("OFF"))
            {
                // parse needed params
                JSONArray values = data.getJSONArray("values");

                String uniqueId = ((JSONObject)values.get(0)).getString("datapoint");
                String value = ((JSONObject)values.get(0)).getString("value");

                boolean setValueResult = false;

                BEnelCloudHistoryDescriptor realDescriptor = getHistoryList().getDescriptorByUniqueId(uniqueId);
                thingId = realDescriptor.getThingId();

                if (realDescriptor != null) {
                    System.out.println("UNIQUE ID FOUND: " + uniqueId);
                    // call the set system
                  if (value.equals("NULL"))
                    setValueResult = enelCloudService.getEnelCloudSetpointManager().resetValueToPoint(realDescriptor.getDeviceName(), realDescriptor.getPointName(), BRelTime.DEFAULT, "in8");
                  else
                    setValueResult = enelCloudService.getEnelCloudSetpointManager().setValueToPoint(realDescriptor.getDeviceName(), realDescriptor.getPointName(), value, BRelTime.DEFAULT, "in8");
                }

                if (setValueResult) {
                    //build reply payload
                    JSONObject replyPayload = new JSONObject();
                    replyPayload.put("status", "SUCCEEDED");
                    replyPayload.put("commandId", commandId);
                    JSONObject statusDetails = new JSONObject();

                    statusDetails.put("correlation_id", correlationId);
                    replyPayload.put("statusDetails", statusDetails);

                    //build the topic with the jobid and send the payload
                  //esol_ap07615_dev/AMACA01/+/command/inbound //TODO
                    getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                    System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                    //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                    getAwsThingInterface().setPayload(replyPayload.toString());
                    BBoolean activationResult = getAwsThingInterface().publish();
                    System.out.println("OK SENT: " + replyPayload.toString());
                }
                else
                {
                    //build activation payload
                    JSONObject activationPayload = new JSONObject();
                    activationPayload.put("status", "FAILED");
                    activationPayload.put("commandId", commandId);
                    JSONObject statusDetails = new JSONObject();
                    statusDetails.put("correlation_id", correlationId);
                    activationPayload.put("statusDetails", statusDetails);

                    //build the topic with the jobid and send the payload //TODO
                    getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                    System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                    //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                    getAwsThingInterface().setPayload(activationPayload.toString());
                    BBoolean activationResult = getAwsThingInterface().publish();
                    System.out.println("FAILED SENT: " + activationPayload.toString());
                }

            }
          else if (command.equals("set-value-date")  && !getEnelCloudService().getGatewayStatus().equals("OFF"))
          {
           //TODO: ??
          }
            else if (command.equals("set-profile")  && !getEnelCloudService().getGatewayStatus().equals("OFF"))
            {

              String uniqueId = data.getString("datapoint");

              boolean commandResult = false;

              BEnelCloudHistoryDescriptor realDescriptor = getHistoryList().getDescriptorByUniqueId(uniqueId);
              thingId = realDescriptor.getThingId();

              if (realDescriptor != null) {
                System.out.println("UNIQUE ID FOUND: " + uniqueId);
                // call the set system
                commandResult = enelCloudService.getEnelCloudSchedulingManager().schedulingSetter(realDescriptor.getDeviceName(), realDescriptor.getPointName(), "profile", data);
              }

              if (commandResult) {
                //build reply payload
                JSONObject replyPayload = new JSONObject();
                replyPayload.put("status", "SUCCEEDED");
                replyPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();

                statusDetails.put("correlation_id", correlationId);
                replyPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload
                //esol_ap07615_dev/AMACA01/+/command/inbound //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(replyPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("OK SENT: " + replyPayload.toString());
              }
              else
              {
                //build activation payload
                JSONObject activationPayload = new JSONObject();
                activationPayload.put("status", "FAILED");
                activationPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();
                statusDetails.put("correlation_id", correlationId);
                activationPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(activationPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("FAILED SENT: " + activationPayload.toString());
              }

            }
            else if (command.equals("set-task")  && !getEnelCloudService().getGatewayStatus().equals("OFF"))
            {

              String uniqueId = data.getString("datapoint");

              boolean commandResult = false;

              BEnelCloudHistoryDescriptor realDescriptor = getHistoryList().getDescriptorByUniqueId(uniqueId);
              thingId = realDescriptor.getThingId();

              if (realDescriptor != null) {
                System.out.println("UNIQUE ID FOUND: " + uniqueId);
                // call the set system
                commandResult = enelCloudService.getEnelCloudSchedulingManager().schedulingSetter(realDescriptor.getDeviceName(), realDescriptor.getPointName(), "task", data);
              }

              if (commandResult) {
                //build reply payload
                JSONObject replyPayload = new JSONObject();
                replyPayload.put("status", "SUCCEEDED");
                replyPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();

                statusDetails.put("correlation_id", correlationId);
                replyPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload
                //esol_ap07615_dev/AMACA01/+/command/inbound //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(replyPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("OK SENT: " + replyPayload.toString());
              }
              else
              {
                //build activation payload
                JSONObject activationPayload = new JSONObject();
                activationPayload.put("status", "FAILED");
                activationPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();
                statusDetails.put("correlation_id", correlationId);
                activationPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(activationPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("FAILED SENT: " + activationPayload.toString());
              }

            }
            else if (command.equals("delete-profile")  && !getEnelCloudService().getGatewayStatus().equals("OFF") )
            {

              String uniqueId = data.getString("datapoint");
              String profileId = data.getString("profile_id");

              boolean commandResult = false;

              BEnelCloudHistoryDescriptor realDescriptor = getHistoryList().getDescriptorByUniqueId(uniqueId);
              thingId = realDescriptor.getThingId();

              if (realDescriptor != null) {
                System.out.println("UNIQUE ID FOUND: " + uniqueId);
                // call the set system
                commandResult = enelCloudService.getEnelCloudSchedulingManager().schedulingDeleter(realDescriptor.getDeviceName(), realDescriptor.getPointName(), "profile", profileId);
              }

              if (commandResult) {
                //build reply payload
                JSONObject replyPayload = new JSONObject();
                replyPayload.put("status", "SUCCEEDED");
                replyPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();

                statusDetails.put("correlation_id", correlationId);
                replyPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload
                //esol_ap07615_dev/AMACA01/+/command/inbound //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(replyPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("OK SENT: " + replyPayload.toString());
              }
              else
              {
                //build activation payload
                JSONObject activationPayload = new JSONObject();
                activationPayload.put("status", "FAILED");
                activationPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();
                statusDetails.put("correlation_id", correlationId);
                activationPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(activationPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("FAILED SENT: " + activationPayload.toString());
              }

            }
            else if (command.equals("delete-task")  && !getEnelCloudService().getGatewayStatus().equals("OFF"))
            {

              String uniqueId = data.getString("datapoint");
              String taskId = data.getString("task_id");

              boolean commandResult = false;

              BEnelCloudHistoryDescriptor realDescriptor = getHistoryList().getDescriptorByUniqueId(uniqueId);
              thingId = realDescriptor.getThingId();

              if (realDescriptor != null) {
                System.out.println("UNIQUE ID FOUND: " + uniqueId);
                // call the set system
                commandResult = enelCloudService.getEnelCloudSchedulingManager().schedulingDeleter(realDescriptor.getDeviceName(), realDescriptor.getPointName(), "task", taskId);
              }

              if (commandResult) {
                //build reply payload
                JSONObject replyPayload = new JSONObject();
                replyPayload.put("status", "SUCCEEDED");
                replyPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();

                statusDetails.put("correlation_id", correlationId);
                replyPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload
                //esol_ap07615_dev/AMACA01/+/command/inbound //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(replyPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("OK SENT: " + replyPayload.toString());
              }
              else
              {
                //build activation payload
                JSONObject activationPayload = new JSONObject();
                activationPayload.put("status", "FAILED");
                activationPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();
                statusDetails.put("correlation_id", correlationId);
                activationPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(activationPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("FAILED SENT: " + activationPayload.toString());
              }

            }
            else if (command.equals("get-task")  && !getEnelCloudService().getGatewayStatus().equals("OFF"))
            {

              String uniqueId = data.getString("datapoint");
              String taskId = data.getString("task_id");

              String commandResult = "";

              BEnelCloudHistoryDescriptor realDescriptor = getHistoryList().getDescriptorByUniqueId(uniqueId);
              thingId = realDescriptor.getThingId();

              if (realDescriptor != null) {
                System.out.println("UNIQUE ID FOUND: " + uniqueId);
                // call the set system
                commandResult = enelCloudService.getEnelCloudSchedulingManager().schedulingGetter(realDescriptor.getDeviceName(), realDescriptor.getPointName(), "task", taskId);
              }

              if (!commandResult.equals("")) {
                //build reply payload
                JSONObject replyPayload = new JSONObject();
                replyPayload.put("status", "SUCCEEDED");
                replyPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();

                statusDetails.put("correlation_id", correlationId);
                replyPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload
                //esol_ap07615_dev/AMACA01/+/command/inbound //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(replyPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("OK SENT: " + replyPayload.toString());
              }
              else
              {
                //build activation payload
                JSONObject activationPayload = new JSONObject();
                activationPayload.put("status", "FAILED");
                activationPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();
                statusDetails.put("correlation_id", correlationId);
                activationPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(activationPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("FAILED SENT: " + activationPayload.toString());
              }

            }
            else if (command.equals("disable-gateway")  && !getEnelCloudService().getGatewayStatus().equals("OFF"))
            {

              String uniqueId = data.getString("datapoint");
              String taskId = data.getString("task_id");

              String commandResult = "";

              BEnelCloudHistoryDescriptor realDescriptor = getHistoryList().getDescriptorByUniqueId(uniqueId);
              thingId = realDescriptor.getThingId();

              if (realDescriptor != null) {
                System.out.println("UNIQUE ID FOUND: " + uniqueId);
                // call the set system
                commandResult = enelCloudService.getEnelCloudSchedulingManager().schedulingGetter(realDescriptor.getDeviceName(), realDescriptor.getPointName(), "task", taskId);
              }

              if (!commandResult.equals("")) {
                //build reply payload
                JSONObject replyPayload = new JSONObject();
                replyPayload.put("status", "SUCCEEDED");
                replyPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();

                statusDetails.put("correlation_id", correlationId);
                replyPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload
                //esol_ap07615_dev/AMACA01/+/command/inbound //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(replyPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("OK SENT: " + replyPayload.toString());
              }
              else
              {
                //build activation payload
                JSONObject activationPayload = new JSONObject();
                activationPayload.put("status", "FAILED");
                activationPayload.put("commandId", commandId);
                JSONObject statusDetails = new JSONObject();
                statusDetails.put("correlation_id", correlationId);
                activationPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(activationPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("FAILED SENT: " + activationPayload.toString());
              }

            }
            else
            {
                System.out.println("COMMAND NOT VALID");

                //build activation payload
                JSONObject activationPayload = new JSONObject();
                activationPayload.put("status", "FAILED");
                JSONObject statusDetails = new JSONObject();
                statusDetails.put("environment_prefix", enelCloudService.getEnvPrefix());
                statusDetails.put("step", "command not valid or permission denied");
                statusDetails.put("command_target", "gateway");
                activationPayload.put("statusDetails", statusDetails);

                //build the topic with the jobid and send the payload //TODO
                getAwsThingInterface().setTopic("" + enelCloudService.getEnvPrefix() + "/" + enelCloudService.getGatewayId() + "/" + thingId  + "/command/outbound");
                System.out.println("TOPIC GENERATED: " + "" + enelCloudService.getEnvPrefix() + "_" + enelCloudService.getGatewayId() +  "/" + thingId  + "/command/outbound");
                //$aws/things/esol_ap07615_dev_AMACA01/jobs/esol_ap07615_dev-2002260941-c6790995-944e-4e65-b07f-8be8a5d71865/update
                getAwsThingInterface().setPayload(activationPayload.toString());
                BBoolean activationResult = getAwsThingInterface().publish();
                System.out.println("JOB FAILED SENT: " + activationPayload.toString());
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

  // call the refresh histories descriptors
  public void doRefreshHistoriesList()
  {
    updateHistoriesDescriptors();
  }

  // call the remove of all histories descriptors
  public void doRemoveAllHistoriesDescriptors()
  {
    getHistoryList().removeAllHistoriesDescriptors();
  }

  // submit the sync histories thread
  public void doStartHistoriesSyncJob(Context cx)
  {
    // check if the gateway is registered?

    // get all descriptors
    BEnelCloudHistoryDescriptor[] allDescriptors = getHistoryList().getAllDescriptors();
    // get the number of all descriptors
    int numberOfDescriptors = allDescriptors.length;

    // check fi reduced traffic mode is enabled.
 /*   if (getReducedTrafficMode())
    {
      // start the job only if we have almost one descriptor
      if (numberOfDescriptors > 0) // 0 - 300
      {
        // check if the job already exist, if not start it!
        if (historySyncjobRT == null && jobsInRunning() == false)
        {
          historySyncjobRT = new BEnelCloudReducedTrafficModeHistoriesSyncJob(this, getHistoryList().getAllDescriptors());
          BOrd jobOrd = historySyncjobRT.submit(cx);
        } else // if one job was already instantiated re-submit only if the previous one isn't in running
        {
          if (!historySyncjobRT.getJobState().isRunning() && jobsInRunning() == false) // check if the previous is finished
          {
            // start the new one
            historySyncjobRT = new BEnelCloudReducedTrafficModeHistoriesSyncJob(this, getHistoryList().getAllDescriptors());
            BOrd jobOrd = historySyncjobRT.submit(cx);
          }
        }
      }
    }*/
  //  else // standard traffic mode

      // start the correct number of jobs based on descriptors qty.
      if (numberOfDescriptors > 0) // 0 - 300
      {
        // as default the end index is the number of descriptors
        int endIndex = numberOfDescriptors;

        // if is > 300 set the end to 300
        //if (numberOfDescriptors > 300)
          //endIndex = 299;

        // check if the job already exist, if not start it!
        if (historySyncjob1 == null)
        {
          historySyncjob1 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 0, endIndex);
          BOrd jobOrd = historySyncjob1.submit(cx);
        } else // if one job was already instantiated re-submit only if the previous one isn't in running
        {
          if (!historySyncjob1.getJobState().isRunning()) // check if the previous is finished
          {
            // start the new one
            historySyncjob1 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 0, endIndex);
            BOrd jobOrd = historySyncjob1.submit(cx);
          }
        }
      }
     /* if (numberOfDescriptors > 300) // 300 - 600
      {
        // as default the end index is the number of descriptors
        int endIndex = numberOfDescriptors;

        // if is > 600 set the end to 600
        if (numberOfDescriptors > 600)
          endIndex = 599;

        // check if the job already exist, if not start it!
        if (historySyncjob2 == null)
        {
          historySyncjob2 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 300, endIndex);
          BOrd jobOrd = historySyncjob2.submit(cx);
        } else // if one job was already instantiated re-submit only if the previous one isn't in running
        {
          if (!historySyncjob2.getJobState().isRunning()) // check if the previous is finished
          {
            // start the new one
            historySyncjob2 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 300, endIndex);
            BOrd jobOrd = historySyncjob2.submit(cx);
          }
        }
      }
      if (numberOfDescriptors > 600) // 600 - 900
      {
        // as default the end index is the number of descriptors
        int endIndex = numberOfDescriptors;

        // if is > 900 set the end to 900
        if (numberOfDescriptors > 900)
          endIndex = 899;

        // check if the job already exist, if not start it!
        if (historySyncjob3 == null)
        {
          historySyncjob3 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 600, endIndex);
          BOrd jobOrd = historySyncjob3.submit(cx);
        } else // if one job was already instantiated re-submit only if the previous one isn't in running
        {
          if (!historySyncjob3.getJobState().isRunning()) // check if the previous is finished
          {
            // start the new one
            historySyncjob3 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 600, endIndex);
            BOrd jobOrd = historySyncjob3.submit(cx);
          }
        }
      }
      if (numberOfDescriptors > 900) //900 - 1200
      {
        // as default the end index is the number of descriptors
        int endIndex = numberOfDescriptors;

        // if is > 1200 set the end to 1200
        if (numberOfDescriptors > 1200)
          endIndex = 1199;

        // check if the job already exist, if not start it!
        if (historySyncjob4 == null)
        {
          historySyncjob4 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 900, endIndex);
          BOrd jobOrd = historySyncjob4.submit(cx);
        } else // if one job was already instantiated re-submit only if the previous one isn't in running
        {
          if (!historySyncjob4.getJobState().isRunning()) // check if the previous is finished
          {
            // start the new one
            historySyncjob4 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 900, endIndex);
            BOrd jobOrd = historySyncjob4.submit(cx);
          }
        }
      }
      if (numberOfDescriptors > 1200)
      {
        // as default the end index is the number of descriptors
        int endIndex = numberOfDescriptors;

        // if is > 1500 set the end to 1500
        if (numberOfDescriptors > 1500)
          endIndex = 1499;

        // check if the job already exist, if not start it!
        if (historySyncjob5 == null)
        {
          historySyncjob5 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 1200, endIndex);
          BOrd jobOrd = historySyncjob5.submit(cx);
        } else // if one job was already instantiated re-submit only if the previous one isn't in running
        {
          if (!historySyncjob5.getJobState().isRunning()) // check if the previous is finished
          {
            // start the new one
            historySyncjob5 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 1200, endIndex);
            BOrd jobOrd = historySyncjob5.submit(cx);
          }
        }
      }
      if (numberOfDescriptors > 1500)
      {
        // as default the end index is the number of descriptors
        int endIndex = numberOfDescriptors;

        // check if the job already exist, if not start it!
        if (historySyncjob6 == null)
        {
          historySyncjob6 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 1500, endIndex);
          BOrd jobOrd = historySyncjob6.submit(cx);
        } else // if one job was already instantiated re-submit only if the previous one isn't in running
        {
          if (!historySyncjob6.getJobState().isRunning()) // check if the previous is finished
          {
            // start the new one
            historySyncjob6 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptors(), 1500, endIndex);
            BOrd jobOrd = historySyncjob6.submit(cx);
          }
        }
      }*/

  }

  public void doStartHistoriesSyncJobFastQueue(Context cx)
  {

    // get all descriptors
    BEnelCloudHistoryDescriptor[] allDescriptors = getHistoryList().getAllDescriptorsFastQueue();
    // get the number of all descriptors
    int numberOfDescriptors = allDescriptors.length;

    // start the correct number of jobs based on descriptors qty.
    if (numberOfDescriptors > 0) // 0 - 300
    {
      // as default the end index is the number of descriptors
      int endIndex = numberOfDescriptors;

      // check if the job already exist, if not start it!
      if (historySyncjob2 == null)
      {
        historySyncjob2 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptorsFastQueue(), 0, endIndex);
        BOrd jobOrd = historySyncjob2.submit(cx);
      } else // if one job was already instantiated re-submit only if the previous one isn't in running
      {
        if (!historySyncjob2.getJobState().isRunning()) // check if the previous is finished
        {
          // start the new one
          historySyncjob2 = new BEnelCloudHistoriesSyncJob(this, getHistoryList().getAllDescriptorsFastQueue(), 0, endIndex);
          BOrd jobOrd = historySyncjob2.submit(cx);
        }
      }
    }
  }
  private String getCurrentTimeStampFormatted(BAbsTime timestamp)
  {
    //use utc
    timestamp = timestamp.toUtcTime();
    String result = "";

    result += timestamp.getYear() + "" + mustBeDoubleDigit("" + (timestamp.getMonth().getOrdinal()+1)) + "" + mustBeDoubleDigit("" + timestamp.getDay());
    result +=  "" + mustBeDoubleDigit("" + timestamp.getHour()) + "" + mustBeDoubleDigit("" + timestamp.getMinute()) + "" + mustBeDoubleDigit("" + timestamp.getSecond()) + "" + timestamp.getMillisecond();

    return result;
  }

  private String mustBeDoubleDigit(String in)
  {
    if (in.length() == 1)
      return "0" + in;
    return in;
  }

////////////////////////////////////////////////////////////////
// Thread management
////////////////////////////////////////////////////////////////

  // start a new ticket for thread
  private void startThreadClock()
  {
    // first, stop the previous
    stopThreadClock();

    // start the new one
    submitHistorySyncTicket = Clock.schedulePeriodically(this, getSyncTimeout(), startHistoriesSyncJob, null);
  }

  // stop current ticket and delete it.
  private void stopThreadClock()
  {
    if (submitHistorySyncTicket != null)
    {
      submitHistorySyncTicket.cancel();
      submitHistorySyncTicket = null;
    }
  }

  // start a new ticket for thread
  private void startThreadClockFastQueue()
  {
    // first, stop the previous
    stopThreadClockFastQueue();

    // start the new one
    submitHistorySyncTicketFastQueue = Clock.schedulePeriodically(this, getSyncTimeoutFastQueue(), startHistoriesSyncJobFastQueue, null);
  }

  // stop current ticket and delete it.
  private void stopThreadClockFastQueue()
  {
    if (submitHistorySyncTicketFastQueue != null)
    {
      submitHistorySyncTicketFastQueue.cancel();
      submitHistorySyncTicketFastQueue = null;
    }
  }

////////////////////////////////////////////////////////////////
// History DB
////////////////////////////////////////////////////////////////

  // get all histories (numeric and boolean) available inside the station and create the descriptors
  private void updateHistoriesDescriptors()
  {
    // get History service
    BHistoryService service = (BHistoryService)Sys.getService(BHistoryService.TYPE);
    // get DB
    BHistoryDatabase db = service.getDatabase();
    // get the list of all available histories
    BIHistory[] histories = db.getHistories();

    // parse all histories
    for (int i = 0; i < histories.length; i++)
    {
      // identify only the numeric and boolean ones
      if(histories[i].getRecordType().equals(BTypeSpec.make(BNumericTrendRecord.TYPE)) || histories[i].getRecordType().equals(BTypeSpec.make(BBooleanTrendRecord.TYPE)))
      {
        logger.fine("Trying to add this history: " + histories[i].getId());
        // try to add the descriptor for current history
        getHistoryList().addHistoryDescriptor(histories[i], enelCloudService.getGatewayId());
      }
    }

  }

 ///////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

  public String getCloudLabHistoryEndpoint()
  {
    return "";
    //return cloudLabService.getCloudLabUrl() + cloudLabService.getInstallationId() + "/readings";
  }


  // check if some RTM jobs are in running.
  public boolean jobsInRunning()
  {
    if (historySyncjobRT1 != null)
      if (historySyncjobRT1.getProgress() < 100)
        return true;

    if (historySyncjobRT2 != null)
      if (historySyncjobRT2.getProgress() < 100)
        return true;

    if (historySyncjobRT3 != null)
      if (historySyncjobRT3.getProgress() < 100)
        return true;

    if (historySyncjobRT4 != null)
      if (historySyncjobRT4.getProgress() < 100)
        return true;

    if (historySyncjobRT5 != null)
      if (historySyncjobRT5.getProgress() < 100)
        return true;

    if (historySyncjobRT6 != null)
      if (historySyncjobRT6.getProgress() < 100)
        return true;

    return false;
  }

////////////////////////////////////////////////////////////////
// Service life-cycle
////////////////////////////////////////////////////////////////

  @Override
  public void descendantsStarted() throws Exception
  {
    super.descendantsStarted();

    // start the Clock for sync histories thread
    if (getEnabled()) {
      startThreadClock();
      startThreadClockFastQueue();
    }
    // check link for topic for active thing
    EnelCloudUtils.makeLinkTopic("TopicLink",getAwsThingInterface(),this,"newMessageOnNotifyNextTopic", "newMessageFromCloud" );
    EnelCloudUtils.makeLinkTopic("TopicThingLink",getAwsThingCommandsInterface(),this,"newMessageOnNotifyNextTopic", "newMessageFromThingCloud" );

  }

  @Override
  public void changed(Property property, Context context)
  {
    super.changed(property, context);

    // check if we are in a running state
    if (!isRunning())return;
    if (!Sys.atSteadyState())return;

    // changed timeout? Update the thread ticket
    if (property == syncTimeout && getEnabled())
      startThreadClock();

    if (property == syncTimeoutFastQueue && getEnabled())
      startThreadClockFastQueue();

    // service disabled -> stop thread ticket
    if (property == enabled)
      if (!getEnabled()) {
        stopThreadClock();
        stopThreadClockFastQueue();
      }
      else {
        startThreadClock();
        startThreadClockFastQueue();
      }
  }


  // get service type for service registration
  public Type[] getServiceTypes()
  {
    return serviceTypes;
  }

  private static Type[] serviceTypes = new Type[] { TYPE };

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

  @Override
  public BIcon getIcon() {
    return BIcon.std("historyDatabase.png");
  }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

  private Clock.Ticket submitHistorySyncTicket = null;
  private Clock.Ticket submitHistorySyncTicketFastQueue = null;
  private BEnelCloudService enelCloudService = null;
  private BEnelCloudHistoriesSyncJob historySyncjob1 = null;
  private BEnelCloudHistoriesSyncJob historySyncjob2 = null;
  private BEnelCloudHistoriesSyncJob historySyncjob3 = null;
  private BEnelCloudHistoriesSyncJob historySyncjob4 = null;
  private BEnelCloudHistoriesSyncJob historySyncjob5 = null;
  private BEnelCloudHistoriesSyncJob historySyncjob6 = null;
  private BEnelCloudReducedTrafficModeHistoriesSyncJob historySyncjobRT = null;
  public BEnelCloudHistoriesSyncJobForRTM historySyncjobRT1 = null;
  public BEnelCloudHistoriesSyncJobForRTM historySyncjobRT2 = null;
  public BEnelCloudHistoriesSyncJobForRTM historySyncjobRT3 = null;
  public BEnelCloudHistoriesSyncJobForRTM historySyncjobRT4 = null;
  public BEnelCloudHistoriesSyncJobForRTM historySyncjobRT5 = null;
  public BEnelCloudHistoriesSyncJobForRTM historySyncjobRT6 = null;
  public static final Logger logger = Logger.getLogger("EnelCloudHistoryManager");
}
