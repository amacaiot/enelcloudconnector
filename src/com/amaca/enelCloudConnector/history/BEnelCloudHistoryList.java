package com.amaca.enelCloudConnector.history;

import com.amaca.enelCloudConnector.scheduling.datastructures.BEnelCloudProfile;

import javax.baja.history.BHistoryId;
import javax.baja.history.BIHistory;
import javax.baja.naming.SlotPath;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.BComponent;
import javax.baja.sys.BIcon;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;

/**
 * Created by Amaca on 11/03/2018.
 */

@NiagaraType
public class BEnelCloudHistoryList extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.history.BEnelCloudHistoryList(2979906276)1.0$ @*/
/* Generated Sun Mar 11 15:14:13 CET 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////

  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudHistoryList.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    // empty constructor
    public BEnelCloudHistoryList()
    {
        super();
    }

////////////////////////////////////////////////////////////////
// Histories access methods
////////////////////////////////////////////////////////////////

    public void addHistoryDescriptor(BIHistory history, String instID)
    {
        // check if the history already have the descriptor, if no, continue.
        if (!checkIfExist(history.getId()))
        {
            // create a new instance of history descriptor
            BEnelCloudHistoryDescriptor historyDesc = new BEnelCloudHistoryDescriptor();
            // set all useful slots
            historyDesc.setHistoryId(history.getId());
            historyDesc.setHistoryType(history.getRecordType());
            historyDesc.calculateDeviceAndPointName();
            historyDesc.setInstallationID(instID);
            // add the new descriptor inside the list
            add(history.getId().getDeviceName() + SlotPath.escape("/") + history.getId().getHistoryName(), historyDesc);
        }
        else
        {
            if (logger.isLoggable(Level.FINE))
                logger.fine("History already have a descriptor: " + history.getId());
        }
    }


    // delete all history descriptors
    public void removeAllHistoriesDescriptors()
    {
        removeAll();
    }

    // check if a specified history id already are managed and available in the list with their descriptor.
    public boolean checkIfExist(BHistoryId historyId)
    {
        // get all descriptor available in the list
        BEnelCloudHistoryDescriptor[] currentManagedHistories = getChildren(BEnelCloudHistoryDescriptor.class);

        // cycle all descriptors
        for (int i = 0; i < currentManagedHistories.length; i++)
        {
            // check if one have the same id of the parameter
            if (currentManagedHistories[i].getHistoryId().equals(historyId))
                return true; //found, return true.
        }
        // if not found, return false.
        return false;
    }

    // get all history descriptors
    public BEnelCloudHistoryDescriptor[] getAllDescriptors()
    {
        // get all descriptor available in the list
        BEnelCloudHistoryDescriptor[] currentManagedHistories = getChildren(BEnelCloudHistoryDescriptor.class);
        // return the array
        return currentManagedHistories;
    }

    // get all history descriptors
    public BEnelCloudHistoryDescriptor[] getAllDescriptorsFastQueue()
    {
        // get all descriptor available in the list
        BEnelCloudHistoryDescriptor[] currentManagedHistories = getChildren(BEnelCloudHistoryDescriptor.class);
        ArrayList<BEnelCloudHistoryDescriptor> fastList = new ArrayList<>();
        // cycle all descriptors
        for (int i = 0; i < currentManagedHistories.length; i++)
        {
            if (currentManagedHistories[i].getFastQueue())
                fastList.add(currentManagedHistories[i]); //found, return.
        }
        return fastList.toArray(new BEnelCloudHistoryDescriptor[fastList.size()]);
    }

    public BEnelCloudHistoryDescriptor getDescriptorByNiagaraId(String niagaraId)
    {
        // get all descriptor available in the list
        BEnelCloudHistoryDescriptor[] currentManagedHistories = getChildren(BEnelCloudHistoryDescriptor.class);

        // cycle all descriptors
        for (int i = 0; i < currentManagedHistories.length; i++)
        {
            // check if one have the same id of the parameter
            currentManagedHistories[i].generateNiagaraId();
            if (currentManagedHistories[i].getNiagaraId().equals(niagaraId))
                return currentManagedHistories[i]; //found, return.
        }
        // if not found, return null.
        return null;
    }

    public BEnelCloudHistoryDescriptor getDescriptorByUniqueId(String uniqueId)
    {
        // get all descriptor available in the list
        BEnelCloudHistoryDescriptor[] currentManagedHistories = getChildren(BEnelCloudHistoryDescriptor.class);

        // cycle all descriptors
        for (int i = 0; i < currentManagedHistories.length; i++)
        {
            // check if one have the same id of the parameter
            if (currentManagedHistories[i].getUniqueId().equals(uniqueId))
                return currentManagedHistories[i]; //found, return.
        }
        // if not found, return null.
        return null;
    }


////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("historyGroup.png");
    }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

    public static final Logger logger = Logger.getLogger("CloudLabHistoryList");
}
