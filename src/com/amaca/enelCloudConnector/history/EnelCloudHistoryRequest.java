package com.amaca.enelCloudConnector.history;

import com.tridium.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by Amaca on 14/04/2018.
 */
public class EnelCloudHistoryRequest
{

    ////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    public EnelCloudHistoryRequest(int limit)
    {
        // create an empty json array
        resultArray = new JSONArray();

        // create the descriptorindexs and initialize it
        descriptorIndexs = new ArrayList<>();

        // set the current limit related to the number of records per request
        this.limit = limit;
    }

////////////////////////////////////////////////////////////////
// Add method
////////////////////////////////////////////////////////////////

    // check if there is enough space in the request for the input array values
    public boolean canAdd(JSONArray inputArray)
    {
        // calculate the sum from input array and current array size
        int sum = resultArray.length() + inputArray.length();

        // check if is >= the limit and return false if it is.
        if (sum >= limit)
            return false;
        else
            return true;
    }

    public boolean addValues(JSONArray inputArray, int descriptorIndex)
    {
        // check if there is space for the input array
        if (canAdd(inputArray))
        {
            try
            {
                // put all values of the input array inside the result array.
                for (int i = 0; i < inputArray.length(); i++)
                {
                    resultArray.put(inputArray.get(i));
                }

                //update the descriptor list
                descriptorIndexs.add(new Integer(descriptorIndex));

                // all ok, all added, return true
                return true;
            }
            catch (Exception e) // if occurs any exception return false
            {
                return false;
            }
        }
        else // return false, not enough space.
        {
            return false;
        }
    }


 ////////////////////////////////////////////////////////////////
// Get method
////////////////////////////////////////////////////////////////

    // get the string with all data
    public String getBodyRequest()
    {
        return resultArray.toString();
    }

 ////////////////////////////////////////////////////////////////
// Confirm method
////////////////////////////////////////////////////////////////

    // confirm all record sent
    public void confirmAll(BEnelCloudHistoryDescriptor[] allDescriptors)
    {
        // iterate all descriptors indexs and call the confirmation on descriptor.
        for (int i = 0; i < descriptorIndexs.size(); i++)
        {
            try
            {
                allDescriptors[descriptorIndexs.get(i).intValue()].confirmLastRecordAndLastSync();
            }
            catch (Exception e)
            {
                System.out.println("Exception in ConfirmAll on CloudLabHistoryReqeust. Index: " + i + "on size: " + descriptorIndexs.size());
            }
        }
    }

////////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

 ////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
    private int limit = 100;
    private ArrayList<Integer> descriptorIndexs = null;
    private JSONArray resultArray = null;
}
