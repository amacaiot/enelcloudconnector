package com.amaca.enelCloudConnector.history.job;

import com.amaca.enelCloudConnector.history.BEnelCloudHistoryDescriptor;
import com.amaca.enelCloudConnector.history.BEnelCloudHistoryManager;
import com.amaca.enelCloudConnector.history.EnelCloudHistoryRequest;
import com.amaca.enelCloudConnector.network.BEnelCloudPostRequestsManager;
import com.tridium.json.JSONArray;

import javax.baja.job.BSimpleJob;
import javax.baja.job.JobLogItem;
import javax.baja.naming.BOrd;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by Amaca on 14/04/2018.
 */
@NiagaraType
public class BEnelCloudReducedTrafficModeHistoriesSyncJob extends BSimpleJob
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.history.job.BEnelCloudReducedTrafficModeHistoriesSyncJob(2979906276)1.0$ @*/
/* Generated Sat Apr 14 09:57:50 CEST 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudReducedTrafficModeHistoriesSyncJob.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    // empty constructor
    public BEnelCloudReducedTrafficModeHistoriesSyncJob()
    {
        // call superclass constructor
        super();
    }

    // main constructor
    public BEnelCloudReducedTrafficModeHistoriesSyncJob(BEnelCloudHistoryManager hManager, BEnelCloudHistoryDescriptor[] allDescriptors)
    {
        // call superclass constructor
        super();
        // set the reference of history manager
        this.hManager = hManager;
        // set the descriptors to sync
        this.allDescriptors = allDescriptors;
        // instantiate a new PostRequestManager based on the master one (timeout).
        this.postRequestsManager = new BEnelCloudPostRequestsManager();
        this.postRequestsManager.setRequestTimeout( hManager.getEnelCloudService().getEnelCloudPostRequestsManager().getRequestTimeout());
        // get the endpoint
        this.endPoint = hManager.getCloudLabHistoryEndpoint();
    }

////////////////////////////////////////////////////////////////
// Job Cycle
////////////////////////////////////////////////////////////////

    @Override
    public void run(Context cx) throws Exception
    {
        log().add(new JobLogItem(0, "Main Data Preparation job started for reduced traffic mode (RTM)"));

        // set the start time of the job
        BAbsTime startTime = BAbsTime.now();

        // array list with all requests
        ArrayList<EnelCloudHistoryRequest> requestsQueue = new ArrayList();

        // instantiate the first request
        EnelCloudHistoryRequest currentRequest = new EnelCloudHistoryRequest(hManager.getNumberOfRecordsPerRequest());

        // total number of new records
        int totNumberOfNewRecords = 0;

        // cycle all descriptors
        for (int i = 0; i < allDescriptors.length; i++)
        {
            // print the current history name
            log().add(new JobLogItem(0, "Preparing data for: " + allDescriptors[i].getHistoryId().toString()));

            // get the new data in JSON Array
           // JSONArray newData = allDescriptors[i].getNewDataForCloudInJSON();
            JSONArray newData = new JSONArray();

            if (newData.length() > 0)
            {
                // update total records value
                totNumberOfNewRecords += newData.length();

                // check if there is space for this data in the current request
                if (currentRequest.canAdd(newData))
                {
                    // put the data inside the current request
                    currentRequest.addValues(newData, i);
                } else // no more space in this request
                {
                    // add the full request inside the queue
                    requestsQueue.add(currentRequest);
                    // instantiate a new empty request
                    currentRequest = new EnelCloudHistoryRequest(hManager.getNumberOfRecordsPerRequest());
                    // put the data inside the new request
                    currentRequest.addValues(newData, i);
                }
            }
        }

        // update progress
        progress(50);

        // add the last request inside the queue
        requestsQueue.add(currentRequest);

        // get the number of requests
        int numberOfRequests = requestsQueue.size();

        // start the correct number of jobs based on requests qty.
        if (numberOfRequests > 0) // 0 - 300
        {
            // as default the end index is the number of requests
            int endIndex = numberOfRequests;

            // if is > 300 set the end to 300
            if (numberOfRequests > 300)
                endIndex = 299;

            // check if the job already exist, if not start it!
            if (hManager.historySyncjobRT1 == null)
            {
                hManager.historySyncjobRT1 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 0, endIndex, requestsQueue);
                BOrd jobOrd = hManager.historySyncjobRT1.submit(cx);
            } else // if one job was already instantiated re-submit only if the previous one isn't in running
            {
                if (!hManager.historySyncjobRT1.getJobState().isRunning()) // check if the previous is finished
                {
                    // start the new one
                    hManager.historySyncjobRT1 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 0, endIndex, requestsQueue);
                    BOrd jobOrd = hManager.historySyncjobRT1.submit(cx);
                }
            }
        }
        if (numberOfRequests > 300) // 0 - 300
        {
            // as default the end index is the number of requests
            int endIndex = numberOfRequests;

            // if is > 300 set the end to 300
            if (numberOfRequests > 600)
                endIndex = 599;

            // check if the job already exist, if not start it!
            if (hManager.historySyncjobRT2 == null)
            {
                hManager.historySyncjobRT2 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 300, endIndex, requestsQueue);
                BOrd jobOrd = hManager.historySyncjobRT2.submit(cx);
            } else // if one job was already instantiated re-submit only if the previous one isn't in running
            {
                if (!hManager.historySyncjobRT2.getJobState().isRunning()) // check if the previous is finished
                {
                    // start the new one
                    hManager.historySyncjobRT2 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 300, endIndex, requestsQueue);
                    BOrd jobOrd = hManager.historySyncjobRT2.submit(cx);
                }
            }
        }
        if (numberOfRequests > 600) // 0 - 300
        {
            // as default the end index is the number of requests
            int endIndex = numberOfRequests;

            // if is > 300 set the end to 300
            if (numberOfRequests > 900)
                endIndex = 899;

            // check if the job already exist, if not start it!
            if (hManager.historySyncjobRT3 == null)
            {
                hManager.historySyncjobRT3 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 600, endIndex, requestsQueue);
                BOrd jobOrd = hManager.historySyncjobRT3.submit(cx);
            } else // if one job was already instantiated re-submit only if the previous one isn't in running
            {
                if (!hManager.historySyncjobRT3.getJobState().isRunning()) // check if the previous is finished
                {
                    // start the new one
                    hManager.historySyncjobRT3 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 600, endIndex, requestsQueue);
                    BOrd jobOrd = hManager.historySyncjobRT3.submit(cx);
                }
            }
        }
        if (numberOfRequests > 900) // 0 - 300
        {
            // as default the end index is the number of requests
            int endIndex = numberOfRequests;

            // if is > 300 set the end to 300
            if (numberOfRequests > 1200)
                endIndex = 1199;

            // check if the job already exist, if not start it!
            if (hManager.historySyncjobRT4 == null)
            {
                hManager.historySyncjobRT4 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 900, endIndex, requestsQueue);
                BOrd jobOrd = hManager.historySyncjobRT4.submit(cx);
            } else // if one job was already instantiated re-submit only if the previous one isn't in running
            {
                if (!hManager.historySyncjobRT4.getJobState().isRunning()) // check if the previous is finished
                {
                    // start the new one
                    hManager.historySyncjobRT4 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 900, endIndex, requestsQueue);
                    BOrd jobOrd = hManager.historySyncjobRT4.submit(cx);
                }
            }
        }
        if (numberOfRequests > 1200) // 0 - 300
        {
            // as default the end index is the number of requests
            int endIndex = numberOfRequests;

            // if is > 300 set the end to 300
            if (numberOfRequests > 1500)
                endIndex = 1499;

            // check if the job already exist, if not start it!
            if (hManager.historySyncjobRT5 == null)
            {
                hManager.historySyncjobRT5 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 1200, endIndex, requestsQueue);
                BOrd jobOrd = hManager.historySyncjobRT5.submit(cx);
            } else // if one job was already instantiated re-submit only if the previous one isn't in running
            {
                if (!hManager.historySyncjobRT5.getJobState().isRunning()) // check if the previous is finished
                {
                    // start the new one
                    hManager.historySyncjobRT5 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 1200, endIndex, requestsQueue);
                    BOrd jobOrd = hManager.historySyncjobRT5.submit(cx);
                }
            }
        }
        if (numberOfRequests > 1500) // 0 - 300
        {
            // as default the end index is the number of requests
            int endIndex = numberOfRequests;

            // check if the job already exist, if not start it!
            if (hManager.historySyncjobRT6 == null)
            {
                hManager.historySyncjobRT6 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 1500, endIndex, requestsQueue);
                BOrd jobOrd = hManager.historySyncjobRT6.submit(cx);
            } else // if one job was already instantiated re-submit only if the previous one isn't in running
            {
                if (!hManager.historySyncjobRT6.getJobState().isRunning()) // check if the previous is finished
                {
                    // start the new one
                    hManager.historySyncjobRT6 = new BEnelCloudHistoriesSyncJobForRTM(hManager, allDescriptors, 1500, endIndex, requestsQueue);
                    BOrd jobOrd = hManager.historySyncjobRT6.submit(cx);
                }
            }
        }

        // calculate the execution time of the job
        BRelTime executionTime = startTime.delta(BAbsTime.now());
        // update progress
        progress(99);
        // print the final message with statistics
        log().add(new JobLogItem(0, "Main Data Preparation Job finished in: " + executionTime.toString()));
        log().add(new JobLogItem(0, "Number of Requests sent to jobs: " + numberOfRequests));
        log().add(new JobLogItem(0, "Number of new records: " + totNumberOfNewRecords));

    }

    ////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

    public static final Logger logger = Logger.getLogger("CloudLabReducedTrafficModeHistoriesSyncJob");
    private BEnelCloudHistoryManager hManager = null;
    private BEnelCloudHistoryDescriptor[] allDescriptors = null;
    private BEnelCloudPostRequestsManager postRequestsManager = null;
    private String endPoint = "";
}
