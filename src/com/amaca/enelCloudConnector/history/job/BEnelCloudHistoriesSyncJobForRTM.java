package com.amaca.enelCloudConnector.history.job;

import com.amaca.enelCloudConnector.history.BEnelCloudHistoryDescriptor;
import com.amaca.enelCloudConnector.history.BEnelCloudHistoryManager;
import com.amaca.enelCloudConnector.history.EnelCloudHistoryRequest;
import com.amaca.enelCloudConnector.network.BEnelCloudEnelPostRequestsManager;
import com.amaca.enelCloudConnector.network.BEnelCloudPostRequestsManager;

import javax.baja.job.BJobState;
import javax.baja.job.BSimpleJob;
import javax.baja.job.JobLogItem;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by Amaca on 17/03/2018.
 */
@NiagaraType
public class BEnelCloudHistoriesSyncJobForRTM extends BSimpleJob
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.history.job.BEnelCloudHistoriesSyncJobForRTM(2979906276)1.0$ @*/
/* Generated Tue Apr 17 21:04:25 CEST 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudHistoriesSyncJobForRTM.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    // empty constructor
    public BEnelCloudHistoriesSyncJobForRTM()
    {
        // call superclass constructor
        super();
    }

    // main constructor
    public BEnelCloudHistoriesSyncJobForRTM(BEnelCloudHistoryManager hManager, BEnelCloudHistoryDescriptor[] allDescriptors, int start, int stop, ArrayList<EnelCloudHistoryRequest> hrequests)
    {
        // call superclass constructor
        super();
        // set the reference of history manager
        this.hManager = hManager;
        // set the descriptors to sync
        this.allDescriptors = allDescriptors;
        //set the start and stop descriptor
        this.startRequest = start;
        this.stopRequest = stop;
        // instantiate a new PostRequestManager based on the master one (timeout).
        this.postRequestsManager = new BEnelCloudPostRequestsManager();
        this.postRequestsManager.setRequestTimeout( hManager.getEnelCloudService().getEnelCloudPostRequestsManager().getRequestTimeout());
        // instantiate a new EnelPostRequestManager based on the master one (timeout).
        this.enelPostRequestsManager = new BEnelCloudEnelPostRequestsManager();
        this.enelPostRequestsManager.setRequestTimeout( hManager.getEnelCloudService().getEnelCloudPostRequestsManager().getRequestTimeout());
        // get the endpoint
        this.endPoint = hManager.getCloudLabHistoryEndpoint();
        // get the enel endpoint and refresh the access token
       // if(hManager.getCloudLabService().getEnelCloudEnelCloud().getEnabled())
        //  this.endPointEnel = hManager.getCloudLabService().getEnelCloudEnelCloud().getEnelCloudUrl() + "?access_token=" + hManager.getCloudLabService().getEnelCloudEnelCloud().getAccessToken();
        // get the array list
        this.hrequests = hrequests;
    }

////////////////////////////////////////////////////////////////
// Job Cycle
////////////////////////////////////////////////////////////////

    @Override
    public void run(Context context) throws Exception
    {
      log().add(new JobLogItem(0, "Sync Job RTM started"));
      // counters
      int successCounter = 0;
      int failedCounter = 0;
      int skippedCounter = 0;

      // set the start time of the job
      BAbsTime startTime = BAbsTime.now();

      // calc the progressbar step and value
      double progressBarStep = (double)100 / (double)(stopRequest - startRequest);
      double progressBarValue = 0;

      // cycle all requests
      for (int i = startRequest; i < stopRequest; i++)
      {
        log().add(new JobLogItem(0, "Syncing request number: " + i));
        // get the new data formatted from the history
        String body = hrequests.get(i).getBodyRequest();
        // send to post manager the request and get the result code
        int result = 0;
        try
        {
          if (body.length() > 10) // check if the body is consistent
          {
            // dispatch the request to ENEL and DART portal
/*            if (hManager.getCloudLabService().getEnabledDart() && hManager.getCloudLabService().getEnelCloudEnelCloud().getEnabled()) //both enabled
            {
              int resultDart = postRequestsManager.sendPostRequest(endPoint, body);
              int resultEnel = enelPostRequestsManager.sendPostRequest(endPointEnel, body);

              // check the combinations
              if (resultDart == resultEnel)
                result = resultDart;
              else if (resultDart == 333 || resultEnel == 333)
                result = 333;
              else if (resultDart != 200)
                result = resultDart;
              else if (resultEnel != 200)
                result = resultEnel;
            }
            else if (hManager.getCloudLabService().getEnabledDart()) //only dart enabled
            {
              result = postRequestsManager.sendPostRequest(endPoint, body);
            }
            else if(hManager.getCloudLabService().getEnelCloudEnelCloud().getEnabled()) //only enel cloud enabled
            {
              result = enelPostRequestsManager.sendPostRequest(endPointEnel, body);
            }*/
            result = postRequestsManager.sendPostRequest(endPoint, body);
          }
          else // if not, set 333: no new data.
            result = 333;
        }
        catch (Exception e){};

        // if result code is 200, confirm data sent correctly to the cloud and update timestamps
        if (result == 200) {
          hrequests.get(i).confirmAll(allDescriptors);
          log().add(new JobLogItem(0, ">> Sync OK"));
          // increment success counter
          successCounter++;
        }
        else if (result == 333)  //no new data
        {
          log().add(new JobLogItem(0, ">> No new data"));
          // increment skipped counter
          skippedCounter++;
        }
        else // sync error
        {
          log().add(new JobLogItem(0, ">> Syncing FAILED, error code: " + result));
          // increment failed counter
          failedCounter++;
        }

        // step up the progress
        progressBarValue += progressBarStep;
        progress((int)progressBarValue);
      }

      // calculate the execution time of the job
      BRelTime executionTime = startTime.delta(BAbsTime.now());
      // print the final message with statistics
      log().add(new JobLogItem(0, "Sync Job finished in: " + executionTime.toString()));
      log().add(new JobLogItem(0, ">> Number of Sync OK: " + successCounter));
      log().add(new JobLogItem(0, ">> Number of Sync FAILED: " + failedCounter));
      log().add(new JobLogItem(0, ">> Number of Sync SKIPPED: " + skippedCounter));
      // complete the job.
      progress(100);
      this.complete(BJobState.success);
    }

  ////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

    public static final Logger logger = Logger.getLogger("CloudLabHistoriesSyncJobRTM");
    private BEnelCloudHistoryManager hManager = null;
    private BEnelCloudHistoryDescriptor[] allDescriptors = null;
    private BEnelCloudPostRequestsManager postRequestsManager = null;
    private BEnelCloudEnelPostRequestsManager enelPostRequestsManager = null;
    private String endPoint = "";
    private String endPointEnel = "";
    private int startRequest = 0;
    private int stopRequest = 0;
    private ArrayList<EnelCloudHistoryRequest> hrequests = null;
}
