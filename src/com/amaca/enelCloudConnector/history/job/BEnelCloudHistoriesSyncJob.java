package com.amaca.enelCloudConnector.history.job;

import com.amaca.enelCloudConnector.history.BEnelCloudHistoryDescriptor;
import com.amaca.enelCloudConnector.history.BEnelCloudHistoryManager;
import com.amaca.enelCloudConnector.network.BEnelCloudEnelPostRequestsManager;
import com.amaca.enelCloudConnector.network.BEnelCloudPostRequestsManager;
import com.amaca.enelCloudConnector.network.BAwsGatewayInterface;

import javax.baja.job.BSimpleJob;
import javax.baja.job.JobLogItem;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import java.util.logging.Logger;

/**
 * Created by Amaca on 12/03/2018.
 */
@NiagaraType
public class BEnelCloudHistoriesSyncJob extends BSimpleJob
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.history.job.BEnelCloudHistoriesSyncJob(2979906276)1.0$ @*/
/* Generated Mon Mar 12 17:25:31 CET 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudHistoriesSyncJob.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    // empty constructor
    public BEnelCloudHistoriesSyncJob()
    {
        // call superclass constructor
        super();
    }

    // main constructor
    public BEnelCloudHistoriesSyncJob(BEnelCloudHistoryManager hManager, BEnelCloudHistoryDescriptor[] allDescriptors, int start, int stop)
    {
        // call superclass constructor
        super();
        // set the reference of history manager
        this.hManager = hManager;
        // set the descriptors to sync
        this.allDescriptors = allDescriptors;
        //set the start and stop descriptor
        this.startDescriptor = start;
        this.stopDescriptor = stop;
        // instantiate a new PostRequestManager based on the master one (timeout).
        //this.postRequestsManager = new BEnelCloudPostRequestsManager();
        //this.postRequestsManager.setRequestTimeout( hManager.getCloudLabService().getEnelCloudPostRequestsManager().getRequestTimeout());
        // instantiate a new EnelPostRequestManager based on the master one (timeout).
        //this.enelPostRequestsManager = new BEnelCloudEnelPostRequestsManager();
        //this.enelPostRequestsManager.setRequestTimeout( hManager.getCloudLabService().getEnelCloudPostRequestsManager().getRequestTimeout());
        // get the endpoint
        //this.endPoint = hManager.getCloudLabHistoryEndpoint();

        //this.awsInterface = new BAwsGatewayInterface();
        this.awsInterface = hManager.getAwsInterface();
        awsInterface.setCertificateFile(hManager.getEnelCloudService().getCertificateFile());
        awsInterface.setPrivateKeyFile(hManager.getEnelCloudService().getPrivateKeyFile());
        awsInterface.setEndPoint(hManager.getEnelCloudService().getEnelCloudEndpoint());


        // get the enel endpoint and refresh the access token
        //if(hManager.getCloudLabService().getEnelCloudEnelCloud().getEnabled())
          //this.endPointEnel = hManager.getCloudLabService().getEnelCloudEnelCloud().getEnelCloudUrl() + "?access_token=" + hManager.getCloudLabService().getEnelCloudEnelCloud().getAccessToken();
    }

////////////////////////////////////////////////////////////////
// Job Cycle
////////////////////////////////////////////////////////////////

    @Override
    public void run(Context context) throws Exception
    {
      log().add(new JobLogItem(0, "Sync Job started"));
      // counters
      int successCounter = 0;
      int failedCounter = 0;
      int skippedCounter = 0;

      // set the start time of the job
      BAbsTime startTime = BAbsTime.now();

      // calc the progressbar step and value
      double progressBarStep = (double)100 / (double)(stopDescriptor-startDescriptor);
      double progressBarValue = 0;

      // cycle all descriptors
      for (int i = startDescriptor; i < stopDescriptor; i++)
      {
        log().add(new JobLogItem(0, "Syncing: " + allDescriptors[i].getHistoryId().toString()));
        // get the new data formatted from the history
        String body = allDescriptors[i].getNewDataForCloud();
        // send to post manager the request and get the result code
        int result = 0;
        try
        {
          if (body.length() > 10) // check if the body is consistent
          {
/*            else if (hManager.getCloudLabService().getEnabledDart()) //only dart enabled
            {*/
              System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@");
              System.out.println("BODY" + body);
              // set topic
              awsInterface.setTopic(hManager.getEnelCloudService().getEnvPrefix()+"/"+hManager.getEnelCloudService().getGatewayId()+"/"+allDescriptors[i].getDeviceType()+"/"+allDescriptors[i].getThingId()+ "/data");
              System.out.println("TOPIC: " + awsInterface.getTopic());
              // set payload
              awsInterface.setPayload(body);
              // publish
              System.out.println("START PUBLISH");
              BBoolean published = BBoolean.make(false);
              try {
                  published = awsInterface.doPublish();
              }catch (Exception e)
              {
                  e.printStackTrace();
              }
              System.out.println("END PUBLISH");
              //result = postRequestsManager.sendPostRequest(endPoint, body);
              if (published.getBoolean())
              {
                  System.out.println("PUBLISH OK");
                result = 200;
              }
              else
              {
                  System.out.println("PUBLISH FAILED");
                result = 0;
              }
              //

/*            }
            else if(hManager.getCloudLabService().getEnelCloudEnelCloud().getEnabled()) //only enel cloud enabled
            {
              result = enelPostRequestsManager.sendPostRequest(endPointEnel, body);
            }*/
          }
          else // if not, set 333: no new data.
            result = 333;
        }
        catch (Exception e){};

        // if result code is 200, confirm data sent correctly to the cloud and update timestamps
        if (result == 200) {
          allDescriptors[i].confirmLastRecordAndLastSync();
          log().add(new JobLogItem(0, ">> Sync OK"));
          // increment success counter
          successCounter++;
        }
        else if (result == 333)  //no new data
        {
          log().add(new JobLogItem(0, ">> No new data"));
          // increment skipped counter
          skippedCounter++;
        }
        else // sync error
        {
          log().add(new JobLogItem(0, ">> Syncing FAILED, error code: " + result));
          // increment failed counter
          failedCounter++;
        }

        // step up the progress
        progressBarValue += progressBarStep;
        progress((int)progressBarValue);
      }

      // calculate the execution time of the job
      BRelTime executionTime = startTime.delta(BAbsTime.now());
      // print the final message with statistics
      log().add(new JobLogItem(0, "Sync Job finished in: " + executionTime.toString()));
      log().add(new JobLogItem(0, ">> Number of Sync OK: " + successCounter));
      log().add(new JobLogItem(0, ">> Number of Sync FAILED: " + failedCounter));
      log().add(new JobLogItem(0, ">> Number of Sync SKIPPED: " + skippedCounter));
    }

  ////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

    public static final Logger logger = Logger.getLogger("EnelCloudHistoriesSyncJob");
    private BEnelCloudHistoryManager hManager = null;
    private BEnelCloudHistoryDescriptor[] allDescriptors = null;
    private BEnelCloudPostRequestsManager postRequestsManager = null;
    private BEnelCloudEnelPostRequestsManager enelPostRequestsManager = null;
    private BAwsGatewayInterface awsInterface = null;
    private String endPoint = "";
    private String endPointEnel = "";
    private int startDescriptor = 0;
    private int stopDescriptor = 0;
}
