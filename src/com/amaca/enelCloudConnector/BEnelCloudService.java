package com.amaca.enelCloudConnector;

import com.amaca.enelCloudConnector.alarming.BEnelCloudAlarmsManager;
import com.amaca.enelCloudConnector.history.BEnelCloudHistoryManager;
import com.amaca.enelCloudConnector.network.*;
import com.amaca.enelCloudConnector.plansdays.BEnelCloudPlansDaysManager;
import com.amaca.enelCloudConnector.scheduling.BEnelCloudSchedulingManager;
import com.amaca.enelCloudConnector.setpoint.BEnelCloudSetpointManager;
import com.tridium.json.JSONObject;
//import com.amaca.enelIotPlatformConnector.*;

import javax.baja.control.BEnumWritable;
import javax.baja.naming.BOrd;
import javax.baja.nre.annotations.*;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;

/**
 * Created by Amaca on 25/02/2018.
 */
@NiagaraProperty(
        name = "moduleVersion",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = ""
)
@NiagaraProperty(
        name = "enelCloudEndpoint",
        type = "baja:String",
        defaultValue = "ax3v62h9b8tp2.iot.eu-central-1.amazonaws.com"
)
@NiagaraProperty(
        name = "accessToken",
        type = "baja:String",
        defaultValue = ""
)
@NiagaraProperty(
        name = "EnelS3BucketInterface",
        type = "enelCloudConnector:S3BucketInterface",
        defaultValue = "new BS3BucketInterface()"
)
@NiagaraProperty(
        name = "certificateFile",
        type = "baja:Ord",
        defaultValue = "BOrd.NULL",
        facets =
                {
                        @Facet(name = "BFacets.TARGET_TYPE", value = "\"baja:IFile\"")
                }
)

@NiagaraProperty(
        name = "privateKeyFile",
        type = "baja:Ord",
        defaultValue = "BOrd.NULL",
        facets =
                {
                        @Facet(name = "BFacets.TARGET_TYPE", value = "\"baja:IFile\"")
                }
)
@NiagaraProperty(
        name = "envPrefix",
        type = "baja:String",
        defaultValue = ""
)
@NiagaraProperty(
        name = "gatewayId",
        type = "baja:String",
        defaultValue = ""
)
@NiagaraProperty(
        name = "installationId",
        type = "baja:String",
        defaultValue = ""
)
@NiagaraProperty(
        name = "gatewayStatus",
        type = "baja:String",
        defaultValue = "OFF"
)
@NiagaraProperty(
        name = "gatewayStatusPoint",
        type = "baja:Ord",
        defaultValue = "BOrd.NULL"
)
@NiagaraProperty(
        name = "serialNumber",
        type = "baja:String",
        defaultValue = "gw-tst"
)
@NiagaraProperty(
        name = "maker",
        type = "baja:String",
        defaultValue = "Enel"
)
@NiagaraProperty(
        name = "gatewayType",
        type = "baja:String",
        defaultValue = "iot_light_gateway"
)
@NiagaraProperty(
        name = "geolocalization",
        type = "baja:String",
        defaultValue = "45.1018799,7.6668177"
)
@NiagaraProperty(
        name = "registrationTopic",
        type = "String",
        defaultValue = "",
        flags = Flags.SUMMARY
)
@NiagaraProperty(
        name = "notifyTopic",
        type = "String",
        defaultValue = "",
        flags = Flags.SUMMARY
)
@NiagaraProperty(
        name = "notifyNextTopic",
        type = "String",
        defaultValue = "",
        flags = Flags.SUMMARY
)
@NiagaraProperty(
        name = "gatewayRegistrerd",
        type = "boolean",
        defaultValue = "false",
        flags = Flags.READONLY
)
@NiagaraProperty(
        name = "enelCloudPostRequestsManager",
        type = "enelCloudConnector:EnelCloudPostRequestsManager",
        flags = Flags.HIDDEN,
        defaultValue = "new BEnelCloudPostRequestsManager()"
)
@NiagaraProperty(
        name = "enelCloudHistoryManager",
        type = "enelCloudConnector:EnelCloudHistoryManager",
        defaultValue = "new BEnelCloudHistoryManager()"
)
@NiagaraProperty(
        name = "enelCloudSetpointManager",
        type = "enelCloudConnector:EnelCloudSetpointManager",
        defaultValue = "new BEnelCloudSetpointManager()"
)
@NiagaraProperty(
        name = "enelCloudSchedulingManager",
        type = "enelCloudConnector:EnelCloudSchedulingManager",
        defaultValue = "new BEnelCloudSchedulingManager()"
)
@NiagaraProperty(
        name = "enelCloudPlansDaysManager",
        type = "enelCloudConnector:EnelCloudPlansDaysManager",
        defaultValue = "new BEnelCloudPlansDaysManager()"
)
@NiagaraProperty(
        name = "enelCloudAlarmsManager",
        type = "enelCloudConnector:EnelCloudAlarmsManager",
        flags = Flags.HIDDEN,
        defaultValue = "new BEnelCloudAlarmsManager()"
)
@NiagaraProperty(
        name = "awsRegistrationInterface",
        type = "enelCloudConnector:AwsGatewayInterface",
        flags = Flags.HIDDEN,
        defaultValue = "new BAwsGatewayInterface()"
)
@NiagaraAction(
        name = "registerGateway"
)
@NiagaraAction(
        name = "unregisterGateway"
)
@NiagaraAction(
        name = "registerToTopics",
        flags = Flags.HIDDEN
)
@NiagaraAction(
        name = "createClientForHistory",
        flags = Flags.HIDDEN
)
@NiagaraAction(
        name = "createClientForThingCommands",
        flags = Flags.HIDDEN
)

@NiagaraType
public class BEnelCloudService extends BAbstractService
{

/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.BEnelCloudService(2430423342)1.0$ @*/
/* Generated Sun Jul 26 18:18:19 CEST 2020 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "moduleVersion"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code moduleVersion} property.
   * @see #getModuleVersion
   * @see #setModuleVersion
   */
  public static final Property moduleVersion = newProperty(Flags.READONLY, "", null);
  
  /**
   * Get the {@code moduleVersion} property.
   * @see #moduleVersion
   */
  public String getModuleVersion() { return getString(moduleVersion); }
  
  /**
   * Set the {@code moduleVersion} property.
   * @see #moduleVersion
   */
  public void setModuleVersion(String v) { setString(moduleVersion, v, null); }

////////////////////////////////////////////////////////////////
// Property "enelCloudEndpoint"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code enelCloudEndpoint} property.
   * @see #getEnelCloudEndpoint
   * @see #setEnelCloudEndpoint
   */
  public static final Property enelCloudEndpoint = newProperty(0, "ax3v62h9b8tp2.iot.eu-central-1.amazonaws.com", null);
  
  /**
   * Get the {@code enelCloudEndpoint} property.
   * @see #enelCloudEndpoint
   */
  public String getEnelCloudEndpoint() { return getString(enelCloudEndpoint); }
  
  /**
   * Set the {@code enelCloudEndpoint} property.
   * @see #enelCloudEndpoint
   */
  public void setEnelCloudEndpoint(String v) { setString(enelCloudEndpoint, v, null); }

////////////////////////////////////////////////////////////////
// Property "accessToken"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code accessToken} property.
   * @see #getAccessToken
   * @see #setAccessToken
   */
  public static final Property accessToken = newProperty(0, "", null);
  
  /**
   * Get the {@code accessToken} property.
   * @see #accessToken
   */
  public String getAccessToken() { return getString(accessToken); }
  
  /**
   * Set the {@code accessToken} property.
   * @see #accessToken
   */
  public void setAccessToken(String v) { setString(accessToken, v, null); }

////////////////////////////////////////////////////////////////
// Property "EnelS3BucketInterface"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code EnelS3BucketInterface} property.
   * @see #getEnelS3BucketInterface
   * @see #setEnelS3BucketInterface
   */
  public static final Property EnelS3BucketInterface = newProperty(0, new BS3BucketInterface(), null);
  
  /**
   * Get the {@code EnelS3BucketInterface} property.
   * @see #EnelS3BucketInterface
   */
  public BS3BucketInterface getEnelS3BucketInterface() { return (BS3BucketInterface)get(EnelS3BucketInterface); }
  
  /**
   * Set the {@code EnelS3BucketInterface} property.
   * @see #EnelS3BucketInterface
   */
  public void setEnelS3BucketInterface(BS3BucketInterface v) { set(EnelS3BucketInterface, v, null); }

////////////////////////////////////////////////////////////////
// Property "certificateFile"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code certificateFile} property.
   * @see #getCertificateFile
   * @see #setCertificateFile
   */
  public static final Property certificateFile = newProperty(0, BOrd.NULL, BFacets.make(BFacets.TARGET_TYPE, "baja:IFile"));
  
  /**
   * Get the {@code certificateFile} property.
   * @see #certificateFile
   */
  public BOrd getCertificateFile() { return (BOrd)get(certificateFile); }
  
  /**
   * Set the {@code certificateFile} property.
   * @see #certificateFile
   */
  public void setCertificateFile(BOrd v) { set(certificateFile, v, null); }

////////////////////////////////////////////////////////////////
// Property "privateKeyFile"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code privateKeyFile} property.
   * @see #getPrivateKeyFile
   * @see #setPrivateKeyFile
   */
  public static final Property privateKeyFile = newProperty(0, BOrd.NULL, BFacets.make(BFacets.TARGET_TYPE, "baja:IFile"));
  
  /**
   * Get the {@code privateKeyFile} property.
   * @see #privateKeyFile
   */
  public BOrd getPrivateKeyFile() { return (BOrd)get(privateKeyFile); }
  
  /**
   * Set the {@code privateKeyFile} property.
   * @see #privateKeyFile
   */
  public void setPrivateKeyFile(BOrd v) { set(privateKeyFile, v, null); }

////////////////////////////////////////////////////////////////
// Property "envPrefix"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code envPrefix} property.
   * @see #getEnvPrefix
   * @see #setEnvPrefix
   */
  public static final Property envPrefix = newProperty(0, "", null);
  
  /**
   * Get the {@code envPrefix} property.
   * @see #envPrefix
   */
  public String getEnvPrefix() { return getString(envPrefix); }
  
  /**
   * Set the {@code envPrefix} property.
   * @see #envPrefix
   */
  public void setEnvPrefix(String v) { setString(envPrefix, v, null); }

////////////////////////////////////////////////////////////////
// Property "gatewayId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code gatewayId} property.
   * @see #getGatewayId
   * @see #setGatewayId
   */
  public static final Property gatewayId = newProperty(0, "", null);
  
  /**
   * Get the {@code gatewayId} property.
   * @see #gatewayId
   */
  public String getGatewayId() { return getString(gatewayId); }
  
  /**
   * Set the {@code gatewayId} property.
   * @see #gatewayId
   */
  public void setGatewayId(String v) { setString(gatewayId, v, null); }

////////////////////////////////////////////////////////////////
// Property "installationId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code installationId} property.
   * @see #getInstallationId
   * @see #setInstallationId
   */
  public static final Property installationId = newProperty(0, "", null);
  
  /**
   * Get the {@code installationId} property.
   * @see #installationId
   */
  public String getInstallationId() { return getString(installationId); }
  
  /**
   * Set the {@code installationId} property.
   * @see #installationId
   */
  public void setInstallationId(String v) { setString(installationId, v, null); }

////////////////////////////////////////////////////////////////
// Property "gatewayStatus"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code gatewayStatus} property.
   * @see #getGatewayStatus
   * @see #setGatewayStatus
   */
  public static final Property gatewayStatus = newProperty(0, "OFF", null);
  
  /**
   * Get the {@code gatewayStatus} property.
   * @see #gatewayStatus
   */
  public String getGatewayStatus() { return getString(gatewayStatus); }
  
  /**
   * Set the {@code gatewayStatus} property.
   * @see #gatewayStatus
   */
  public void setGatewayStatus(String v) { setString(gatewayStatus, v, null); }

////////////////////////////////////////////////////////////////
// Property "gatewayStatusPoint"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code gatewayStatusPoint} property.
   * @see #getGatewayStatusPoint
   * @see #setGatewayStatusPoint
   */
  public static final Property gatewayStatusPoint = newProperty(0, BOrd.NULL, null);
  
  /**
   * Get the {@code gatewayStatusPoint} property.
   * @see #gatewayStatusPoint
   */
  public BOrd getGatewayStatusPoint() { return (BOrd)get(gatewayStatusPoint); }
  
  /**
   * Set the {@code gatewayStatusPoint} property.
   * @see #gatewayStatusPoint
   */
  public void setGatewayStatusPoint(BOrd v) { set(gatewayStatusPoint, v, null); }

////////////////////////////////////////////////////////////////
// Property "serialNumber"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code serialNumber} property.
   * @see #getSerialNumber
   * @see #setSerialNumber
   */
  public static final Property serialNumber = newProperty(0, "gw-tst", null);
  
  /**
   * Get the {@code serialNumber} property.
   * @see #serialNumber
   */
  public String getSerialNumber() { return getString(serialNumber); }
  
  /**
   * Set the {@code serialNumber} property.
   * @see #serialNumber
   */
  public void setSerialNumber(String v) { setString(serialNumber, v, null); }

////////////////////////////////////////////////////////////////
// Property "maker"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code maker} property.
   * @see #getMaker
   * @see #setMaker
   */
  public static final Property maker = newProperty(0, "Enel", null);
  
  /**
   * Get the {@code maker} property.
   * @see #maker
   */
  public String getMaker() { return getString(maker); }
  
  /**
   * Set the {@code maker} property.
   * @see #maker
   */
  public void setMaker(String v) { setString(maker, v, null); }

////////////////////////////////////////////////////////////////
// Property "gatewayType"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code gatewayType} property.
   * @see #getGatewayType
   * @see #setGatewayType
   */
  public static final Property gatewayType = newProperty(0, "iot_light_gateway", null);
  
  /**
   * Get the {@code gatewayType} property.
   * @see #gatewayType
   */
  public String getGatewayType() { return getString(gatewayType); }
  
  /**
   * Set the {@code gatewayType} property.
   * @see #gatewayType
   */
  public void setGatewayType(String v) { setString(gatewayType, v, null); }

////////////////////////////////////////////////////////////////
// Property "geolocalization"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code geolocalization} property.
   * @see #getGeolocalization
   * @see #setGeolocalization
   */
  public static final Property geolocalization = newProperty(0, "45.1018799,7.6668177", null);
  
  /**
   * Get the {@code geolocalization} property.
   * @see #geolocalization
   */
  public String getGeolocalization() { return getString(geolocalization); }
  
  /**
   * Set the {@code geolocalization} property.
   * @see #geolocalization
   */
  public void setGeolocalization(String v) { setString(geolocalization, v, null); }

////////////////////////////////////////////////////////////////
// Property "registrationTopic"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code registrationTopic} property.
   * @see #getRegistrationTopic
   * @see #setRegistrationTopic
   */
  public static final Property registrationTopic = newProperty(Flags.SUMMARY, "", null);
  
  /**
   * Get the {@code registrationTopic} property.
   * @see #registrationTopic
   */
  public String getRegistrationTopic() { return getString(registrationTopic); }
  
  /**
   * Set the {@code registrationTopic} property.
   * @see #registrationTopic
   */
  public void setRegistrationTopic(String v) { setString(registrationTopic, v, null); }

////////////////////////////////////////////////////////////////
// Property "notifyTopic"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code notifyTopic} property.
   * @see #getNotifyTopic
   * @see #setNotifyTopic
   */
  public static final Property notifyTopic = newProperty(Flags.SUMMARY, "", null);
  
  /**
   * Get the {@code notifyTopic} property.
   * @see #notifyTopic
   */
  public String getNotifyTopic() { return getString(notifyTopic); }
  
  /**
   * Set the {@code notifyTopic} property.
   * @see #notifyTopic
   */
  public void setNotifyTopic(String v) { setString(notifyTopic, v, null); }

////////////////////////////////////////////////////////////////
// Property "notifyNextTopic"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code notifyNextTopic} property.
   * @see #getNotifyNextTopic
   * @see #setNotifyNextTopic
   */
  public static final Property notifyNextTopic = newProperty(Flags.SUMMARY, "", null);
  
  /**
   * Get the {@code notifyNextTopic} property.
   * @see #notifyNextTopic
   */
  public String getNotifyNextTopic() { return getString(notifyNextTopic); }
  
  /**
   * Set the {@code notifyNextTopic} property.
   * @see #notifyNextTopic
   */
  public void setNotifyNextTopic(String v) { setString(notifyNextTopic, v, null); }

////////////////////////////////////////////////////////////////
// Property "gatewayRegistrerd"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code gatewayRegistrerd} property.
   * @see #getGatewayRegistrerd
   * @see #setGatewayRegistrerd
   */
  public static final Property gatewayRegistrerd = newProperty(Flags.READONLY, false, null);
  
  /**
   * Get the {@code gatewayRegistrerd} property.
   * @see #gatewayRegistrerd
   */
  public boolean getGatewayRegistrerd() { return getBoolean(gatewayRegistrerd); }
  
  /**
   * Set the {@code gatewayRegistrerd} property.
   * @see #gatewayRegistrerd
   */
  public void setGatewayRegistrerd(boolean v) { setBoolean(gatewayRegistrerd, v, null); }

////////////////////////////////////////////////////////////////
// Property "enelCloudPostRequestsManager"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code enelCloudPostRequestsManager} property.
   * @see #getEnelCloudPostRequestsManager
   * @see #setEnelCloudPostRequestsManager
   */
  public static final Property enelCloudPostRequestsManager = newProperty(Flags.HIDDEN, new BEnelCloudPostRequestsManager(), null);
  
  /**
   * Get the {@code enelCloudPostRequestsManager} property.
   * @see #enelCloudPostRequestsManager
   */
  public BEnelCloudPostRequestsManager getEnelCloudPostRequestsManager() { return (BEnelCloudPostRequestsManager)get(enelCloudPostRequestsManager); }
  
  /**
   * Set the {@code enelCloudPostRequestsManager} property.
   * @see #enelCloudPostRequestsManager
   */
  public void setEnelCloudPostRequestsManager(BEnelCloudPostRequestsManager v) { set(enelCloudPostRequestsManager, v, null); }

////////////////////////////////////////////////////////////////
// Property "enelCloudHistoryManager"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code enelCloudHistoryManager} property.
   * @see #getEnelCloudHistoryManager
   * @see #setEnelCloudHistoryManager
   */
  public static final Property enelCloudHistoryManager = newProperty(0, new BEnelCloudHistoryManager(), null);
  
  /**
   * Get the {@code enelCloudHistoryManager} property.
   * @see #enelCloudHistoryManager
   */
  public BEnelCloudHistoryManager getEnelCloudHistoryManager() { return (BEnelCloudHistoryManager)get(enelCloudHistoryManager); }
  
  /**
   * Set the {@code enelCloudHistoryManager} property.
   * @see #enelCloudHistoryManager
   */
  public void setEnelCloudHistoryManager(BEnelCloudHistoryManager v) { set(enelCloudHistoryManager, v, null); }

////////////////////////////////////////////////////////////////
// Property "enelCloudSetpointManager"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code enelCloudSetpointManager} property.
   * @see #getEnelCloudSetpointManager
   * @see #setEnelCloudSetpointManager
   */
  public static final Property enelCloudSetpointManager = newProperty(0, new BEnelCloudSetpointManager(), null);
  
  /**
   * Get the {@code enelCloudSetpointManager} property.
   * @see #enelCloudSetpointManager
   */
  public BEnelCloudSetpointManager getEnelCloudSetpointManager() { return (BEnelCloudSetpointManager)get(enelCloudSetpointManager); }
  
  /**
   * Set the {@code enelCloudSetpointManager} property.
   * @see #enelCloudSetpointManager
   */
  public void setEnelCloudSetpointManager(BEnelCloudSetpointManager v) { set(enelCloudSetpointManager, v, null); }

////////////////////////////////////////////////////////////////
// Property "enelCloudSchedulingManager"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code enelCloudSchedulingManager} property.
   * @see #getEnelCloudSchedulingManager
   * @see #setEnelCloudSchedulingManager
   */
  public static final Property enelCloudSchedulingManager = newProperty(0, new BEnelCloudSchedulingManager(), null);
  
  /**
   * Get the {@code enelCloudSchedulingManager} property.
   * @see #enelCloudSchedulingManager
   */
  public BEnelCloudSchedulingManager getEnelCloudSchedulingManager() { return (BEnelCloudSchedulingManager)get(enelCloudSchedulingManager); }
  
  /**
   * Set the {@code enelCloudSchedulingManager} property.
   * @see #enelCloudSchedulingManager
   */
  public void setEnelCloudSchedulingManager(BEnelCloudSchedulingManager v) { set(enelCloudSchedulingManager, v, null); }

////////////////////////////////////////////////////////////////
// Property "enelCloudPlansDaysManager"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code enelCloudPlansDaysManager} property.
   * @see #getEnelCloudPlansDaysManager
   * @see #setEnelCloudPlansDaysManager
   */
  public static final Property enelCloudPlansDaysManager = newProperty(0, new BEnelCloudPlansDaysManager(), null);
  
  /**
   * Get the {@code enelCloudPlansDaysManager} property.
   * @see #enelCloudPlansDaysManager
   */
  public BEnelCloudPlansDaysManager getEnelCloudPlansDaysManager() { return (BEnelCloudPlansDaysManager)get(enelCloudPlansDaysManager); }
  
  /**
   * Set the {@code enelCloudPlansDaysManager} property.
   * @see #enelCloudPlansDaysManager
   */
  public void setEnelCloudPlansDaysManager(BEnelCloudPlansDaysManager v) { set(enelCloudPlansDaysManager, v, null); }

////////////////////////////////////////////////////////////////
// Property "enelCloudAlarmsManager"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code enelCloudAlarmsManager} property.
   * @see #getEnelCloudAlarmsManager
   * @see #setEnelCloudAlarmsManager
   */
  public static final Property enelCloudAlarmsManager = newProperty(Flags.HIDDEN, new BEnelCloudAlarmsManager(), null);
  
  /**
   * Get the {@code enelCloudAlarmsManager} property.
   * @see #enelCloudAlarmsManager
   */
  public BEnelCloudAlarmsManager getEnelCloudAlarmsManager() { return (BEnelCloudAlarmsManager)get(enelCloudAlarmsManager); }
  
  /**
   * Set the {@code enelCloudAlarmsManager} property.
   * @see #enelCloudAlarmsManager
   */
  public void setEnelCloudAlarmsManager(BEnelCloudAlarmsManager v) { set(enelCloudAlarmsManager, v, null); }

////////////////////////////////////////////////////////////////
// Property "awsRegistrationInterface"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code awsRegistrationInterface} property.
   * @see #getAwsRegistrationInterface
   * @see #setAwsRegistrationInterface
   */
  public static final Property awsRegistrationInterface = newProperty(Flags.HIDDEN, new BAwsGatewayInterface(), null);
  
  /**
   * Get the {@code awsRegistrationInterface} property.
   * @see #awsRegistrationInterface
   */
  public BAwsGatewayInterface getAwsRegistrationInterface() { return (BAwsGatewayInterface)get(awsRegistrationInterface); }
  
  /**
   * Set the {@code awsRegistrationInterface} property.
   * @see #awsRegistrationInterface
   */
  public void setAwsRegistrationInterface(BAwsGatewayInterface v) { set(awsRegistrationInterface, v, null); }

////////////////////////////////////////////////////////////////
// Action "registerGateway"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code registerGateway} action.
   * @see #registerGateway()
   */
  public static final Action registerGateway = newAction(0, null);
  
  /**
   * Invoke the {@code registerGateway} action.
   * @see #registerGateway
   */
  public void registerGateway() { invoke(registerGateway, null, null); }

////////////////////////////////////////////////////////////////
// Action "unregisterGateway"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code unregisterGateway} action.
   * @see #unregisterGateway()
   */
  public static final Action unregisterGateway = newAction(0, null);
  
  /**
   * Invoke the {@code unregisterGateway} action.
   * @see #unregisterGateway
   */
  public void unregisterGateway() { invoke(unregisterGateway, null, null); }

////////////////////////////////////////////////////////////////
// Action "registerToTopics"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code registerToTopics} action.
   * @see #registerToTopics()
   */
  public static final Action registerToTopics = newAction(Flags.HIDDEN, null);
  
  /**
   * Invoke the {@code registerToTopics} action.
   * @see #registerToTopics
   */
  public void registerToTopics() { invoke(registerToTopics, null, null); }

////////////////////////////////////////////////////////////////
// Action "createClientForHistory"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code createClientForHistory} action.
   * @see #createClientForHistory()
   */
  public static final Action createClientForHistory = newAction(Flags.HIDDEN, null);
  
  /**
   * Invoke the {@code createClientForHistory} action.
   * @see #createClientForHistory
   */
  public void createClientForHistory() { invoke(createClientForHistory, null, null); }

////////////////////////////////////////////////////////////////
// Action "createClientForThingCommands"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code createClientForThingCommands} action.
   * @see #createClientForThingCommands()
   */
  public static final Action createClientForThingCommands = newAction(Flags.HIDDEN, null);
  
  /**
   * Invoke the {@code createClientForThingCommands} action.
   * @see #createClientForThingCommands
   */
  public void createClientForThingCommands() { invoke(createClientForThingCommands, null, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudService.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    // empty constructor
    public BEnelCloudService()
    {
        super();
    }

////////////////////////////////////////////////////////////////
// Action
////////////////////////////////////////////////////////////////

    public void doRegisterGateway()
    {
        // generate registration payload
        JSONObject payload = new JSONObject();
        payload.put("version", "1.0");
        payload.put("command", "register-gateway");
        payload.put("gateway_id", getGatewayId());

        JSONObject param = new JSONObject();
        param.put("geolocalization", getGeolocalization());
        param.put("management_mode", "jobs");
        param.put("environment_prefix", getEnvPrefix());
        param.put("serial_number", getSerialNumber());
        param.put("maker", getMaker());
        param.put("device_type", getGatewayType());

        payload.put("parameters", param);

        // set the whole aws component dedicated to the gateway registration
        getAwsRegistrationInterface().setPrivateKeyFile(getPrivateKeyFile());
        getAwsRegistrationInterface().setCertificateFile(getCertificateFile());
        getAwsRegistrationInterface().setEndPoint(getEnelCloudEndpoint());
        //getAwsRegistrationInterface().setPayload(payload.toString());
        getAwsRegistrationInterface().setRegistrationPayload(payload.toString());
        getAwsRegistrationInterface().setRegistrationTopic(getRegistrationTopic());
        getAwsRegistrationInterface().setNotifyNextTopic(getNotifyNextTopic());
        getAwsRegistrationInterface().setNotifyTopic(getNotifyTopic());

        // setup mqtt client
        getAwsRegistrationInterface().doSetupMqttClient();

        // register
        BBoolean registrationResult = getAwsRegistrationInterface().doRegistration();
        setGatewayRegistrerd(registrationResult.getBoolean());

      if (getGatewayRegistrerd())
        getEnelCloudHistoryManager().setEnabled(true);

        // start timers for other registrations
      if (getGatewayRegistrerd())
        startMqttRegistration = Clock.schedule(this, BRelTime.makeSeconds(15), registerToTopics, null);

      if (getGatewayRegistrerd())
        startMqttRegistration2 = Clock.schedule(this, BRelTime.makeSeconds(35), createClientForHistory, null);

      if (getGatewayRegistrerd())
        startMqttRegistration3 = Clock.schedule(this, BRelTime.makeSeconds(55), createClientForThingCommands, null);


      //set also the one related to histories
        //getEnelCloudHistoryManager().getAwsInterface().doSetupMqttClient();
    }

    public void doRegisterToTopics()
    {
      getEnelCloudHistoryManager().getAwsThingInterface().setClientId(getEnvPrefix()+"_"+getGatewayId()+"_1");

      try{
        getEnelCloudHistoryManager().getAwsThingInterface().doSetupMqttClient();
      }catch (Exception e)
      {
        getEnelCloudHistoryManager().getAwsThingInterface().doSetupMqttClient();
      }
      startMqttRegistration.cancel();
      startMqttRegistration = null;
    }


  public void doCreateClientForHistory()
{
  getEnelCloudHistoryManager().getAwsInterface().setClientId(getEnvPrefix()+"_"+getGatewayId());
  getEnelCloudHistoryManager().getAwsInterface().doSetupMqttClient();
  startMqttRegistration2.cancel();
  startMqttRegistration2 = null;
}

  public void doCreateClientForThingCommands()
  {
    getEnelCloudHistoryManager().getAwsThingCommandsInterface().setClientId(getEnvPrefix()+"_"+getGatewayId()+"_2");
    getEnelCloudHistoryManager().getAwsThingCommandsInterface().doSetupMqttClient();
    startMqttRegistration3.cancel();
    startMqttRegistration3 = null;
  }

  public void doUnregisterGateway()
  {
    /*
    // generate registration payload
    JSONObject payload = new JSONObject();
    payload.put("version", "1.0");
    payload.put("command", "register-gateway");
    payload.put("gateway_id", getGatewayId());

    JSONObject param = new JSONObject();
    param.put("geolocalization", getGeolocalization());
    param.put("management_mode", "jobs");
    param.put("environment_prefix", getEnvPrefix());
    param.put("serial_number", getSerialNumber());
    param.put("maker", getMaker());
    param.put("device_type", getGatewayType());

    payload.put("parameters", param);

    // set the whole aws component dedicated to the gateway registration
    getAwsRegistrationInterface().setPrivateKeyFile(getPrivateKeyFile());
    getAwsRegistrationInterface().setCertificateFile(getCertificateFile());
    //getAwsRegistrationInterface().setPayload(payload.toString());
    getAwsRegistrationInterface().setRegistrationPayload(payload.toString());
    getAwsRegistrationInterface().setRegistrationTopic(getRegistrationTopic());
    getAwsRegistrationInterface().setNotifyNextTopic(getNotifyNextTopic());
    getAwsRegistrationInterface().setNotifyTopic(getNotifyTopic());

    // setup mqtt client
    getAwsRegistrationInterface().doSetupMqttClient();

    // register
    BBoolean registrationResult = getAwsRegistrationInterface().doRegistration();*/
    setGatewayRegistrerd(false);
    getEnelCloudHistoryManager().setEnabled(false);

    //set also the one related to histories
    //getEnelCloudHistoryManager().getAwsInterface().doSetupMqttClient();
  }

  ////////////////////////////////////////////////////////////////
// Service life-cycle
////////////////////////////////////////////////////////////////

    // get service type for service registration
    public Type[] getServiceTypes()
    {
        return serviceTypes;
    }
    private static Type[] serviceTypes = new Type[] { TYPE };


  @Override
  public void stationStarted() throws Exception {
    super.stationStarted();

    System.out.println("***debug: Start loading registrations");

    if (getGatewayRegistrerd())
      startMqttRegistration = Clock.schedule(this, BRelTime.makeSeconds(40), registerToTopics, null);

    if (getGatewayRegistrerd())
      startMqttRegistration2 = Clock.schedule(this, BRelTime.makeSeconds(70), createClientForHistory, null);

    if (getGatewayRegistrerd())
      startMqttRegistration3 = Clock.schedule(this, BRelTime.makeSeconds(110), createClientForThingCommands, null);

  }

  @Override
  public void descendantsStarted() throws Exception
  {
    // call the superclass
    super.descendantsStarted();
    // set the cloudlab instance into history manager
    getEnelCloudHistoryManager().setEnelCloudService(this);
    // disable all the services not related to histories
    getEnelCloudSetpointManager().setEnabled(true);
    getEnelCloudSchedulingManager().setEnabled(true);
    getEnelCloudPlansDaysManager().setEnabled(true);
    getEnelCloudAlarmsManager().setEnabled(false);

    setModuleVersion("1.2.0.4"); //TODO: version change

  }

  @Override
  public void changed(Property property, Context context)
  {
    super.changed(property, context);

    // check if we are in a running state
    if (!isRunning())return;
    if (!Sys.atSteadyState())return;

    // service disabled -> stop all sub-services
    if (property == enabled)
      if (!getEnabled())
      {
        getEnelCloudHistoryManager().setEnabled(false);
        getEnelCloudSetpointManager().setEnabled(false);
        getEnelCloudSchedulingManager().setEnabled(false);
        getEnelCloudPlansDaysManager().setEnabled(false);
        getEnelCloudAlarmsManager().setEnabled(false);
      }

    if (property  == notifyNextTopic || property == certificateFile || property == privateKeyFile || property == enelCloudEndpoint)
    {
      // aws interface for publish histories
      getEnelCloudHistoryManager().getAwsInterface().setCertificateFile(getCertificateFile());
      getEnelCloudHistoryManager().getAwsInterface().setPrivateKeyFile(getPrivateKeyFile());
      getEnelCloudHistoryManager().getAwsInterface().setEndPoint(getEnelCloudEndpoint());
      //getEnelCloudHistoryManager().getAwsInterface().setNotifyNextTopic(getNotifyNextTopic());
      //getEnelCloudHistoryManager().getAwsInterface().setNotifyTopic(getNotifyTopic());
      getEnelCloudHistoryManager().getAwsInterface().setRegistrationTopic(getRegistrationTopic());
     // if (getGatewayRegistrerd())
     //   getEnelCloudHistoryManager().getAwsInterface().doSetupMqttClient();

      //aws interface for register/activate things
      getEnelCloudHistoryManager().getAwsThingInterface().setCertificateFile(getCertificateFile());
      getEnelCloudHistoryManager().getAwsThingInterface().setPrivateKeyFile(getPrivateKeyFile());
      getEnelCloudHistoryManager().getAwsThingInterface().setEndPoint(getEnelCloudEndpoint());
      getEnelCloudHistoryManager().getAwsThingInterface().setNotifyTopic(getNotifyTopic());
      getEnelCloudHistoryManager().getAwsThingInterface().setNotifyNextTopic(getNotifyNextTopic());
      getEnelCloudHistoryManager().getAwsThingInterface().setRegistrationTopic(getRegistrationTopic());

      //aws interface for gateway commands //<environment_prefix>/<gateway_id>/+/command/inbound
      getEnelCloudHistoryManager().getAwsThingCommandsInterface().setCertificateFile(getCertificateFile());
      getEnelCloudHistoryManager().getAwsThingCommandsInterface().setPrivateKeyFile(getPrivateKeyFile());
      getEnelCloudHistoryManager().getAwsThingCommandsInterface().setEndPoint(getEnelCloudEndpoint());
      getEnelCloudHistoryManager().getAwsThingCommandsInterface().setNotifyTopic(getEnvPrefix()+"/"+getGatewayId()+"/+/command/inbound");
      getEnelCloudHistoryManager().getAwsThingCommandsInterface().setNotifyNextTopic(getEnvPrefix()+"/"+getGatewayId()+"/+/command/inbound");
      getEnelCloudHistoryManager().getAwsThingCommandsInterface().setRegistrationTopic(getRegistrationTopic());

     // if (getGatewayRegistrerd())
      //  getEnelCloudHistoryManager().getAwsThingInterface().doSetupMqttClient();

      if (getGatewayRegistrerd())
        startMqttRegistration = Clock.schedule(this, BRelTime.makeSeconds(15), registerToTopics, null);

      if (getGatewayRegistrerd())
        startMqttRegistration2 = Clock.schedule(this, BRelTime.makeSeconds(35), createClientForHistory, null);

      if (getGatewayRegistrerd())
        startMqttRegistration3 = Clock.schedule(this, BRelTime.makeSeconds(55), createClientForThingCommands, null);

    }

  }

  ////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

  @Override
  public BIcon getIcon() {
    return BIcon.std("cloud.png");
  }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

  private Clock.Ticket startMqttRegistration = null;
  private Clock.Ticket startMqttRegistration2 = null;
  private Clock.Ticket startMqttRegistration3 = null;
  /*


  compile "Amaca:enelIotPlatformConnector-rt"
   */
}
