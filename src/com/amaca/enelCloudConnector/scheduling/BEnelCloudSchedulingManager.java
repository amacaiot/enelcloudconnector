package com.amaca.enelCloudConnector.scheduling;

import com.amaca.enelCloudConnector.BEnelCloudService;
import com.amaca.enelCloudConnector.point.BEnelCloudPointManager;
import com.amaca.enelCloudConnector.scheduling.datastructures.BEnelCloudProfile;
import com.amaca.enelCloudConnector.scheduling.datastructures.BEnelCloudTask;
import com.amaca.enelCloudConnector.utils.EnelCloudUtils;
import com.tridium.json.JSONArray;
import com.tridium.json.JSONObject;

import javax.baja.collection.BITable;
import javax.baja.collection.Column;
import javax.baja.collection.ColumnList;
import javax.baja.collection.TableCursor;
import javax.baja.control.BControlPoint;
import javax.baja.driver.BDevice;
import javax.baja.driver.BDeviceExt;
import javax.baja.driver.point.BPointDeviceExt;
import javax.baja.naming.BOrd;
import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.schedule.*;
import javax.baja.sys.*;
import javax.baja.util.BFolder;
import javax.baja.web.BWebServlet;
import javax.baja.web.WebOp;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Amaca on 21/07/2018.
 */

@NiagaraProperty(
        name = "servletName",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "niagarasched"
)
@NiagaraProperty(
        name = "ProfilesContainer",
        type = "BFolder",
        defaultValue = "new BFolder()"
)
@NiagaraProperty(
        name = "TasksContainer",
        type = "BFolder",
        defaultValue = "new BFolder()"
)
@NiagaraProperty(
        name = "GeneratedSchedulersContainer",
        type = "BFolder",
        defaultValue = "new BFolder()"
)

@NiagaraType
public class BEnelCloudSchedulingManager extends BWebServlet
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.scheduling.BEnelCloudSchedulingManager(3257100580)1.0$ @*/
/* Generated Sun Jul 22 11:59:30 CEST 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "servletName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code servletName} property.
   * @see #getServletName
   * @see #setServletName
   */
  public static final Property servletName = newProperty(Flags.READONLY, "niagarasched", null);
  
  /**
   * Get the {@code servletName} property.
   * @see #servletName
   */
  public String getServletName() { return getString(servletName); }
  
  /**
   * Set the {@code servletName} property.
   * @see #servletName
   */
  public void setServletName(String v) { setString(servletName, v, null); }

////////////////////////////////////////////////////////////////
// Property "ProfilesContainer"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code ProfilesContainer} property.
   * @see #getProfilesContainer
   * @see #setProfilesContainer
   */
  public static final Property ProfilesContainer = newProperty(0, new BFolder(), null);
  
  /**
   * Get the {@code ProfilesContainer} property.
   * @see #ProfilesContainer
   */
  public BFolder getProfilesContainer() { return (BFolder)get(ProfilesContainer); }
  
  /**
   * Set the {@code ProfilesContainer} property.
   * @see #ProfilesContainer
   */
  public void setProfilesContainer(BFolder v) { set(ProfilesContainer, v, null); }

////////////////////////////////////////////////////////////////
// Property "TasksContainer"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code TasksContainer} property.
   * @see #getTasksContainer
   * @see #setTasksContainer
   */
  public static final Property TasksContainer = newProperty(0, new BFolder(), null);
  
  /**
   * Get the {@code TasksContainer} property.
   * @see #TasksContainer
   */
  public BFolder getTasksContainer() { return (BFolder)get(TasksContainer); }
  
  /**
   * Set the {@code TasksContainer} property.
   * @see #TasksContainer
   */
  public void setTasksContainer(BFolder v) { set(TasksContainer, v, null); }

////////////////////////////////////////////////////////////////
// Property "GeneratedSchedulersContainer"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code GeneratedSchedulersContainer} property.
   * @see #getGeneratedSchedulersContainer
   * @see #setGeneratedSchedulersContainer
   */
  public static final Property GeneratedSchedulersContainer = newProperty(0, new BFolder(), null);
  
  /**
   * Get the {@code GeneratedSchedulersContainer} property.
   * @see #GeneratedSchedulersContainer
   */
  public BFolder getGeneratedSchedulersContainer() { return (BFolder)get(GeneratedSchedulersContainer); }
  
  /**
   * Set the {@code GeneratedSchedulersContainer} property.
   * @see #GeneratedSchedulersContainer
   */
  public void setGeneratedSchedulersContainer(BFolder v) { set(GeneratedSchedulersContainer, v, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudSchedulingManager.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// SET PROFILE-TASK-CALENDARMONTH
////////////////////////////////////////////////////////////////

    public boolean schedulingSetter(String deviceCode, String dataPointName, String commands, JSONObject dataObj)
    {
        try
        {
            // load the main service
            loadService();

                // get the data from url
                //es. installations/:installationId/devices/code/:deviceCode/datapoint/:dataPointName/commands/profile/

                // select the right function
                if (commands.equals("profile")) // token function
                {

                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {

                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }

                    // check if the point exist, if not, BAD REQUEST
                    if (findRealPoint(deviceCode, dataPointName) == null)
                    {
                        System.out.println("Data point not found!");
                        return false;
                     }
                    else
                    {

                        // call the create/edit profile method
                        int resultOp = addEditProfile(deviceCode, dataPointName, dataObj);

                        // parse the result code
                        if (resultOp == 0)
                            System.out.println("New profile created");
                        else if (resultOp == 1)
                            System.out.println("Existing profile replaced");
                        else
                        {
                            System.out.println("Error in creating the profile: " + resultOp);
                            return false;
                        }
                    }

                }
                else if (commands.equals("task")) // token function
                {


                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }

                    // check if the point exist, if not, BAD REQUEST
                    if (findRealPoint(deviceCode, dataPointName) == null)
                    {
                        return false;
                    }
                    else
                    {
                        // call the create task method
                        int resultOp = addEditTask(deviceCode, dataPointName, dataObj);

                        // parse the result code
                        if (resultOp == 0)
                            System.out.println("New task created");
                        else if (resultOp == 1)
                            System.out.println("Existing task replaced");
                        else
                        {
                            return false;
                        }
                    }
                }
                else if (commands.equals("calendarMonth")) // token function
                {

                    // decode the string
                    String monthString = dataObj.getString("month");
                    int month = BInteger.make(monthString.substring(4)).getInt();
                    int year = BInteger.make(monthString.substring(0,4)).getInt();
                    // get the right schedule
                    BAbstractSchedule schedule = (BAbstractSchedule)getGeneratedSchedulersContainer().get("Schedule_" + deviceCode + "_" + dataPointName);
                    // set the start date
                    BMonth startMonth = BMonth.make(month-1);
                    BAbsTime selectedMonth = BAbsTime.make(year,startMonth, 1,1,1);

                    // create the response array of profiles
                    JSONArray monthProfiles = new JSONArray();

                    // all days in the month
                    while (selectedMonth.getMonth().equals(startMonth))
                    {
                        // get the source name of the output for the specified day
                        BAbstractSchedule src = ((BWeeklySchedule)schedule).getOutputSource(selectedMonth);
                        String currentOutput = ((BWeeklySchedule) schedule).getSummary(src);

                        // put -1, no output set.
                        if (currentOutput.equals("Default Output"))
                        {
                            // put value inside the array
                            monthProfiles.put("-1");
                        }
                        else // search for the task name
                        {
                            StringTokenizer tokenizerSch = new StringTokenizer(currentOutput, "_");
                            //skip first
                            tokenizerSch.nextToken();
                            // get the right task
                            String taskId = "Task_" + deviceCode + "_" + dataPointName + "_" + tokenizerSch.nextToken() + "";
                            BEnelCloudTask selectedTask = (BEnelCloudTask)getTasksContainer().get(taskId);
                            // put value inside the array
                            monthProfiles.put(selectedTask.getProfileId());
                        }

                        // increment day
                        selectedMonth = selectedMonth.nextDay();
                    }

                    // create the response
                    JSONObject result = new JSONObject();
                    result.put("retval", true);
                    JSONObject dataObjRes = new JSONObject();
                    dataObjRes.put("monthProfiles", monthProfiles);
                    result.put("dataObject", dataObjRes);
                    result.put("message", "done");

                    // send the response
                    System.out.println(result.toString());
                }
                else
                {
                    return false;
                }
                return  true;

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    ////////////////////////////////////////////////////////////////
// GET PROFILE-TASK-CALENDARMONTH
////////////////////////////////////////////////////////////////

    public String schedulingGetter(String deviceCode, String dataPointName, String commands, String paramId)
    {
        try
        {
            // load the main service
            loadService();

                // select the right function
                if (commands.equals("profile")) // token function
                {
                    // String for profileId or other parameters
                    String profileId = paramId;


                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }

                    // if profileId it's null, get all tasks for this point
                    if (profileId == null)
                    {
                        // searcj all tasks for this datapoint
                        BEnelCloudProfile[] profiles = searchProfilesFromDevicePoint(deviceCode, dataPointName);

                        // check if there are at least one profile
                        if (profiles.length > 0)
                        {
                            // create a json array
                            JSONArray profileArray = new JSONArray();
                            // and get all tasks
                            for (int i = 0; i < profiles.length; i++)
                                profileArray.put(profiles[i].getJSONObject());

                            // make the result string
                            String result = "{" +
                                    "\"retval\":\"" + true + "\"," +
                                    "\"dataObject\":{" + profileArray.toString() +
                                    "}," +
                                    "\"message\":\"" + "done" + "\"" +
                                    "}";

                            // write the result
                            System.out.println(result);
                            return result;
                        }
                        else //if not, error, bad request
                        {
                            return "";
                         }
                    }
                    else
                    {
                        String uniqueProfileId = "Profile_" + deviceCode + "_" + dataPointName + "_" + profileId;

                        // call the create/edit profile method
                        BEnelCloudProfile foundProfile = searchExistingProfile(uniqueProfileId);
                        // parse the result code
                        if (foundProfile == null)
                        {
                            return "";
                         }
                        else
                        {
                            return foundProfile.getJSONDescriptor();
                        }
                    }
                }
                else if (commands.equals("task")) // token function
                {

                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }

                    // String for taskId or other parameters
                    String taskId = paramId;

                    // if taskId it's null, get all tasks for this point
                    if (taskId == null)
                    {
                        // searcj all tasks for this datapoint
                        BEnelCloudTask[] tasks = searchTasksFromDevicePoint(deviceCode, dataPointName);
                        // create a json array
                        JSONArray taskArray = new JSONArray();
                        // and get all tasks
                        for (int i = 0; i < tasks.length; i++)
                            taskArray.put(tasks[i].getJSONObject());

                        // make the result string
                        String result = "{"+
                                "\"retval\":\"" + true+ "\"," +
                                "\"dataObject\":{"  + taskArray.toString() +
                                "}," +
                                "\"message\":\"" + "done" + "\"" +
                                "}";

                        // write the result
                        return result;
                    }
                    else if (taskId.startsWith("?")) // start with ?, not a specific profile, but all profiles for this point start or end in defined date
                    {
                        //TODO: da implementare
                    }
                    else // specific profile requested
                    {
                        String uniqueTaskId = "Task_" + deviceCode + "_" + dataPointName + "_" + taskId;

                        // call the create/edit profile method
                        BEnelCloudTask foundTask = searchExistingTask(uniqueTaskId);

                        // parse the result code
                        if (foundTask == null)
                        {
                            return "";
                        }
                        else
                            return foundTask.getJSONDescriptor();
                    }
                }
                else
                {
                    return "";
                 }

        }
        catch (Exception e)
        {
            return "";
        }
        return "";
    }

    ////////////////////////////////////////////////////////////////
// DELETE PROFILE-TASK
////////////////////////////////////////////////////////////////

    public boolean schedulingDeleter(String deviceCode, String dataPointName, String commands, String taskId)
    {
        try
        {
            // load the main service
            loadService();

                // select the right function
                if (commands.equals("task")) // token function
                {
                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }


                    String uniqueTaskId = "Task_" + deviceCode + "_" + dataPointName + "_" + taskId;
                    // call the create/edit profile method
                    BEnelCloudTask foundTask = searchExistingTask(uniqueTaskId);

                    // parse the result code
                    if (foundTask == null)
                    {
                        return false;
                    }
                    else
                    {
                        // remove the task from the folder
                        getTasksContainer().remove(foundTask.getName());
                        // response OK - 200
                        return true;
                    }
                }
                else if (commands.equals("profile")) // token function
                {
                    String profileId = taskId;

                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }


                    String uniqueProfileId = "Profile_" + deviceCode + "_" + dataPointName + "_" + profileId;
                    // call the create/edit profile method
                    BEnelCloudProfile foundProfile = searchExistingProfile(uniqueProfileId);

                    // parse the result code
                    if (foundProfile == null)
                    {
                        return false;
                    }
                    else
                    {
                        // remove the task from the folder
                        getProfilesContainer().remove(foundProfile.getName());
                        // response OK - 200
                        return true;
                    }
                }
                else
                {
                    return false;
                }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

////////////////////////////////////////////////////////////////
// POST
////////////////////////////////////////////////////////////////

    @Override
    public void doPost(WebOp op) throws Exception
    {
        try
        {
            // load the main service
            loadService();

            // security check
            if (isRequestWithValidToken(op))
            {
                // get the data from url
                //es. installations/:installationId/devices/code/:deviceCode/datapoint/:dataPointName/commands/profile/
                StringTokenizer tokenizer = new StringTokenizer(op.getPathInfo(), "/");
                tokenizer.nextToken();                        // installations
                String installatonId = tokenizer.nextToken(); //installationId
                tokenizer.nextToken();                        // devices
                tokenizer.nextToken();                        // code
                String deviceCode = tokenizer.nextToken();    //deviceCode
                tokenizer.nextToken();                        // datapoint
                String dataPointName = tokenizer.nextToken(); //dataPointName
                tokenizer.nextToken();                        // commands
                String commands = tokenizer.nextToken();      //commands name

                // select the right function
                if (commands.equals("profile")) // token function
                {
                    // Read data from request
                    StringBuilder buffer = new StringBuilder();
                    BufferedReader reader = op.getRequest().getReader();
                    String line;
                    // cycle all lines
                    while ((line = reader.readLine()) != null)
                    {
                        buffer.append(line);
                    }
                    // all line in one string.
                    String data = buffer.toString();

                    // read the JSON object and get the value
                    JSONObject dataObj = new JSONObject(data);

                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("installatonId: " + installatonId);
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }

                    // check if the point exist, if not, BAD REQUEST
                    if (findRealPoint(deviceCode, dataPointName) == null)
                    {
                        op.getResponse().sendError(400, "BAD REQUEST");
                        service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                    }
                    else
                    {

                        // call the create/edit profile method
                        int resultOp = addEditProfile(deviceCode, dataPointName, dataObj);

                        // parse the result code
                        if (resultOp == 0)
                            op.getWriter().write("New profile created");
                        else if (resultOp == 1)
                            op.getWriter().write("Existing profile replaced");
                        else
                        {
                            op.getResponse().sendError(400, "BAD REQUEST");
                            service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                        }
                    }

                }
                else if (commands.equals("task")) // token function
                {
                    // Read data from request
                    StringBuilder buffer = new StringBuilder();
                    BufferedReader reader = op.getRequest().getReader();
                    String line;
                    // cycle all lines
                    while ((line = reader.readLine()) != null)
                    {
                        buffer.append(line);
                    }
                    // all line in one string.
                    String data = buffer.toString();

                    // read the JSON object and get the value
                    JSONObject dataObj = new JSONObject(data);

                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("installatonId: " + installatonId);
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }

                    // check if the point exist, if not, BAD REQUEST
                    if (findRealPoint(deviceCode, dataPointName) == null)
                    {
                        op.getResponse().sendError(400, "BAD REQUEST");
                        service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                    }
                    else
                    {
                        // call the create task method
                        int resultOp = addEditTask(deviceCode, dataPointName, dataObj);

                        // parse the result code
                        if (resultOp == 0)
                            op.getWriter().write("New task created");
                        else if (resultOp == 1)
                            op.getWriter().write("Existing task replaced");
                        else
                        {
                            op.getResponse().sendError(400, "BAD REQUEST" );
                            service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                        }
                    }
                }
                else if (commands.equals("calendarMonth")) // token function
                {
                    // Read data from request
                    StringBuilder buffer = new StringBuilder();
                    BufferedReader reader = op.getRequest().getReader();
                    String line;
                    // cycle all lines
                    while ((line = reader.readLine()) != null)
                    {
                        buffer.append(line);
                    }
                    // all line in one string.
                    String data = buffer.toString();

                    // read the JSON object and get the value
                    JSONObject dataObj = new JSONObject(data);

                    // decode the string
                    String monthString = dataObj.getString("month");
                    int month = BInteger.make(monthString.substring(4)).getInt();
                    int year = BInteger.make(monthString.substring(0,4)).getInt();
                    // get the right schedule
                    BAbstractSchedule schedule = (BAbstractSchedule)getGeneratedSchedulersContainer().get("Schedule_" + deviceCode + "_" + dataPointName);
                    // set the start date
                    BMonth startMonth = BMonth.make(month-1);
                    BAbsTime selectedMonth = BAbsTime.make(year,startMonth, 1,1,1);

                    // create the response array of profiles
                    JSONArray monthProfiles = new JSONArray();

                    // all days in the month
                    while (selectedMonth.getMonth().equals(startMonth))
                    {
                        // get the source name of the output for the specified day
                        BAbstractSchedule src = ((BWeeklySchedule)schedule).getOutputSource(selectedMonth);
                        String currentOutput = ((BWeeklySchedule) schedule).getSummary(src);

                        // put -1, no output set.
                        if (currentOutput.equals("Default Output"))
                        {
                            // put value inside the array
                            monthProfiles.put("-1");
                        }
                        else // search for the task name
                        {
                            StringTokenizer tokenizerSch = new StringTokenizer(currentOutput, "_");
                            //skip first
                            tokenizerSch.nextToken();
                            // get the right task
                            String taskId = "Task_" + deviceCode + "_" + dataPointName + "_" + tokenizerSch.nextToken() + "";
                            BEnelCloudTask selectedTask = (BEnelCloudTask)getTasksContainer().get(taskId);
                            // put value inside the array
                            monthProfiles.put(selectedTask.getProfileId());
                        }

                        // increment day
                        selectedMonth = selectedMonth.nextDay();
                    }

                    // create the response
                    JSONObject result = new JSONObject();
                    result.put("retval", true);
                    JSONObject dataObjRes = new JSONObject();
                    dataObjRes.put("monthProfiles", monthProfiles);
                    result.put("dataObject", dataObjRes);
                    result.put("message", "done");

                    // send the response
                    op.getWriter().write(result.toString());
                }
                else
                {
                    op.getResponse().sendError(400, "BAD REQUEST");
                    service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                }
            } else
            {
                op.getResponse().sendError(403, "FORBIDDEN");
                service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: FORBIDDEN");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            op.getResponse().sendError(400, "BAD REQUEST");
            service.getEnelCloudAlarmsManager().generateAlarm(this, "POST: BAD REQUEST");
        }
    }

 ////////////////////////////////////////////////////////////////
// GET
////////////////////////////////////////////////////////////////

    @Override
    public void doGet(WebOp op) throws Exception
    {
        try
        {
            // load the main service
            loadService();

            // security check
            if (isRequestWithValidToken(op))
            {
                // get the data from url
                //es. installations/:installationId/devices/code/:deviceCode/datapoint/:dataPointName/commands/profile/:profileId
                StringTokenizer tokenizer = new StringTokenizer(op.getPathInfo(), "/");
                tokenizer.nextToken();                        // installations
                String installatonId = tokenizer.nextToken(); //installationId
                tokenizer.nextToken();                        // devices
                tokenizer.nextToken();                        // code
                String deviceCode = tokenizer.nextToken();    //deviceCode
                tokenizer.nextToken();                        // datapoint
                String dataPointName = tokenizer.nextToken(); //dataPointName
                tokenizer.nextToken();                        // commands
                String commands = tokenizer.nextToken();      //profile o task

                // select the right function
                if (commands.equals("profile")) // token function
                {
                    // String for profileId or other parameters
                    String profileId = null;

                    // try to read the last token
                    try
                    {
                        profileId = tokenizer.nextToken();      //profileId
                    }
                    catch (Exception e){}; // no profileId

                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("installatonId: " + installatonId);
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }

                    // if profileId it's null, get all tasks for this point
                    if (profileId == null)
                    {
                        // searcj all tasks for this datapoint
                        BEnelCloudProfile[] profiles = searchProfilesFromDevicePoint(deviceCode, dataPointName);

                        // check if there are at least one profile
                        if (profiles.length > 0)
                        {
                            // create a json array
                            JSONArray profileArray = new JSONArray();
                            // and get all tasks
                            for (int i = 0; i < profiles.length; i++)
                                profileArray.put(profiles[i].getJSONObject());

                            // make the result string
                            String result = "{" +
                                    "\"retval\":\"" + true + "\"," +
                                    "\"dataObject\":{" + profileArray.toString() +
                                    "}," +
                                    "\"message\":\"" + "done" + "\"" +
                                    "}";

                            // write the result
                            op.getWriter().write(result);
                        }
                        else //if not, error, bad request
                        {
                            op.getResponse().sendError(400, "BAD REQUEST");
                            service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                        }
                    }
                    else
                    {
                        String uniqueProfileId = "Profile_" + deviceCode + "_" + dataPointName + "_" + profileId;

                        // call the create/edit profile method
                        BEnelCloudProfile foundProfile = searchExistingProfile(uniqueProfileId);
                        // parse the result code
                        if (foundProfile == null)
                        {
                            op.getResponse().sendError(400, "BAD REQUEST" );
                            service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                        }
                        else
                        {
                            op.getWriter().write(foundProfile.getJSONDescriptor());
                        }
                    }
                }
                else if (commands.equals("task")) // token function
                {

                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("installatonId: " + installatonId);
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }

                    // String for taskId or other parameters
                    String taskId = null;

                    // try to read the last token
                    try
                    {
                        taskId = tokenizer.nextToken();      //taskId
                    }
                    catch (Exception e){}; // no taskId and no timerange

                    // if taskId it's null, get all tasks for this point
                    if (taskId == null)
                    {
                        // searcj all tasks for this datapoint
                        BEnelCloudTask[] tasks = searchTasksFromDevicePoint(deviceCode, dataPointName);
                        // create a json array
                        JSONArray taskArray = new JSONArray();
                        // and get all tasks
                        for (int i = 0; i < tasks.length; i++)
                            taskArray.put(tasks[i].getJSONObject());

                        // make the result string
                        String result = "{"+
                                "\"retval\":\"" + true+ "\"," +
                                "\"dataObject\":{"  + taskArray.toString() +
                                "}," +
                                "\"message\":\"" + "done" + "\"" +
                                "}";

                        // write the result
                        op.getWriter().write( result);
                    }
                    else if (taskId.startsWith("?")) // start with ?, not a specific profile, but all profiles for this point start or end in defined date
                    {
                        //TODO: da implementare
                    }
                    else // specific profile requested
                    {
                        String uniqueTaskId = "Task_" + deviceCode + "_" + dataPointName + "_" + taskId;

                        // call the create/edit profile method
                        BEnelCloudTask foundTask = searchExistingTask(uniqueTaskId);

                        // parse the result code
                        if (foundTask == null)
                        {
                            op.getResponse().sendError(400, "BAD REQUEST");
                            service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                        }
                        else
                            op.getWriter().write( foundTask.getJSONDescriptor());
                    }
                }
                else
                {
                    op.getResponse().sendError(400, "BAD REQUEST");
                    service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                }
            } else
            {
                op.getResponse().sendError(403, "FORBIDDEN");
                service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: FORBIDDEN");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            op.getResponse().sendError(400, "BAD REQUEST");
            service.getEnelCloudAlarmsManager().generateAlarm(this, "GET: BAD REQUEST");
        }
    }


    ////////////////////////////////////////////////////////////////
// DELETE
////////////////////////////////////////////////////////////////

    @Override
    public void doDelete(WebOp op) throws Exception
    {
        try
        {
            // load the main service
            loadService();

            // security check
            if (isRequestWithValidToken(op))
            {
                // get the data from url
                //es. installations/:installationId/devices/code/:deviceCode/datapoint/:dataPointName/commands/task/:taskId
                StringTokenizer tokenizer = new StringTokenizer(op.getPathInfo(), "/");
                tokenizer.nextToken();                        // installations
                String installatonId = tokenizer.nextToken(); //installationId
                tokenizer.nextToken();                        // devices
                tokenizer.nextToken();                        // code
                String deviceCode = tokenizer.nextToken();    //deviceCode
                tokenizer.nextToken();                        // datapoint
                String dataPointName = tokenizer.nextToken(); //dataPointName
                tokenizer.nextToken();                        // commands
                String commands = tokenizer.nextToken();      //task
                String taskId = tokenizer.nextToken();      //taskId

                // select the right function
                if (commands.equals("task")) // token function
                {
                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("installatonId: " + installatonId);
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }


                    String uniqueTaskId = "Task_" + deviceCode + "_" + dataPointName + "_" + taskId;
                    // call the create/edit profile method
                    BEnelCloudTask foundTask = searchExistingTask(uniqueTaskId);

                    // parse the result code
                    if (foundTask == null)
                    {
                        op.getResponse().sendError(400, "BAD REQUEST");
                        service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                    }
                    else
                    {
                        // remove the task from the folder
                        getTasksContainer().remove(foundTask.getName());
                        // response OK - 200
                        op.getWriter().write("Task deleted");
                    }
                }
                else if (commands.equals("profile")) // token function
                {
                    String profileId = taskId;

                    // fine log messages
                    if (logger.isLoggable(Level.FINE))
                    {
                        logger.fine("installatonId: " + installatonId);
                        logger.fine("deviceCode: " + deviceCode);
                        logger.fine("dataPointName: " + dataPointName);
                    }


                    String uniqueProfileId = "Profile_" + deviceCode + "_" + dataPointName + "_" + profileId;
                    // call the create/edit profile method
                    BEnelCloudProfile foundProfile = searchExistingProfile(uniqueProfileId);

                    // parse the result code
                    if (foundProfile == null)
                    {
                        op.getResponse().sendError(400, "BAD REQUEST");
                        service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                    }
                    else
                    {
                        // remove the task from the folder
                        getProfilesContainer().remove(foundProfile.getName());
                        // response OK - 200
                        op.getWriter().write("Task deleted");
                    }
                }
                else
                {
                    op.getResponse().sendError(400, "BAD REQUEST");
                    service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: BAD REQUEST" + " DEVICE: " + deviceCode + " POINT: " + dataPointName );
                }
            } else
            {
                op.getResponse().sendError(403, "FORBIDDEN");
                service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: FORBIDDEN");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            op.getResponse().sendError(400, "BAD REQUEST");
            service.getEnelCloudAlarmsManager().generateAlarm(this, "DELETE: BAD REQUEST");
        }
    }


    ////////////////////////////////////////////////////////////////
// Profiles
////////////////////////////////////////////////////////////////

    // add or edit an existing profile
    // return 0 = new add; return 1 = edited; return 2 = error
    private int addEditProfile(String deviceName, String pointName, JSONObject profilePayload)
    {
        // return value as default error
        int returnvalue = 2;

        // decode payload
        String profileId = profilePayload.getString("profileId");
        JSONArray profileData = profilePayload.getJSONArray("profileData");
        String profileDescription = profilePayload.getString("profileDescription");

        // id and description must be setted
        if (profileId.length() == 0)
        {
            System.out.println("ERROR: profile id lenght wrong");
            return 2;
        }
        if (profileDescription.length() == 0)
        {
            System.out.println("ERROR: Description must be > 0");
            return 2;
        }
        // the array mustbe 96 values
        if(profileData.length() != 96)
        {
            System.out.println("ERROR: the array mustbe 96 values");
            return 2;
        }

        String uniqueProfileId = "Profile_" + deviceName + "_" + pointName + "_" + profileId;

        //search for an existing profile
        BEnelCloudProfile profile = searchExistingProfile(uniqueProfileId);

        // check if we need only to edit or we need to add the new profile
        if (profile == null)
        {
            returnvalue = 0;
            // create the new profile
            profile = new BEnelCloudProfile(profileId, profileData, profileDescription, deviceName, pointName);
            // add the profile into the container folder
            getProfilesContainer().add(profile.getProfileUniqueId(), profile); //TODO: try/catch
        }
        else
        {
            returnvalue = 1;
            // edit the current profile
            profile.setProfileData(profileData.toString());
            profile.setProfileDescription(profileDescription);
            profile.setDeviceName(deviceName);
            profile.setPointName(pointName);
            profile.setProfileDataJSON(profileData);
        }

        System.out.println("Error Code: " + returnvalue);
        // error return.
        return returnvalue;
    }

    // check if already exist one profile with this id
    private BEnelCloudProfile searchExistingProfile(String uniqueProfileId)
    {
        // get all profiles objects
        BEnelCloudProfile[] profiles = getProfilesContainer().getChildren(BEnelCloudProfile.class);
        // set the found flag to false
        boolean found = false;
        // cycle every profile instance
        for (int i = 0; i < profiles.length && found == false; i++)
        {
            BEnelCloudProfile currentProfile = profiles[i];

            // if found get it.
            if (currentProfile.getProfileUniqueId().equals(uniqueProfileId))
            {
                return currentProfile;
            }
        }

        // if not found return null
        return null;
    }

    // search all profiles associated to a specific point
    private BEnelCloudProfile[] searchProfilesFromDevicePoint(String deviceCode, String pointCode)
    {
        // create a container for the results
        ArrayList<BEnelCloudProfile> profilesList = new ArrayList<BEnelCloudProfile>();

        // get all tasks objects
        BEnelCloudProfile[] profiles = getProfilesContainer().getChildren(BEnelCloudProfile.class);

        // cycle every task instance
        for (int i = 0; i < profiles.length; i++)
        {
            BEnelCloudProfile currentProfile = profiles[i];

            // if found get it.
            if (currentProfile.getDeviceName().equals(deviceCode) && currentProfile.getPointName().equals(pointCode) )
            {
                profilesList.add(currentProfile);
            }
        }

        // return the result array
        return profilesList.toArray(new BEnelCloudProfile[profilesList.size()]);
    }

////////////////////////////////////////////////////////////////
// Tasks
////////////////////////////////////////////////////////////////

    // add or edit an existing task
    // return 0 = new add; return 1 = edited; return 2 = error
    private int addEditTask(String deviceName, String pointName, JSONObject taskPayload)
    {
        // return value as default error
        int returnvalue = 2;

        // decode payload
        String taskId = taskPayload.getString("taskId");
        String taskDescription = taskPayload.getString("taskDescription");
        String profileId = taskPayload.getString("profileId");
        JSONObject startDate = taskPayload.getJSONObject("startDate");
        JSONObject endDate = taskPayload.getJSONObject("endDate");
        boolean onMonday = taskPayload.getBoolean("onMonday");
        boolean onTuesday = taskPayload.getBoolean("onTuesday");
        boolean onWednesday = taskPayload.getBoolean("onWednesday");
        boolean onThursday = taskPayload.getBoolean("onThursday");
        boolean onFriday = taskPayload.getBoolean("onFriday");
        boolean onSaturday = taskPayload.getBoolean("onSaturday");
        boolean onSunday = taskPayload.getBoolean("onSunday");
        int priority = taskPayload.getInt("priority");
        boolean isException = taskPayload.getBoolean("isException");

        // id and description must be setted
        if (taskId.length() == 0)
            return 2;
        if (taskDescription.length() == 0)
            return 2;

        String uniqueTaskId = "Task_" + deviceName + "_" + pointName + "_" + taskId;

        //verify the profile id validity
        if (searchExistingProfile("Profile_" + deviceName + "_" + pointName + "_" + profileId) == null) {
            System.out.println("PROFILE ID ASSOCIATED NOT VALID");
            return 2;
        }

        // check if we have also the slot for this set
        String slotId = "in8";
        try
        {
            slotId = taskPayload.getString("slotId");
        } catch (Exception e) {};

        //search for an existing profile
        BEnelCloudTask task = searchExistingTask(uniqueTaskId);

        // check if we need only to edit or we need to add the new profile
        if (task == null)
        {
            returnvalue = 0;
            // create the new task
            task = new BEnelCloudTask(taskId, taskDescription, profileId, startDate, endDate, onMonday, onTuesday, onWednesday, onThursday, onFriday, onSaturday, onSunday, priority, isException, deviceName, pointName, slotId );
            // add the task into the container folder
            getTasksContainer().add(task.getTaskUniqueId(), task); //TODO: try/catch
        }
        else
        {
            returnvalue = 1;
            //delete the old task
            getTasksContainer().remove(task);
            // create the new task
            task = new BEnelCloudTask(taskId, taskDescription, profileId, startDate, endDate, onMonday, onTuesday, onWednesday, onThursday, onFriday, onSaturday, onSunday, priority, isException, deviceName, pointName, slotId );
            // add the task into the container folder
            getTasksContainer().add(task.getTaskUniqueId(), task); //TODO: try/catch
        }

        // create/update schedule for this point
        try
        {
            createScheduleFromProfilesTasks(deviceName,pointName);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            returnvalue = 2;
        }

        // error return.
        return returnvalue;
    }

    // check if already exist one task with this id
    private BEnelCloudTask searchExistingTask(String uniqueTaskId)
    {
        // get all tasks objects
        BEnelCloudTask[] tasks = getTasksContainer().getChildren(BEnelCloudTask.class);
        // set the found flag to false
        boolean found = false;
        // cycle every task instance
        for (int i = 0; i < tasks.length && found == false; i++)
        {
            BEnelCloudTask currentTask = tasks[i];

            // if found get it.
            if (currentTask.getTaskUniqueId().equals(uniqueTaskId))
            {
                return currentTask;
            }
        }

        // if not found return null
        return null;
    }

    private BEnelCloudTask[] searchTasksFromDevicePoint(String deviceCode, String pointCode)
    {
        // create a container for the results
        ArrayList<BEnelCloudTask> tasksList = new ArrayList<BEnelCloudTask>();

        // get all tasks objects
        BEnelCloudTask[] tasks = getTasksContainer().getChildren(BEnelCloudTask.class);

        // cycle every task instance
        for (int i = 0; i < tasks.length; i++)
        {
            BEnelCloudTask currentTask = tasks[i];

            // if found get it.
            if (currentTask.getDeviceName().equals(deviceCode) && currentTask.getPointName().equals(pointCode) )
            {
                tasksList.add(currentTask);
            }
        }

        // Sorting based on priority
        Collections.sort(tasksList, new Comparator<BEnelCloudTask>() {
            @Override
            public int compare(BEnelCloudTask task2, BEnelCloudTask task1)
            {
                if (task1.getPriority() < (task2.getPriority()))
                    return -1;
                else if (task1.getPriority() > (task2.getPriority()))
                    return 1;
                return 0;
            }
        });

        // return the result array
        return tasksList.toArray(new BEnelCloudTask[tasksList.size()]);
    }

 ////////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

    private void loadService()
    {
        // check if the service is loaded, if not, load it.
        try
        {
            if (service == null)
                service = (BEnelCloudService)getParent();
        }
        catch (Exception e)
        {
            //error - servlet in wrong location.
            e.printStackTrace();
        }
    }

    // utility fo building the string response
    private String buildResponse(String retVal, String output, String failOver, String timestamp )
    {
        String result = "{"+
                "\"retval\":\"" + retVal+ "\"," +
                "\"dataObject\":{" +
                "\"output\":\"" + output +"\"," +
                "\"failOver\":\"" + failOver + "\"," +
                "\"timestamp\":\"" + timestamp + "\"" +
                "}" +
                "}";

        return result;
    }


    // check if the token on the request is valid
    private boolean isRequestWithValidToken(WebOp op)
    {
        try
        {
           String token = op.getRequest().getHeader("x-access-token");
            // ask to the service for the validity
            if (service.getAccessToken().equals(token))
                return true;
            else
                return false;
            //return true;
        }
        catch (Exception e)
        {
            return false; // something wrong, security check failed.
        }
    }

////////////////////////////////////////////////////////////////
// Create a result schedule for Profiles and Tasks
////////////////////////////////////////////////////////////////

    public void createScheduleFromProfilesTasks(String deviceCode, String pointCode)
    {
        // search all task related to this device/point
        BEnelCloudTask[] tasks = searchTasksFromDevicePoint(deviceCode, pointCode);

        // create an empty instance of schedule
        BWeeklySchedule newSchedule = null;

        //check if there is a manager associated, if not create one.
        BEnelCloudPointManager pointManager = EnelCloudUtils.makePointManager(findRealPoint(deviceCode, pointCode));

        // create a new schedule
        if (pointManager.isNumeric())
             newSchedule = new BNumericSchedule();
        else if (pointManager.isBoolean())
            newSchedule = new BBooleanSchedule();
        else if (pointManager.isString())
            newSchedule = new BStringSchedule();
        else if (pointManager.isEnum())
            newSchedule = new BEnumSchedule();

        // parse all task and create a special event for every task
        for (int i = 0; i < tasks.length; i++)
        {
            // create the start and end date for this task
            BDate startDate = BDate.make(tasks[i].getStartYear(), tasks[i].getStartMonth(), tasks[i].getStartDay());
            BDate endDate = BDate.make(tasks[i].getEndYear(), tasks[i].getEndMonth(), tasks[i].getEndDay());
            // day value coming from profile associated to this task
            //BDaySchedule currentTaskDailySchedule = searchExistingProfile("Profile_" + deviceCode + "_" + pointCode + "_" + tasks[i].getProfileId()).getDayScheduleObject();
            BDaySchedule currentTaskDailySchedule = null;
            if (pointManager.isNumeric())
                currentTaskDailySchedule = searchExistingProfile("Profile_" + deviceCode + "_" + pointCode + "_" + tasks[i].getProfileId()).getDayScheduleObject();
            else if (pointManager.isBoolean())
                currentTaskDailySchedule = searchExistingProfile("Profile_" + deviceCode + "_" + pointCode + "_" + tasks[i].getProfileId()).getDayScheduleObjectBoolean();
            else if (pointManager.isString())
                currentTaskDailySchedule = searchExistingProfile("Profile_" + deviceCode + "_" + pointCode + "_" + tasks[i].getProfileId()).getDayScheduleObjectString();
            else if (pointManager.isEnum())
                currentTaskDailySchedule = searchExistingProfile("Profile_" + deviceCode + "_" + pointCode + "_" + tasks[i].getProfileId()).getDayScheduleObjectEnum();

            // if is a all weekday task, create only one event instead of one per single day
            if (tasks[i].allWeekdaysTask())
            {
                // create a new dailySchedule related to task
                BDailySchedule newDailyCustomEventSchedule = new BDailySchedule();
                // set the day value coming from profile associated to this task
                newDailyCustomEventSchedule.setDay((BDaySchedule) currentTaskDailySchedule.newCopy());
                // set the date range
                BDateRangeSchedule dateRangeSchedule = new BDateRangeSchedule();
                dateRangeSchedule.setStart(new BDateSchedule(startDate.getDay(), startDate.getMonth(), startDate.getYear()));
                dateRangeSchedule.setEnd(new BDateSchedule(endDate.getDay(),endDate.getMonth(), endDate.getYear()));
                // add to special event the dateschedule
                newDailyCustomEventSchedule.setDays(dateRangeSchedule);
                // add this special event
                newSchedule.addSpecialEvent("Task_" + tasks[i].getTaskId(), newDailyCustomEventSchedule);
            }
            else
            {
                //counter
                int counter = 0;
                // cycle all days from start to end
                //while (!startDate.equals(endDate)) //ERRORE ESTREMI DATE
                while (startDate.compareTo(endDate) <= 0)
                {
                    // if we are inside a valid weekday, we proceed, instead skip
                    if (tasks[i].validWeekday(startDate.getWeekday()))
                    {
                        // create a new dailySchedule related to task
                        BDailySchedule newDailyCustomEventSchedule = new BDailySchedule();
                        // set the day value coming from profile associated to this task
                        newDailyCustomEventSchedule.setDay((BDaySchedule) currentTaskDailySchedule.newCopy());
                        // set the current day
                        BDateSchedule dateSchedule = new BDateSchedule(startDate.getDay(), startDate.getMonth(), startDate.getYear());
                        // add to special event the dateschedule
                        newDailyCustomEventSchedule.setDays(dateSchedule);
                        // add this special event
                        newSchedule.addSpecialEvent("Task_" + tasks[i].getTaskId() + "_" + counter, newDailyCustomEventSchedule);
                        // increment counter
                        counter++;
                    }

                    // increment start date
                    startDate = startDate.nextDay();
                }
            }
        }

        // remove if exist a previous schedule in the same point
        try
        {
            getGeneratedSchedulersContainer().remove("Schedule_" + deviceCode + "_" + pointCode);
        }
        catch (Exception e) {};

        // add schedule into schedule folder
        getGeneratedSchedulersContainer().add("Schedule_" + deviceCode + "_" + pointCode, newSchedule);

        // make link with scheduler to the real point manager
        //set the right slotID for scheduling manager, based on high priorirty task.
        pointManager.doResetStandardSet(BString.make(pointManager.getSchedulingManagerInputId())); // first reset the previous one
        pointManager.setSchedulingManagerInputId(tasks[0].getSlotId());

        if (pointManager.isNumeric())
            EnelCloudUtils.makeLink("Link_" + deviceCode + "_" + pointCode, newSchedule, pointManager, "out", "inputSchedulingManagerNumeric");
        else if (pointManager.isBoolean())
            EnelCloudUtils.makeLink("Link_" + deviceCode + "_" + pointCode, newSchedule, pointManager, "out", "inputSchedulingManagerBoolean");
        else if (pointManager.isString())
            EnelCloudUtils.makeLink("Link_" + deviceCode + "_" + pointCode, newSchedule, pointManager, "out", "inputSchedulingManagerString");
        else if (pointManager.isEnum())
            EnelCloudUtils.makeLink("Link_" + deviceCode + "_" + pointCode, newSchedule, pointManager, "out", "inputSchedulingManagerEnum");

    }


 ////////////////////////////////////////////////////////////////
// Find the real point inside drivers
////////////////////////////////////////////////////////////////

    private BControlPoint findRealPoint(String deviceCode, String dataPointName)
    {
        // create the bql query for searching the right device
        BOrd query = BOrd.make("slot:/|bql:select slotPathOrd from driver:Device where name = '" + deviceCode + "'");
        // resolve the bql query
        BITable result = (BITable) query.resolve(this).get();
        // get the columns
        ColumnList columns = result.getColumns();
        // select the frist one with the slotPathOrd
        Column valueColumn = columns.get(0);
        // get the cursor
        TableCursor tableCursor = result.cursor();
        // get the first row
        tableCursor.next();
        // get the device from the slotPathOrd getted
        BDevice device = ((BDevice) tableCursor.get());
        // get all extensions and set to null the points one
        BDeviceExt[] exts = device.getDeviceExts();
        BPointDeviceExt pointDeviceExt = null;

        // search the point extension
        for (int i = 0; i < exts.length; i++)
        {
            if (exts[i] instanceof BPointDeviceExt)
                pointDeviceExt = (BPointDeviceExt) exts[i];
        }

        // check if the extension exist
        if (pointDeviceExt != null)
        {
            // get all points inside the extension
            //BControlPoint[] points = pointDeviceExt.getPoints();
            BControlPoint[] points = pointDeviceExt.getChildren(BControlPoint.class);
            // found flag
            boolean found = false;
            // search for the right one
            for (int i = 0; i < points.length && !found; i++)
            {
                BControlPoint currentPoint = points[i];
                // check the name
                if (currentPoint.getName().equals(dataPointName))
                {
                    // change the flag
                    return currentPoint;
                }
            }
            // if not found in all points, get error
            if (!found)
            {
                return null;
            }
        }
        return null;
    }

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("schedule.png");
    }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////
    private BEnelCloudService service = null;
    public static final Logger logger = Logger.getLogger("EnelCloudSchedulingManager");
}
