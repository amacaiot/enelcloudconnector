package com.amaca.enelCloudConnector.scheduling.datastructures;

import com.tridium.json.JSONObject;

import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.sys.*;
import java.util.ArrayList;

/**
 * Created by Amaca on 21/07/2018.
 */

@NiagaraProperty(
        name = "taskUniqueId",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = ""
)
@NiagaraProperty(
        name = "taskId",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = ""
)
@NiagaraProperty(
        name = "taskDescription",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = ""
)
@NiagaraProperty(
        name = "profileId",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = ""
)
@NiagaraProperty(
        name = "startDate",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "endDate",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "onMonday",
        type = "baja:Boolean",
        flags = Flags.READONLY,
        defaultValue = "BBoolean.make(false)"
)
@NiagaraProperty(
        name = "onTuesday",
        type = "baja:Boolean",
        flags = Flags.READONLY,
        defaultValue = "BBoolean.make(false)"
)
@NiagaraProperty(
        name = "onWednesday",
        type = "baja:Boolean",
        flags = Flags.READONLY,
        defaultValue = "BBoolean.make(false)"
)
@NiagaraProperty(
        name = "onThursday",
        type = "baja:Boolean",
        flags = Flags.READONLY,
        defaultValue = "BBoolean.make(false)"
)
@NiagaraProperty(
        name = "onFriday",
        type = "baja:Boolean",
        flags = Flags.READONLY,
        defaultValue = "BBoolean.make(false)"
)
@NiagaraProperty(
        name = "onSaturday",
        type = "baja:Boolean",
        flags = Flags.READONLY,
        defaultValue = "BBoolean.make(false)"
)
@NiagaraProperty(
        name = "onSunday",
        type = "baja:Boolean",
        flags = Flags.READONLY,
        defaultValue = "BBoolean.make(false)"
)
@NiagaraProperty(
        name = "priority",
        type = "baja:Integer",
        flags = Flags.READONLY,
        defaultValue = "BInteger.make(0)"
)
@NiagaraProperty(
        name = "isException",
        type = "baja:Boolean",
        flags = Flags.READONLY,
        defaultValue = "BBoolean.make(false)"
)
@NiagaraProperty(
        name = "deviceName",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "pointName",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "slotId",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "in8"
)

@NiagaraType
public class BEnelCloudTask extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.scheduling.datastructures.BEnelCloudTask(301236268)1.0$ @*/
/* Generated Sun Aug 05 10:12:44 CEST 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "taskUniqueId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code taskUniqueId} property.
   * @see #getTaskUniqueId
   * @see #setTaskUniqueId
   */
  public static final Property taskUniqueId = newProperty(Flags.READONLY, "", null);
  
  /**
   * Get the {@code taskUniqueId} property.
   * @see #taskUniqueId
   */
  public String getTaskUniqueId() { return getString(taskUniqueId); }
  
  /**
   * Set the {@code taskUniqueId} property.
   * @see #taskUniqueId
   */
  public void setTaskUniqueId(String v) { setString(taskUniqueId, v, null); }

////////////////////////////////////////////////////////////////
// Property "taskId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code taskId} property.
   * @see #getTaskId
   * @see #setTaskId
   */
  public static final Property taskId = newProperty(Flags.READONLY, "", null);
  
  /**
   * Get the {@code taskId} property.
   * @see #taskId
   */
  public String getTaskId() { return getString(taskId); }
  
  /**
   * Set the {@code taskId} property.
   * @see #taskId
   */
  public void setTaskId(String v) { setString(taskId, v, null); }

////////////////////////////////////////////////////////////////
// Property "taskDescription"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code taskDescription} property.
   * @see #getTaskDescription
   * @see #setTaskDescription
   */
  public static final Property taskDescription = newProperty(Flags.READONLY, "", null);
  
  /**
   * Get the {@code taskDescription} property.
   * @see #taskDescription
   */
  public String getTaskDescription() { return getString(taskDescription); }
  
  /**
   * Set the {@code taskDescription} property.
   * @see #taskDescription
   */
  public void setTaskDescription(String v) { setString(taskDescription, v, null); }

////////////////////////////////////////////////////////////////
// Property "profileId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code profileId} property.
   * @see #getProfileId
   * @see #setProfileId
   */
  public static final Property profileId = newProperty(Flags.READONLY, "", null);
  
  /**
   * Get the {@code profileId} property.
   * @see #profileId
   */
  public String getProfileId() { return getString(profileId); }
  
  /**
   * Set the {@code profileId} property.
   * @see #profileId
   */
  public void setProfileId(String v) { setString(profileId, v, null); }

////////////////////////////////////////////////////////////////
// Property "startDate"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code startDate} property.
   * @see #getStartDate
   * @see #setStartDate
   */
  public static final Property startDate = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code startDate} property.
   * @see #startDate
   */
  public String getStartDate() { return getString(startDate); }
  
  /**
   * Set the {@code startDate} property.
   * @see #startDate
   */
  public void setStartDate(String v) { setString(startDate, v, null); }

////////////////////////////////////////////////////////////////
// Property "endDate"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code endDate} property.
   * @see #getEndDate
   * @see #setEndDate
   */
  public static final Property endDate = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code endDate} property.
   * @see #endDate
   */
  public String getEndDate() { return getString(endDate); }
  
  /**
   * Set the {@code endDate} property.
   * @see #endDate
   */
  public void setEndDate(String v) { setString(endDate, v, null); }

////////////////////////////////////////////////////////////////
// Property "onMonday"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code onMonday} property.
   * @see #getOnMonday
   * @see #setOnMonday
   */
  public static final Property onMonday = newProperty(Flags.READONLY, ((BBoolean)(BBoolean.make(false))).getBoolean(), null);
  
  /**
   * Get the {@code onMonday} property.
   * @see #onMonday
   */
  public boolean getOnMonday() { return getBoolean(onMonday); }
  
  /**
   * Set the {@code onMonday} property.
   * @see #onMonday
   */
  public void setOnMonday(boolean v) { setBoolean(onMonday, v, null); }

////////////////////////////////////////////////////////////////
// Property "onTuesday"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code onTuesday} property.
   * @see #getOnTuesday
   * @see #setOnTuesday
   */
  public static final Property onTuesday = newProperty(Flags.READONLY, ((BBoolean)(BBoolean.make(false))).getBoolean(), null);
  
  /**
   * Get the {@code onTuesday} property.
   * @see #onTuesday
   */
  public boolean getOnTuesday() { return getBoolean(onTuesday); }
  
  /**
   * Set the {@code onTuesday} property.
   * @see #onTuesday
   */
  public void setOnTuesday(boolean v) { setBoolean(onTuesday, v, null); }

////////////////////////////////////////////////////////////////
// Property "onWednesday"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code onWednesday} property.
   * @see #getOnWednesday
   * @see #setOnWednesday
   */
  public static final Property onWednesday = newProperty(Flags.READONLY, ((BBoolean)(BBoolean.make(false))).getBoolean(), null);
  
  /**
   * Get the {@code onWednesday} property.
   * @see #onWednesday
   */
  public boolean getOnWednesday() { return getBoolean(onWednesday); }
  
  /**
   * Set the {@code onWednesday} property.
   * @see #onWednesday
   */
  public void setOnWednesday(boolean v) { setBoolean(onWednesday, v, null); }

////////////////////////////////////////////////////////////////
// Property "onThursday"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code onThursday} property.
   * @see #getOnThursday
   * @see #setOnThursday
   */
  public static final Property onThursday = newProperty(Flags.READONLY, ((BBoolean)(BBoolean.make(false))).getBoolean(), null);
  
  /**
   * Get the {@code onThursday} property.
   * @see #onThursday
   */
  public boolean getOnThursday() { return getBoolean(onThursday); }
  
  /**
   * Set the {@code onThursday} property.
   * @see #onThursday
   */
  public void setOnThursday(boolean v) { setBoolean(onThursday, v, null); }

////////////////////////////////////////////////////////////////
// Property "onFriday"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code onFriday} property.
   * @see #getOnFriday
   * @see #setOnFriday
   */
  public static final Property onFriday = newProperty(Flags.READONLY, ((BBoolean)(BBoolean.make(false))).getBoolean(), null);
  
  /**
   * Get the {@code onFriday} property.
   * @see #onFriday
   */
  public boolean getOnFriday() { return getBoolean(onFriday); }
  
  /**
   * Set the {@code onFriday} property.
   * @see #onFriday
   */
  public void setOnFriday(boolean v) { setBoolean(onFriday, v, null); }

////////////////////////////////////////////////////////////////
// Property "onSaturday"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code onSaturday} property.
   * @see #getOnSaturday
   * @see #setOnSaturday
   */
  public static final Property onSaturday = newProperty(Flags.READONLY, ((BBoolean)(BBoolean.make(false))).getBoolean(), null);
  
  /**
   * Get the {@code onSaturday} property.
   * @see #onSaturday
   */
  public boolean getOnSaturday() { return getBoolean(onSaturday); }
  
  /**
   * Set the {@code onSaturday} property.
   * @see #onSaturday
   */
  public void setOnSaturday(boolean v) { setBoolean(onSaturday, v, null); }

////////////////////////////////////////////////////////////////
// Property "onSunday"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code onSunday} property.
   * @see #getOnSunday
   * @see #setOnSunday
   */
  public static final Property onSunday = newProperty(Flags.READONLY, ((BBoolean)(BBoolean.make(false))).getBoolean(), null);
  
  /**
   * Get the {@code onSunday} property.
   * @see #onSunday
   */
  public boolean getOnSunday() { return getBoolean(onSunday); }
  
  /**
   * Set the {@code onSunday} property.
   * @see #onSunday
   */
  public void setOnSunday(boolean v) { setBoolean(onSunday, v, null); }

////////////////////////////////////////////////////////////////
// Property "priority"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code priority} property.
   * @see #getPriority
   * @see #setPriority
   */
  public static final Property priority = newProperty(Flags.READONLY, ((BInteger)(BInteger.make(0))).getInt(), null);
  
  /**
   * Get the {@code priority} property.
   * @see #priority
   */
  public int getPriority() { return getInt(priority); }
  
  /**
   * Set the {@code priority} property.
   * @see #priority
   */
  public void setPriority(int v) { setInt(priority, v, null); }

////////////////////////////////////////////////////////////////
// Property "isException"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code isException} property.
   * @see #getIsException
   * @see #setIsException
   */
  public static final Property isException = newProperty(Flags.READONLY, ((BBoolean)(BBoolean.make(false))).getBoolean(), null);
  
  /**
   * Get the {@code isException} property.
   * @see #isException
   */
  public boolean getIsException() { return getBoolean(isException); }
  
  /**
   * Set the {@code isException} property.
   * @see #isException
   */
  public void setIsException(boolean v) { setBoolean(isException, v, null); }

////////////////////////////////////////////////////////////////
// Property "deviceName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code deviceName} property.
   * @see #getDeviceName
   * @see #setDeviceName
   */
  public static final Property deviceName = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code deviceName} property.
   * @see #deviceName
   */
  public String getDeviceName() { return getString(deviceName); }
  
  /**
   * Set the {@code deviceName} property.
   * @see #deviceName
   */
  public void setDeviceName(String v) { setString(deviceName, v, null); }

////////////////////////////////////////////////////////////////
// Property "pointName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code pointName} property.
   * @see #getPointName
   * @see #setPointName
   */
  public static final Property pointName = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code pointName} property.
   * @see #pointName
   */
  public String getPointName() { return getString(pointName); }
  
  /**
   * Set the {@code pointName} property.
   * @see #pointName
   */
  public void setPointName(String v) { setString(pointName, v, null); }

////////////////////////////////////////////////////////////////
// Property "slotId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code slotId} property.
   * @see #getSlotId
   * @see #setSlotId
   */
  public static final Property slotId = newProperty(Flags.READONLY, "in8", null);
  
  /**
   * Get the {@code slotId} property.
   * @see #slotId
   */
  public String getSlotId() { return getString(slotId); }
  
  /**
   * Set the {@code slotId} property.
   * @see #slotId
   */
  public void setSlotId(String v) { setString(slotId, v, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudTask.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


 ////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    public BEnelCloudTask()
    {
        super();
    }

    public BEnelCloudTask(String taskId, String taskDescription, String profileId, JSONObject startDate, JSONObject endDate, boolean onMonday, boolean onTuesday, boolean onWednesday, boolean onThursday, boolean onFriday, boolean onSaturday, boolean onSunday, int priority, boolean isException, String deviceCode, String pointCode, String slotId)
    {
        setTaskId(taskId);
        setTaskDescription(taskDescription);
        setProfileId(profileId);
        setStartDate(startDate.toString());
        setEndDate(endDate.toString());
        setOnMonday(onMonday);
        setOnTuesday(onTuesday);
        setOnWednesday(onWednesday);
        setOnThursday(onThursday);
        setOnFriday(onFriday);
        setOnSaturday(onSaturday);
        setOnSunday(onSunday);
        setPriority(priority);
        setIsException(isException);
        setDeviceName(deviceCode);
        setPointName(pointCode);
        setSlotId(slotId);
        this.startDateJO = startDate;
        this.endDateJO = endDate;

        // set unique id
        setTaskUniqueId("Task_" + getDeviceName() + "_" + getPointName() + "_" + getTaskId());
    }

  ////////////////////////////////////////////////////////////////
// Scheduling accessing method
////////////////////////////////////////////////////////////////

  public int getStartDay()
  {
    // create the obj
    makeJSONObjects();

    return startDateJO.getInt("day");
  }

  public BMonth getStartMonth()
  {
    // create the obj
    makeJSONObjects();

    return BMonth.make(startDateJO.getInt("month") - 1);
  }

  public int getStartYear()
  {
    // create the obj
    makeJSONObjects();

    return startDateJO.getInt("year");
  }

  public int getEndDay()
  {
    // create the obj
    makeJSONObjects();

    return endDateJO.getInt("day");
  }

  public BMonth getEndMonth()
  {
    // create the obj
    makeJSONObjects();

    return BMonth.make(endDateJO.getInt("month") - 1);
  }

  public int getEndYear()
  {
    // create the obj
    makeJSONObjects();

    return endDateJO.getInt("year");
  }

  ////////////////////////////////////////////////////////////////
// weekDays related methods
////////////////////////////////////////////////////////////////

  // get a weekday array for creating the BEnum associated
  public int[] getWeekdayArray()
  {
    // create the basic arraylist
    ArrayList<Integer> weekdays = new ArrayList<Integer>();
    // add the only the true weekedays
    if (getOnSunday())
      weekdays.add(new Integer(0));
    if (getOnMonday())
      weekdays.add(new Integer(1));
    if (getOnTuesday())
      weekdays.add(new Integer(2));
    if (getOnWednesday())
      weekdays.add(new Integer(3));
    if (getOnThursday())
      weekdays.add(new Integer(4));
    if (getOnFriday())
      weekdays.add(new Integer(5));
    if (getOnSaturday())
      weekdays.add(new Integer(6));

    // return the int array
    return toIntArray(weekdays);
  }

  // return true if the parameter weekday is enabled in this profile
  public boolean validWeekday(BWeekday weedkday)
  {
    if (weedkday.getOrdinal() == 0)
      return getOnSunday();
    if (weedkday.getOrdinal() == 1)
      return getOnMonday();
    if (weedkday.getOrdinal() == 2)
      return getOnTuesday();
    if (weedkday.getOrdinal() == 3)
      return getOnWednesday();
    if (weedkday.getOrdinal() == 4)
      return getOnThursday();
    if (weedkday.getOrdinal() == 5)
      return getOnFriday();
    if (weedkday.getOrdinal() == 6)
      return getOnSaturday();

    return false;
  }

  // if all weekdays are true get true, instead false.
  public boolean allWeekdaysTask()
  {
    if (getOnSunday() && getOnMonday() && getOnTuesday() && getOnWednesday() && getOnThursday() && getOnFriday() && getOnSaturday())
      return true;

    return false;
  }

  ////////////////////////////////////////////////////////////////
// Utils
////////////////////////////////////////////////////////////////

  // create the two json objects from string slot saved
  private void makeJSONObjects()
  {
    if (startDateJO == null || endDateJO == null)
    {
      startDateJO = new JSONObject(getStartDate());
      endDateJO = new JSONObject(getEndDate());
    }
  }

  //utils method for getting int[] from ArrayList<Integer>
  private int[] toIntArray(ArrayList<Integer> list)
  {
    int[] ret = new int[list.size()];
    for(int i = 0;i < ret.length;i++)
      ret[i] = list.get(i);
    return ret;
  }

  // get the json object for the task
  public JSONObject getJSONObject()
  {
    // check if the start and enddate are already done
    makeJSONObjects();

    // make the object for this task
    JSONObject task = new JSONObject();
    task.put("taskId", getTaskId());
    task.put("taskDescription", getTaskDescription());
    task.put("startDate", startDateJO);
    task.put("endDate", endDateJO);
    task.put("onMonday", getOnMonday());
    task.put("onTuesday", getOnTuesday());
    task.put("onWednesday", getOnWednesday());
    task.put("onThursday", getOnThursday());
    task.put("onFriday", getOnFriday());
    task.put("onSaturday", getOnSaturday());
    task.put("onSunday", getOnSunday());
    task.put("priority", getPriority());
    task.put("isException", getIsException());
    task.put("slotId", getSlotId());

    return task;
  }

  // get the ready to use response for one task in JSON string
  public String getJSONDescriptor()
  {
    String result = "{"+
            "\"retval\":\"" + true+ "\"," +
            "\"dataObject\":{["  + getJSONObject().toString() +
            "]}," +
            "\"message\":\"" + "done" + "\"" +
            "}";

    return result;
  }


////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("task.png");
    }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

    private JSONObject startDateJO = null;
    private JSONObject endDateJO = null;
}
