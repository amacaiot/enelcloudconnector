package com.amaca.enelCloudConnector.scheduling.datastructures;

import com.tridium.json.JSONArray;
import com.tridium.json.JSONObject;

import javax.baja.nre.annotations.NiagaraProperty;
import javax.baja.nre.annotations.NiagaraType;
import javax.baja.schedule.BDaySchedule;
import javax.baja.status.BStatusBoolean;
import javax.baja.status.BStatusEnum;
import javax.baja.status.BStatusNumeric;
import javax.baja.status.BStatusString;
import javax.baja.sys.*;

/**
 * Created by Amaca on 21/07/2018.
 */

@NiagaraProperty(
        name = "profileUniqueId",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = ""
)
@NiagaraProperty(
        name = "profileId",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = ""
)
@NiagaraProperty(
        name = "profileData",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = ""
)
@NiagaraProperty(
        name = "profileDescription",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = ""
)
@NiagaraProperty(
        name = "deviceName",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)
@NiagaraProperty(
        name = "pointName",
        type = "baja:String",
        flags = Flags.READONLY,
        defaultValue = "null"
)

@NiagaraType
public class BEnelCloudProfile extends BComponent
{
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.amaca.enelCloudConnector.scheduling.datastructures.BEnelCloudProfile(1003499159)1.0$ @*/
/* Generated Thu Jul 26 16:17:10 CEST 2018 by Slot-o-Matic (c) Tridium, Inc. 2012 */

////////////////////////////////////////////////////////////////
// Property "profileUniqueId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code profileUniqueId} property.
   * @see #getProfileUniqueId
   * @see #setProfileUniqueId
   */
  public static final Property profileUniqueId = newProperty(Flags.READONLY, "", null);
  
  /**
   * Get the {@code profileUniqueId} property.
   * @see #profileUniqueId
   */
  public String getProfileUniqueId() { return getString(profileUniqueId); }
  
  /**
   * Set the {@code profileUniqueId} property.
   * @see #profileUniqueId
   */
  public void setProfileUniqueId(String v) { setString(profileUniqueId, v, null); }

////////////////////////////////////////////////////////////////
// Property "profileId"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code profileId} property.
   * @see #getProfileId
   * @see #setProfileId
   */
  public static final Property profileId = newProperty(Flags.READONLY, "", null);
  
  /**
   * Get the {@code profileId} property.
   * @see #profileId
   */
  public String getProfileId() { return getString(profileId); }
  
  /**
   * Set the {@code profileId} property.
   * @see #profileId
   */
  public void setProfileId(String v) { setString(profileId, v, null); }

////////////////////////////////////////////////////////////////
// Property "profileData"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code profileData} property.
   * @see #getProfileData
   * @see #setProfileData
   */
  public static final Property profileData = newProperty(Flags.READONLY, "", null);
  
  /**
   * Get the {@code profileData} property.
   * @see #profileData
   */
  public String getProfileData() { return getString(profileData); }
  
  /**
   * Set the {@code profileData} property.
   * @see #profileData
   */
  public void setProfileData(String v) { setString(profileData, v, null); }

////////////////////////////////////////////////////////////////
// Property "profileDescription"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code profileDescription} property.
   * @see #getProfileDescription
   * @see #setProfileDescription
   */
  public static final Property profileDescription = newProperty(Flags.READONLY, "", null);
  
  /**
   * Get the {@code profileDescription} property.
   * @see #profileDescription
   */
  public String getProfileDescription() { return getString(profileDescription); }
  
  /**
   * Set the {@code profileDescription} property.
   * @see #profileDescription
   */
  public void setProfileDescription(String v) { setString(profileDescription, v, null); }

////////////////////////////////////////////////////////////////
// Property "deviceName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code deviceName} property.
   * @see #getDeviceName
   * @see #setDeviceName
   */
  public static final Property deviceName = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code deviceName} property.
   * @see #deviceName
   */
  public String getDeviceName() { return getString(deviceName); }
  
  /**
   * Set the {@code deviceName} property.
   * @see #deviceName
   */
  public void setDeviceName(String v) { setString(deviceName, v, null); }

////////////////////////////////////////////////////////////////
// Property "pointName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the {@code pointName} property.
   * @see #getPointName
   * @see #setPointName
   */
  public static final Property pointName = newProperty(Flags.READONLY, "null", null);
  
  /**
   * Get the {@code pointName} property.
   * @see #pointName
   */
  public String getPointName() { return getString(pointName); }
  
  /**
   * Set the {@code pointName} property.
   * @see #pointName
   */
  public void setPointName(String v) { setString(pointName, v, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  @Override
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BEnelCloudProfile.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


 ////////////////////////////////////////////////////////////////
// Constructors
////////////////////////////////////////////////////////////////

    public BEnelCloudProfile()
    {
      super();
    }

    public BEnelCloudProfile(String profileId, JSONArray profileData, String profileDescription, String deviceName, String pointName)
    {
        // set all slots
        setProfileId(profileId);
        setProfileDescription(profileDescription);
        setDeviceName(deviceName);
        setPointName(pointName);
        setProfileData(profileData.toString());

        // set unique id
        setProfileUniqueId("Profile_" + getDeviceName() + "_" + getPointName() + "_" + getProfileId());

        // set the attribute
        profileDataJSON = profileData;
    }

  ////////////////////////////////////////////////////////////////
// Scheduling accessing method
////////////////////////////////////////////////////////////////

  // get the day schedule with profile data
  public BDaySchedule getDayScheduleObject()
  {
    // create a new dayschedule
    BDaySchedule daySchedule = new BDaySchedule();

    //set the start and end time
    BTime startTime = BTime.MIDNIGHT;
    BTime endTime = BTime.MIDNIGHT;

    // create the json array for the profile data
    profileDataJSON = new JSONArray(getProfileData());

    //System.out.println("ProfileDataNumbersOfValues (must be 96) -> " + profileDataJSON.length());

    // cycle for every 96 values (24*4) data every 15mins
    for (int i = 0; i < 96 ; i++)
    {
      endTime = endTime.add(BRelTime.makeMinutes(15));

      // calcolate the value for the set.
      double value = 0;
      if (profileDataJSON.getDouble(i) == 0)
        value = 0;
      else
        value = profileDataJSON.getDouble(i) / 10;

      //add the 15mins value, starting from midnight
      daySchedule.add(startTime,endTime, new BStatusNumeric(profileDataJSON.getDouble(i)));
      // update the start time
      startTime = (BTime)endTime.newCopy();
    }

    // return the complete daySchedule
    return daySchedule;
  }

  // get the day schedule with profile data
  public BDaySchedule getDayScheduleObjectEnum()
  {
    // create a new dayschedule
    BDaySchedule daySchedule = new BDaySchedule();

    //set the start and end time
    BTime startTime = BTime.MIDNIGHT;
    BTime endTime = BTime.MIDNIGHT;

    // create the json array for the profile data
    profileDataJSON = new JSONArray(getProfileData());

    //System.out.println("ProfileDataNumbersOfValues (must be 96) -> " + profileDataJSON.length());

    // cycle for every 96 values (24*4) data every 15mins
    for (int i = 0; i < 96 ; i++)
    {
      endTime = endTime.add(BRelTime.makeMinutes(15));

      //add the 15mins value, starting from midnight
      daySchedule.add(startTime,endTime, new BStatusEnum(BDynamicEnum.make((int)profileDataJSON.getDouble(i))));
      // update the start time
      startTime = (BTime)endTime.newCopy();
    }

    // return the complete daySchedule
    return daySchedule;
  }

  // get the day schedule with profile data
  public BDaySchedule getDayScheduleObjectString()
  {
    // create a new dayschedule
    BDaySchedule daySchedule = new BDaySchedule();

    //set the start and end time
    BTime startTime = BTime.MIDNIGHT;
    BTime endTime = BTime.MIDNIGHT;

    // create the json array for the profile data
    profileDataJSON = new JSONArray(getProfileData());

    //System.out.println("ProfileDataNumbersOfValues (must be 96) -> " + profileDataJSON.length());

    // cycle for every 96 values (24*4) data every 15mins
    for (int i = 0; i < 96 ; i++)
    {
      endTime = endTime.add(BRelTime.makeMinutes(15));

      //add the 15mins value, starting from midnight
      daySchedule.add(startTime,endTime, new BStatusString(profileDataJSON.getString(i)));
      // update the start time
      startTime = (BTime)endTime.newCopy();
    }

    // return the complete daySchedule
    return daySchedule;
  }

  // get the day schedule with profile data
  public BDaySchedule getDayScheduleObjectBoolean()
  {
    // create a new dayschedule
    BDaySchedule daySchedule = new BDaySchedule();

    //set the start and end time
    BTime startTime = BTime.MIDNIGHT;
    BTime endTime = BTime.MIDNIGHT;

    // create the json array for the profile data
    profileDataJSON = new JSONArray(getProfileData());

    //System.out.println("ProfileDataNumbersOfValues (must be 96) -> " + profileDataJSON.length());

    // cycle for every 96 values (24*4) data every 15mins
    for (int i = 0; i < 96 ; i++)
    {
      endTime = endTime.add(BRelTime.makeMinutes(15));

      //add the  value
      boolean valueToSet = false;
      if (profileDataJSON.getDouble(i) == 0)
        valueToSet = false;
      else
        valueToSet = true;

      //add the 15mins value, starting from midnight
      daySchedule.add(startTime,endTime, new BStatusBoolean(valueToSet));
      // update the start time
      startTime = (BTime)endTime.newCopy();
    }

    // return the complete daySchedule
    return daySchedule;
  }


 ////////////////////////////////////////////////////////////////
// Access
////////////////////////////////////////////////////////////////

    public void setProfileDataJSON(JSONArray profileData)
    {
        this.profileDataJSON = profileData;
    }

    public String getJSONDescriptor()
    {
      String result = "{"+
              "\"retval\":\"" + true+ "\"," +
              "\"dataObject\":{" +
              "\"profileId\":\"" + getProfileId() +"\"," +
              "\"profileData\":\"" + getProfileData() + "\"," +
              "\"profileDescription\":\"" + getProfileDescription() + "\"" +
              "}," +
              "\"message\":\"" + "done" + "\"" +
              "}";

      return result;
    }

  // get the json object for the profile
  public JSONObject getJSONObject()
  {
    // make the object for this profile
    JSONObject profile = new JSONObject();
    profile.put("profileId", getProfileId());
    profile.put("profileData", new JSONArray(getProfileData()));
    profile.put("profileDescription", getProfileDescription());

    return profile;
  }

////////////////////////////////////////////////////////////////
// Presentation
////////////////////////////////////////////////////////////////

    @Override
    public BIcon getIcon() {
        return BIcon.std("stop.png");
    }

////////////////////////////////////////////////////////////////
// Attributes
////////////////////////////////////////////////////////////////

    private JSONArray profileDataJSON = null;
}
